var spherecastApp = angular.module('spherecastApp', ['ngRoute']);

spherecastApp.run(function($rootScope, $templateCache) {
    $rootScope.$on('$routeChangeStart', function(event, next, current) {
        if (typeof(current) !== 'undefined'){
            $templateCache.remove(current.templateUrl);
        }
    });
});

spherecastApp.constant('Modernizr', Modernizr);

/*
spherecastApp.controller('SceneController', ['$scope', function($scope, $routeParams, $http)
{
	$http.get('http://api.spherecast.org/project/1').success(function(response)
	{
		$scope.project = response;
	})
}]);
*/

// See http://stackoverflow.com/questions/21667613/in-angular-how-to-pass-json-object-array-into-directive

spherecastApp.controller('AppCtrl', ['$scope', function($scope) {
    $scope.canvasWidth = 400;
    $scope.canvasHeight = 400;
    $scope.dofillcontainer = true;
    $scope.scale = 1;

	$scope.toggleMode = function() {
		if ($scope.mode == 'vr')
			$scope.mode = 'standard';
		else
			$scope.mode = 'vr';
	};
}]);

/*
spherecastApp.controller('ProjectController', function($scope, $http)
{
	$http.get('http://app.spherecast.org/api/projects')
		.success(function(data, status, headers, config)
		{
			$scope.projects = data;
		})
		.error(function(data, status, headers, config)
		{
			// TODO: Error-handling.
		});
});
*/

// TODO: Inject the service like this:
// spherecastApp.controller('SceneController', ['$scope', 'loadSceneService', ($scope, loadSceneService)
spherecastApp.controller('SceneController', function($scope, $http, $routeParams)
{
	$http.get('http://app.spherecast.org/api/project/' + $routeParams.projectId + '/scenes')
		.success(function(data, status, headers, config)
		{
			$scope.scenes = data;
		})
		.error(function(data, status, headers, config)
		{
			// TODO: Error-handling.
		});

	// Toggle the value of showViewerOptions.
	$scope.toggleViewerOptions = function()
	{
		$scope.showViewerOptions = ! $scope.showViewerOptions;
	}
});

spherecastApp.factory('getProject', ['$http', function($http)
{
	return	{
		projectJSON: function(projectId)
		{
			return $http.get('/api/project/' + projectId);
		}
	}
}]);

// spherecastApp.controller('ViewEmbedController', function($scope, projectFactory)

// See http://stackoverflow.com/questions/21667613/in-angular-how-to-pass-json-object-array-into-directive
spherecastApp.controller('ViewEmbedController', ['$scope', 'getProject', '$http', '$routeParams', 'Modernizr', function($scope, getProject, $http, $routeParams, Modernizr)
{
	$scope.embedded = true;
	$scope.canvasWidth = '100%';
	$scope.canvasHeight = '100%';
    $scope.dofillcontainer = true;
    $scope.scale = 1;
    $scope.vrMode = true;

	$scope.showShareBlock = true;
	$scope.toggleShareBlock = function()
	{
		$scope.showShareBlock = $scope.showShareBlock === false ? true : false;
	};

	$scope.selectShareText = function($event)
	{
		$event.target.select();
	};

	// TODO: Use a hidden button as the trigger for fullscreen, and process it in a $scope.watch().
	$scope.toggleFullscreen = function()
	{
	}

	if (Modernizr.hasEvent('deviceOrientation'))
//	if (window.DeviceOrientationEvent || window.OrientationEvent || typeof window.onorientationchange != 'undefined')
	{
		$scope.vrMode = true;
	}

//	$scope.json = projectFactory.getProjectJSON(1);
/*
	var url = '/api/project/' + $routeParams.projectId + '/scenes';
	$http.get(url)
		.success(function(data)
		{
			$scope.activeProject = data;
		})
		.error(function()
		{
			// TODO: Error-handle.
		}
	);
*/
/*
	getProject.projectJSON($routeParams.projectId).success(function(data)
	{
		alert(data);
		$scope.activeProjectJSON = data;
		console.log($scope.activeProjectJSON);
	});
*/
/*
	$http.get('/api/project/' + $routeParams.projectId).success(function(data)
	{
		$scope.activeProject = data;
		console.log($scope.activeProject);
	});
*/
	if ($routeParams.projectId == 1)
	{
		$scope.activeProject = projectjson1;
	}
	else if ($routeParams.projectId == 2)
	{
		$scope.activeProject = projectjson2;
	}
	else if ($routeParams.projectId == 3)
	{
		$scope.activeProject = projectjson3;
	}
	else if ($routeParams.projectId == 4)
	{
		$scope.activeProject = projectjson4;
	}
	else if ($routeParams.projectId == 5)
	{
		$scope.activeProject = projectjson5;
	}
	else if ($routeParams.projectId == 6)
	{
		$scope.activeProject = projectjson6;
	}
}]);

/*
spherecastApp.service('getProject', function(projectId)
{
	var url = '/api/project/' + projectId + '/scenes';
	var deferred = $q.defer();
	$http({
		method: 'JSONP',
		url: url
	}).success(function(data)
	{
		deferred.resolve(data);
	}).error(function()
	{
		deferred.reject('Could not retrieve project.');
	});
	return deferred.promise;
});
*/

spherecastApp.config(function($routeProvider)
{
	$routeProvider
		.when('/new',
		{
			templateUrl: 'partials/projects.html',
			controller: 'ProjectController'
		})
		.when('/projects',
		{
			templateUrl: 'partials/projects.html',
			controller: 'ProjectController'
		})
		.when('/scenes/:projectId',
		{
			templateUrl: 'partials/scenes.html',
			controller: 'SceneController'
		})
		.when('/:projectId',
		{
			templateUrl: 'partials/viewer.html',
			controller: 'ViewEmbedController'
		})
		.when('/:projectId/edit',
		{
			templateUrl: 'partials/projects.html',
			controller: 'ProjectController'
		})
		.otherwise({
			redirectTo: '/new'
	})
});

/*
spherecastApp.directive('ngSparkline', function()
{
	return {
		get: { method: 'JSONP' },
		params: { callback: 'JSON_CALLBACK' },
		restrict: 'E',
		require: '',
		scope: {
			ngCity: '@'
		},
		template: '<div class="sparkline">Weather</div>',
		controller: ['$scope', '$http', function($scope, $http)	{
			$scope.getTemp = function(city)	{
				$http(
				{
					method: 'JSONP',
					url: url + city
				}).success(function(data) {
					var weather = [];
				    angular.forEach(data.list, function(value)
					{
						weather.push(value);
					});
				    $scope.weather = weather;
				});
			}
		}],
		link: function(scope, iElement, iAttrs, ctrl) {
			scope.getTemp(iAttrs.ngcity);
		}
	}
});
*/