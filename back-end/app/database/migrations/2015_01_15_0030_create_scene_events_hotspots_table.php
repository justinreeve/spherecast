<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSceneEventsHotspotsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sceneevents_hotspots', function(Blueprint $table)
		{
			$table->integer('eventId');
			$table->decimal('positionX', 5, 3)->default(0.0);
			$table->decimal('positionY', 5, 3)->default(0.0);
			$table->decimal('positionZ', 5, 3)->default(0.0);
			$table->decimal('radius', 5, 3)->decimal(20.0);
			$table->integer('duration')->default(0);

			// If true, prevent other hotspots from initiating while this hotspot's actions are executing.
			$table->boolean('blockOtherHotspots')->default(false);

			// A media item (typically a spritemap) that plays while the hotspot is being triggered, but
			// before the actions are added to the execution queue.
			$table->integer('countdownMedia')->nullable();
			$table->text('countdownMediaParams')->nullable();

			// Indexes.
			$table->primary('eventId');

			// Foreign keys.
			$table->foreign('eventId')
				->references('id')->on('sceneevents')
				->onDelete('cascade')
				->onUpdate('cascade');

			// created_at, updated_at DATETIME
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sceneevents_hotspots', function(Blueprint $table)
		{
		});
	}

}
