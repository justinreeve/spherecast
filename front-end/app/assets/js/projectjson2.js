var projectjson2 = {
	id: 2,
	shortname: 'comic',
	title: 'Comic',
	subtitle: '',
	description: '',
	createdby: 1,
	timecreated: 0,
	startScene: 1,

	hotspotProfiles: [							// These are the default hotspot profiles per mode (currently "standard" or "VR" mode); this indicate how hotspots can be triggered by default, but can be overridden in specific hotspot "modeProfiles" profiles.
	{
		modeType: 'standard',
		activateByLook: false,
		activateByClick: true,
		activateByHover: false,					// Mouse hover.
		activateByDoubleTap: false,
		activateByController: false,
		activateByVoice: false,
	},
	{
		modeType: 'vr',
		activateByLook: true,
		activateByClick: false,
		activateByHover: false,
		activateByDoubleTap: false,
		activateByController: false,
		activateByVoice: false,
	}],

	hotspotProfileStandard: 
	{
		activateByLook: false,
		activateByClick: true,
		activateByHover: false,
		activateByDoubleTap: false,
		activateByController: false,
		activateByVoice: false,
	},

	hotspotProfileVR:
	{
		activateByLook: true,
		activateByClick: false,
		activateByHover: false,
		activateByDoubleTap: false,
		activateByController: false,
		activateByVoice: false,
	},

	globalVariables: [],

	prefetchAssets:
	{
		video: [
		{
			id: 21,
			sceneid: 1,
			path: '/files/7d/7d0053558759ec36b10b4fbbc9c0a8e3e7eaa358',
			priority: 1,
			type: 'video',
		}],

		sceneManifest: [
		{
			sceneid: 1,
			paths: [
			{
				id: 'video_21',
				src: '/files/7d/7d0053558759ec36b10b4fbbc9c0a8e3e7eaa358',
				assetType: 'video',
			}],
		}],
	},

	scenes: [
	{
		id: 1,
		shortname: 'eribuibni45nnvuf',
		projectid: 2,
		sequence: 1,
		parent: 0,
		title: 'Lake Shore',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		initialCameraX: 75.789,
		initialCameraY: -18.672,
		initialCameraZ: 61.703,
//		initialCameraX: 0.5,
//		initialCameraY: 0.5,
//		initialCameraZ: 0.5,
		backgroundFile:
		{
			fileId: 93,
			filePath: '/files/65/6584f55dfe064c60b331195a9ad0e5b6536a5644',
			fileType: 'image',
		},
		embedid: '0',
		createdby: '1',
		lockedby: '0',
		lockedtime: '0',
		timecreated: '0',
		timemodified: '0',

		startEvents: [
		{
			id: 100,
			visible: 0,
			overridable: 1,
			actions: [
			{
				actionType: 'spritemap',
				actionSequence: 1,
				actionContinue: true,

				// TODO: If actionPlacement is not specified, but X, Y, Z is specified, assume it's fixed. Otherwise, it's an overlay.
				actionPlacement: 'fixed',
				positionX: 87.880,
				positionY: -46.844,
				positionZ: -4.583,

				media:
				{
					id: 88,
					fileScaleX: 1.25,
					fileScaleY: 0.675,
					fileType: 'spritemap',
					fileIdLow: 88,
					fileIdMedium: 88,
					fileIdHigh: 88,
					fileIdUltra: 88,
					files: [
					{
						fileId: 88,
						filePath: '/files/d0/d01f359d3b40fb617095c5e83d652eb852c7d5b9',
						fileType: 'spritemap',
						fileHeight: 2052,
						fileWidth: 946,
					}],
				},

				spritemapTilesNum: 132,
				spritemapTilesHorizontal: 12,
				spritemapTilesVertical: 11,
				spritemapHeight: 2052,
				spritemapWidth: 946,
				spritemapDuration: 7000,			// TODO: If not specified, this should simply be the "duration" of the hotspot; divide this by the number of tiles to figure out how long to display each tile.
				spritemapRepeat: true,				// Don't repeat the animation continuously.
				spritemapShowFirstAsFixed: true,	// Use the first item in the sequence as a fixed label.
				spritemapTileSequence: [			// An ordered sequence of all the tiles we'll be playing in the animation, numbered left-to-right, top-to-bottom. To just have a static image, we could just have one tile listed in this, then the spritemap could be used, for example, for a character that moves around at different intervals.
					0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
					11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
					21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
					31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
					41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
					51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
					61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
					71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
					81, 82, 83, 84, 85, 86, 87, 88, 89, 90,
					91, 92, 93, 94, 95, 96, 97, 98, 99, 100,
					101, 102, 103, 104, 105, 106, 107, 108, 109, 110,
					111, 112, 113, 114, 115, 116, 117, 118, 119, 120,
					121, 122, 123, 124, 125, 126, 127, 128, 129
				],
			},
/*
			{
				actionType: 'spritemap',
				actionSequence: 2,
				actionContinue: true,

				actionPlacement: 'fixed',
				positionX: 67.679,
				positionY: -51.796,
				positionZ: 51.504,

				media:
				{
					id: 90,
					fileScaleX: 1.0,
					fileScaleY: 1.0,
					fileType: 'spritemap',
					fileIdLow: 90,
					fileIdMedium: 90,
					fileIdHigh: 90,
					fileIdUltra: 90,
					files: [
					{
						fileId: 90,
						filePath: '/files/c9/c92ee03b9196270699dd572f6aabdf5e7bde3556',
						fileType: 'spritemap',
						fileHeight: 960,
						fileWidth: 600,
					}],
				},

				spritemapTilesNum: 40,
				spritemapTilesHorizontal: 8,
				spritemapTilesVertical: 5,
				spritemapHeight: 960,
				spritemapWidth: 600,
				spritemapDuration: 7000,			// TODO: If not specified, this should simply be the "duration" of the hotspot; divide this by the number of tiles to figure out how long to display each tile.
				spritemapRepeat: true,				// Don't repeat the animation continuously.
				spritemapShowFirstAsFixed: true,	// Use the first item in the sequence as a fixed label.
				spritemapTileSequence: [			// An ordered sequence of all the tiles we'll be playing in the animation, numbered left-to-right, top-to-bottom. To just have a static image, we could just have one tile listed in this, then the spritemap could be used, for example, for a character that moves around at different intervals.
					8, 9, 10, 11, 12, 13, 14, 15,
					8, 9, 10, 11, 12, 13, 14, 15,
					8, 9, 10, 11, 12, 13, 14, 15,
					8, 9, 10, 11, 12, 13, 14, 15,
					8, 9, 10, 11, 12, 13, 14, 15,
				],
			},
			{
				actionType: 'spritemap',
				actionSequence: 3,
				actionContinue: true,

				actionPlacement: 'fixed',
				positionX: 98.486,
				positionY: -8.787,
				positionZ: 10.992,

				media:
				{
					id: 90,
					fileScaleX: 0.5,
					fileScaleY: 0.5,
					fileType: 'spritemap',
					fileIdLow: 90,
					fileIdMedium: 90,
					fileIdHigh: 90,
					fileIdUltra: 90,
					files: [
					{
						fileId: 90,
						filePath: '/files/c9/c92ee03b9196270699dd572f6aabdf5e7bde3556',
						fileType: 'spritemap',
						fileHeight: 960,
						fileWidth: 600,
					}],
				},

				spritemapTilesNum: 40,
				spritemapTilesHorizontal: 8,
				spritemapTilesVertical: 5,
				spritemapHeight: 960,
				spritemapWidth: 600,
				spritemapDuration: 7000,			// TODO: If not specified, this should simply be the "duration" of the hotspot; divide this by the number of tiles to figure out how long to display each tile.
				spritemapRepeat: true,				// Don't repeat the animation continuously.
				spritemapShowFirstAsFixed: true,	// Use the first item in the sequence as a fixed label.
				spritemapTileSequence: [			// An ordered sequence of all the tiles we'll be playing in the animation, numbered left-to-right, top-to-bottom. To just have a static image, we could just have one tile listed in this, then the spritemap could be used, for example, for a character that moves around at different intervals.
					24, 25, 26, 27, 28, 29,
					24, 25, 26, 27, 28, 29,
					24, 25, 26, 27, 28, 29,
					24, 25, 26, 27, 28, 29,
					24, 25, 26, 27, 28, 29,
					24, 25, 26, 27, 28, 29,
					24, 25, 26, 27, 28, 29,
					24, 25, 26, 27, 28, 29,
					24, 25, 26, 27, 28, 29,
					24, 25, 26, 27, 28, 29,
					24, 25, 26, 27, 28, 29,
					24, 25, 26, 27, 28, 29,
				],
			},
			{
				actionType: 'spritemap',
				actionSequence: 4,
				actionContinue: true,

				actionPlacement: 'fixed',
				positionX: 83.636,
				positionY: -12.731,
				positionZ: 52.708,

				media:
				{
					id: 91,
					fileScaleX: 0.75,
					fileScaleY: 0.75,
					fileType: 'spritemap',
					fileIdLow: 91,
					fileIdMedium: 91,
					fileIdHigh: 91,
					fileIdUltra: 91,
					files: [
					{
						fileId: 91,
						filePath: '/files/98/989f909d9b2e777901cb53b5958c22e51822ade3',
						fileType: 'spritemap',
						fileHeight: 900,
						fileWidth: 900,
					}],
				},

				spritemapTilesNum: 32,
				spritemapTilesHorizontal: 6,
				spritemapTilesVertical: 6,
				spritemapHeight: 900,
				spritemapWidth: 900,
				spritemapDuration: 1500,			// TODO: If not specified, this should simply be the "duration" of the hotspot; divide this by the number of tiles to figure out how long to display each tile.
				spritemapRepeat: true,				// Don't repeat the animation continuously.
				spritemapShowFirstAsFixed: true,	// Use the first item in the sequence as a fixed label.
				spritemapTileSequence: [			// An ordered sequence of all the tiles we'll be playing in the animation, numbered left-to-right, top-to-bottom. To just have a static image, we could just have one tile listed in this, then the spritemap could be used, for example, for a character that moves around at different intervals.
					10, 11, 12, 13, 14, 15, 16, 17,
					18, 19, 20, 21, 22, 23,
				],
			},
			{
				actionType: 'spritemap',
				actionSequence: 5,
				actionContinue: true,

				actionPlacement: 'fixed',
				positionX: 51.687,
				positionY: -12.726,
				positionZ: 84.183,

				media:
				{
					id: 92,
					fileScaleX: 0.75,
					fileScaleY: 0.75,
					fileType: 'spritemap',
					fileIdLow: 92,
					fileIdMedium: 92,
					fileIdHigh: 92,
					fileIdUltra: 92,
					files: [
					{
						fileId: 92,
						filePath: '/files/40/4085db885d825c474aebdf4368fafd698b890ad7',
						fileType: 'spritemap',
						fileHeight: 665,
						fileWidth: 360,
					}],
				},

				spritemapTilesNum: 21,
				spritemapTilesHorizontal: 7,
				spritemapTilesVertical: 3,
				spritemapHeight: 665,
				spritemapWidth: 360,
				spritemapDuration: 3000,			// TODO: If not specified, this should simply be the "duration" of the hotspot; divide this by the number of tiles to figure out how long to display each tile.
				spritemapRepeat: true,				// Don't repeat the animation continuously.
				spritemapShowFirstAsFixed: true,	// Use the first item in the sequence as a fixed label.
				spritemapTileSequence: [			// An ordered sequence of all the tiles we'll be playing in the animation, numbered left-to-right, top-to-bottom. To just have a static image, we could just have one tile listed in this, then the spritemap could be used, for example, for a character that moves around at different intervals.
					0, 1, 2, 3, 4, 5, 6,
					7, 8, 9, 10, 11, 12, 13,
					14, 15, 16, 17, 18, 19, 20,
					19, 18, 17, 16, 15, 14, 13, 12,
				],
			}
*/
			],
		},
/*
		{
			id: 200,
			visible: 0,
			overridable: 1,
			actions: [
			{
				id: 1000,
				actionType: 'spritemap',
				actionSequence: 1,
				actionContinue: true,
				actionDuration: 3000,
				actionPlacement: 'fixed',
				positionX: 79.509,
				positionY: -8.992,
				positionZ: -59.297,

				media:
				{
					id: 91,
					fileScaleX: 0.75,
					fileScaleY: 0.75,
					fileType: 'spritemap',
					fileIdLow: 91,
					fileIdMedium: 91,
					fileIdHigh: 91,
					fileIdUltra: 91,
					files: [
					{
						fileId: 91,
						filePath: '/files/98/989f909d9b2e777901cb53b5958c22e51822ade3',
						fileType: 'spritemap',
						fileHeight: 900,
						fileWidth: 900,
					}],
				},

				spritemapTilesNum: 32,
				spritemapTilesHorizontal: 6,
				spritemapTilesVertical: 6,
				spritemapHeight: 900,
				spritemapWidth: 900,
				spritemapDuration: 1500,			// TODO: If not specified, this should simply be the "duration" of the hotspot; divide this by the number of tiles to figure out how long to display each tile.
				spritemapRepeat: false,				// Don't repeat the animation continuously.
				spritemapShowFirstAsFixed: true,	// Use the first item in the sequence as a fixed label.
				spritemapTileSequence: [			// An ordered sequence of all the tiles we'll be playing in the animation, numbered left-to-right, top-to-bottom. To just have a static image, we could just have one tile listed in this, then the spritemap could be used, for example, for a character that moves around at different intervals.
					10, 11, 12, 13, 14, 15, 16, 17,
					18, 19, 20, 21, 22, 23,
				],
			},
			{
				id: 1001,
				actionType: 'audio',
				actionItemId: 58,
				actionMedia: {},
				actionVolume: 1.0,
				actionFile:
				{
					fileId: 58,
					filePath: '/files/7f/7f6867a50c4004ea66a05726ddb855d3e0d08029',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				id: 1002,
				actionType: 'spritemap',
				actionTarget: 1000,					// This indicates another action in the scene (doesn't have to be the same event sequence) is being directly targeted.
				actionSequence: 3,
				actionContinue: false,
				actionDuration: 3000,
				actionPlacement: 'fixed',
				positionX: 92.838,
				positionY: -4.972,
				positionZ: -35.947,

				media:
				{
					id: 91,
					fileScaleX: 0.75,
					fileScaleY: 0.75,
					fileType: 'spritemap',
					fileIdLow: 91,
					fileIdMedium: 91,
					fileIdHigh: 91,
					fileIdUltra: 91,
					files: [
					{
						fileId: 91,
						filePath: '/files/98/989f909d9b2e777901cb53b5958c22e51822ade3',
						fileType: 'spritemap',
						fileHeight: 900,
						fileWidth: 900,
					}],
				},

				spritemapTilesNum: 32,
				spritemapTilesHorizontal: 6,
				spritemapTilesVertical: 6,
				spritemapHeight: 900,
				spritemapWidth: 900,
				spritemapDuration: 1500,			// TODO: If not specified, this should simply be the "duration" of the hotspot; divide this by the number of tiles to figure out how long to display each tile.
				spritemapRepeat: true,				// Don't repeat the animation continuously.
				spritemapShowFirstAsFixed: true,	// Use the first item in the sequence as a fixed label.
				spritemapTileSequence: [			// An ordered sequence of all the tiles we'll be playing in the animation, numbered left-to-right, top-to-bottom. To just have a static image, we could just have one tile listed in this, then the spritemap could be used, for example, for a character that moves around at different intervals.
					10, 11, 12, 13, 14, 15, 16, 17,
					18, 19, 20, 21, 22, 23,
				],
			}],
		}
*/
		],

		// End events only occur immediately before a scene jump. The jump takes place when they have all finished.
		endEvents: [
		{
		}],

		// If a hotspot is not "overridable" that means no other hotspots can be activated while the event is playing its actions.
		// If it is "overridable" as soon as as hotspot event is triggered (after the arc completes its rotation), the overridable
		// event will stop playing.

		// If there's an unmet condition on a hotspot event, don't even process the hotspot countdown. This is how hotspots can
		// be deactivated, by using global variables.

		// The toggleOtherHotspots field deactivates all other hotspots in the scene until the same hotspot is activated again.
		// Although this is intended to be used as a nav panel trigger to turn hotspots on/off, it could also be used in conjunction
		// with global variables to restrict access to different parts of the scene until conditions are met.
		hotspots: [
		{
		}],
	}],
};
