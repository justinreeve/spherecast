<?php
class Media extends Eloquent
{
	protected $id;
	protected $projectId;
	protected $type;
	protected $filename;
	protected $filepath;
	protected $scaleX;
	protected $scaleY;
	protected $createdBy;
	protected $params;

	protected $guarded = array('id');
	protected $table = 'media';

	public function project()
	{
		return $this->belongsTo('Project', 'projectId', 'id');
	}

	public function actions()
	{
//		return $this->belongsToMany('Action', 'actions_files', 'fileid', 'actionid');
	}
/*
	public function mediable()
	{
		return $this->morphTo();
	}
*/
	public function mediaFiles()
	{
		return $this->hasMany('MediaFile', 'mediaId', 'id');
	}

	public function folder()
	{
		return $this->belongsTo('MediaFolder', 'folderId', 'id');
	}
}
?>