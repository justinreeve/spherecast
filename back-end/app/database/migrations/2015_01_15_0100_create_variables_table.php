<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVariablesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('variables', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('projectId');
			$table->string('name');
			$table->string('defaultValue')->nullable();

			// Indexes.
			$table->unique(array('projectId', 'name'));
			$table->index('projectId');

			// Foreign keys.
			$table->foreign('projectId')
				->references('id')->on('projects')
				->onDelete('cascade')
				->onUpdate('cascade');

			// created_at, updated_at DATETIME
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('variables', function(Blueprint $table)
		{
		});
	}

}
