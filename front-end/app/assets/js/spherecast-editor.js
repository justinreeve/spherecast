'use strict';

angular.module('spherecastApp')
  .directive('spherecastEditor', function() {
    return {
      restrict: 'A',
      scope: false,
      link: function(scope, element, attrs) {

        var camera,
        	scene, overlayScene, hotspotOverlayScene, labelOverlayScene, textOverlayScene, imageOverlayScene, videoOverlayScene,
        	renderer, shadowMesh, icosahedron, light,
			mouseX = 0, mouseY = 0,
			contW = (scope.fillcontainer) ? element[0].clientWidth : scope.width,
			contH = scope.height,
			windowHalfX = contW / 2,
			windowHalfY = contH / 2,
			materials = {};
		var cameraSphereIntersection;
		var controls;
		var stereoEffect;
		var domElement;
		var sceneViewerCanvas;
		var cameraOrientationCanvas;
		var raycasterOrientationCanvas;
		var activeScene = {};
		var activeSceneJump = null;			// A flag which indicates we're ready to jump to another scene. 
		var activeHotspot = null;			// Only one hotspot can be active (i.e. in the process of being triggered) at a time.
		var lastHotspot = null;
		var activeSceneAssets = {};			// TODO: Get rid of this, because it doesn't seem like we're using it anymore. It was "Anything that has been loaded on to the scene via actions."
		var activeActionSequences = [];		// TODO: Get rid of this, because it doesn't seem like we're using it anymore.
		var activeEvents = [];
		var audioPlayer;
		var videoPlayer;
		var videoCanvasContext;
		var videoTexture;
		var textCanvases = [];
		var imageCanvases = [];
		var videoCanvases = [];
		var textSprites = [];
		var imageSprites = [];
		var videoSprites = [];
		var variables = [];					// An array of user-defined global variables.
		var variablesIndexes = [];			// An array of the index of each variable in the "variables" variable (lulz).
		var spritemaps = [];
		var spritemapIndexes = [];

		var preloaderManifest;
		var currentPreloadingScene;
		var preloadedScenes = [];
		var continueScene = true;			// A flag which indicates whether we can keep going through the scene, or need to wait to load other media.

		var activatedAudioPlayer = false;	// Indicates whether or not we've done the initial trigger for audio (since mobile requires it before playback).
		var activatedVideoPlayer = false;	// Indicates whether or not we've done the initial trigger for video (since mobile requires it before playback).

		// TODO: Use a JSON of all hotspots retrieved from the database.
		var hotspots = [];
		var hotspotCountdowns = [];
		var clock = new THREE.Clock();		// TODO: Use the clock to count hotspot durations.

		var rayTime = 0;

		scope.init = function()
		{
			clock.start();
			rayTime = clock.getElapsedTime();

			// Primary scene
			scene = new THREE.Scene();

			overlayScene = new THREE.Scene();

			// Overlay scene for labels. Labels are always for items that must always be on top, such as
			// text labels. This does not count image labels, which are actually considered hotspot images,
			// and are presented on hotspotOverlayScene, which appears one level below labelOverlayScene.
			labelOverlayScene = new THREE.Scene();

			videoOverlayScene = new THREE.Scene();
			imageOverlayScene = new THREE.Scene();

			// Renderer
			renderer = new THREE.WebGLRenderer();
			renderer.setClearColor(0x000000);
			renderer.setSize(contW, contH);
			renderer.autoClear = false;
			domElement = renderer.domElement;

			// Camera
			camera = new THREE.PerspectiveCamera(90, windowHalfX / windowHalfY, 0.001, 10000);
			camera.position.set(0, 0, 0);
			scene.add(camera);

			// Crosshairs HUD
			scope.crosshairs();

			// Element is provided by the angular directive
			sceneViewerCanvas = element[0].appendChild(domElement);
			sceneViewerCanvas.id = 'sceneViewerCanvas';

			// Controls
			controls = new THREE.OrbitControls(camera, domElement);
			controls.rotateUp(Math.PI / 4);
			controls.target.set(
				camera.position.x + 0.1,
				camera.position.y,
				camera.position.z
			);
//			controls.connect();
			controls.update();

			// Continually update the camera orientation with a self-calling function.
			scope.drawCameraOrientation();
			scope.drawRaycasterOrientation();	// We're not bothering to create this after loadPage(), since we don't want hotspots to be activated as soon as the scene loads up anyway.

			// We might have to restructure the <audio> and <video> tags to include all the sources first,
			// if media items aren't playing as they should.
			scope.createAudioPlayer();
			scope.createVideoPlayer();

			// Don't continue if the scope doesn't have an activeScene yet.
			if (typeof scope.activeScene === 'undefined')
				return;

			// Confirm the microphone now, if applicable, so the user isn't prompted to do it later when
			// they're in the middle of a scene.
//			scope.confirmMicrophone();

			scope.setInitialVariables();

			scope.toggleMode();

			if (typeof scope.activeScene.prefetchAssets !== 'undefined')
			{
				preloaderManifest = scope.activeScene.prefetchAssets.sceneManifest;
				currentPreloadingScene = scope.activeScene.startScene;
				scope.prefetch(currentPreloadingScene);
			}

			// The "click" event, in addition to activating full-screen mode on mobile, should also activate
			// playback of audio and video. We're putting this here instead of the "deviceorientation" event
			// since we may not always want VR mode to be activated on some phones or tablets.
/*
			window.addEventListener('click', function()
			{
				// TODO: Don't let this interfere with any events at the beginning which play audio. When
				// actually playing a complete VR spherecast or previewing a scene in VR mode, a landing
				// page should always come up first and prompt a keypress to go to fullscreen (and activate
				// audio and video playback).
				if (activatedAudioPlayer == false)
				{
					if (audioPlayer.paused)
					{
						// Assign the first audio file to this player.

						// Start and immediately stop the player.
						audioPlayer.play();
						audioPlayer.pause();
					}
					activatedAudioPlayer = true;
				}

				if (activatedVideoPlayer == false)
				{
					if (videoPlayer.paused)
					{
						// Assign the first video file to this player.

						// Start and immediately stop the player.
						videoPlayer.play();
						videoPlayer.pause();
					}
					activatedVideoPlayer = true;
				}
			});
*/
			window.addEventListener('resize', scope.onWindowResize, false);

			console.log(scope.activeScene);

			scope.jumpToScene(scope.activeScene);
		};



		// Determine if there's at least one voice event in a scene. If so, we need to confirm microphone access.
		scope.confirmMicrophone = function()
		{
		}



		// Assign the variables to their defaults.
		// TODO: If we're loading a saved session, load the variable values from the database.
		scope.setInitialVariables = function()
		{
		}



		scope.jumpToScene = function(newScene)
		{
			// A new scene was actually found, so we can remove the current one's background and add
			// the new one, if one exists (could be a blank screen).
			if (activeScene && activeScene.background)
			{
				scene.remove(activeScene.background);
			}

			// Stop any audio.
			if (typeof audioPlayer != 'undefined')
			{
				audioPlayer.pause();
				audioPlayer.currentTime = 0;
			}

			// Clear out any scene activity.
			activeHotspot = null;
			lastHotspot = null;
			hotspots = [];
			activeActionSequences = [];
			activeEvents = [];

			// TODO: Clear out image and text canvases.

			// Clear out the overlay scene.
			// TODO: Is there a "better" way of clearing the overlay scene?
			if (overlayScene)
			{
				overlayScene = new THREE.Scene();
			}

			// Assign the active scene.
			activeScene = newScene;

			// Create arrays of the sprites that will end up being used, so we know what to delete when we're jumping
			// to a new scene.
			// TODO: Do we want to just use native three.js and removeChild() functions and get rid of everything with
			// certain classes from the scene?
			activeScene.textSprites = [];
			activeScene.imageSprites = [];
			activeScene.videoSprites = [];
			activeScene.spritemaps = [];

			// TODO: Make sure all assets are prefetched before loading the scene.

			// Load the scene background.
			if (activeScene.backgroundFile !== null || activeScene.backgroundMedia !== null)
			{
				if (typeof activeScene.backgroundMedia !== 'undefined')
				{
					if (typeof activeScene.assets.media[activeScene.backgroundMedia] !== 'undefined')
					{
						var backgroundMedia = activeScene.assets.media[activeScene.backgroundMedia];
						if (backgroundMedia.type == 'backgroundimage')
						{
							// TODO: Look at the width and height of the background image to determine how to map the texture.
							// If it's a 2:1 aspect ratio, assume it's an equirectangular photosphere. If wider than
							// 2:1, assume it's a panorama, and add enough blank space on the top and bottom to compensate.
	
							// It's not clear if three.js is capable of mapping only on a certain part of the sphere. We
							// may just turn the background into a "child" of the main background image uploaded, and handle
							// this in the creation utility itself so it has the proper amount of blank space around it for
							// a 2:1 aspect ratio.

							var backgroundFile = scope.getMediaFile(backgroundMedia);
							console.log(backgroundFile);

							if (backgroundFile)
							{
								var backgroundTexture = THREE.ImageUtils.loadTexture(backgroundFile.filepath);

								// TODO: Do we need to make sure the sphere is only created once when the viewer loads?
								var backgroundGeometry = new THREE.SphereGeometry(100, 32, 32);
								var backgroundMaterial = new THREE.MeshBasicMaterial({
										map: backgroundTexture,
										side: THREE.DoubleSide,
								});
								activeScene.background = new THREE.Mesh(
									backgroundGeometry, backgroundMaterial
								);
								activeScene.background.material.side = THREE.DoubleSide;
								activeScene.background.scale.x = -1;
								scene.add(activeScene.background);

								activeScene.backgroundFile = backgroundFile;
							}
						}
					}
				}

				if (typeof activeScene.background !== 'undefined')
				{
					activeScene.background.name = 'sceneBackground';
				}
			}
			else
			{
				// If there's no background, show a blank screen.
			}

			// Set the initial camera bearing if one is specified.
			if (typeof activeScene.initialCameraX !== 'undefined' && typeof activeScene.initialCameraY !== 'undefined' && typeof activeScene.initialCameraZ !== 'undefined')
			{
				var initialOrientation = new THREE.Vector3(activeScene.initialCameraX, activeScene.initialCameraY, activeScene.initialCameraZ);
				camera.up.set(0, 1, 0);
				camera.lookAt(initialOrientation);
			}

			hotspots = activeScene.hotspots;
			scope.drawHotspotLabels();
		}



		function getSceneInSequence(sceneId)
		{
		}



		// TODO: Determine the best file to deliver depending on device, bandwidth, connection speed, current cache,
		// and user preferences.
		scope.getMediaFile = function(media)
		{
			if (typeof media.files !== 'undefined')
			{
				var mediaFile;
				for (var f = 0; f < media.files.length; f++)
				{
					if (media.files[f].id == media.fileIdHigh)
					{
						mediaFile = media.files[f];
						break;
					}
				}
				return mediaFile;
			}
			return null;
		}




        // -----------------------------------
        // Event listeners
        // -----------------------------------
        scope.onWindowResize = function () {

          scope.resizeCanvas();

        };

		// Any label position should be calculated in terms of an offset from the sphere.
		// See http://stackoverflow.com/questions/15079592/how-to-add-label-to-sphere-in-three-js
		// TODO: Create rounded rectangles.
		// See http://stackoverflow.com/questions/1255512/how-to-draw-a-rounded-rectangle-on-html-canvas
		scope.drawHotspotLabels = function()
		{
		}

		scope.createLabelCanvas = function()
		{
		}

		scope.createTransparentLabel = function(text, x, y, z, size, color, style)
		{
		}

		scope.createLabel = function(text, x, y, z, size, color, style)
		{
		}

		var countdownCanvas;
		var countdownContext;
		var countdownImageData;
		var countdownTexture;
		var countdownMaterial;
		var countdownSpriteCanvasContext;

		scope.prefetchMedia = function()
		{
		}



		scope.createAudioPlayer = function()
		{
		}



		// We'll want to create a system where videos can be paused and resumed immediately when other videos
		// load. There might be problems with mobile devices playing more than one video simultaneously since
		// only one video should be running at a time according to the HTML5 spec.

		// We may need to play and immediately stop all video player when first
		scope.createVideoPlayer = function()
		{
		}



		// TODO: Should this be put in a scope.$watch() function?
		scope.executeActions = function()
		{
		}



		scope.createTextCanvas = function()
		{
		}



		scope.createImageCanvas = function()
		{
		}



		// Process voice events.
		// TODO: Gather all the condition variables used for voice events, then monitor when they change.
		// If one does change, instantly restart annyang so the system is looking for the user to speak
		// a new word.
		// TODO: If all the voice events have conditions, test that at least one exists before
		// starting annyang. That way, we're not wasting resources.
		// TODO: Abort the listener when none of the conditions for any of the commands for the scene are met,
		// and start it again when they are, so we're not wasting resources.
		scope.listenVoiceEvents = function()
		{
		}



		scope.executeVoiceCallback = function(e)
		{
		}



		// Don't activate the countdown if the hotspot profile doesn't allow activating by looking.
		// TODO: Allow for more diverse hotspot profiles. Right now we're still using scope.mode, when we should
		// have a full-featured customizable profile instead.
		scope.scanHotspots = function()
		{
		}



		// We don't actually get rid of the stereoEffect (not sure it can be done), but we just
		// render the view differently in render(). We have to make sure to reset the renderer
		// size when we toggle back to standard mode, since StereoEffect changes that.
		scope.toggleMode = function()
		{
			if (scope.vrMode == true)
			{
				if (typeof stereoEffect === 'undefined')
				{
					// Stereoscopic effect
					stereoEffect = new THREE.StereoEffect(renderer);
					stereoEffect.separation = -6.2;
					stereoEffect.setSize(contW, contH);
				}
				camera.fov = 90;
				camera.updateProjectionMatrix();

				// Active the DeviceOrientation controls.
				window.addEventListener('deviceorientation', scope.setOrientationControls, true);
			}
			else
			{
				renderer.setSize(contW, contH);
				camera.fov = 70;
				camera.updateProjectionMatrix();

				// Deactivate the DeviceOrientation controls.
				controls = new THREE.DeviceOrientationControls(camera);
				controls.disconnect();

//				window.removeEventListener('deviceorientation', scope.setOrientationControls, true);
			}
		}



		scope.setOrientationControls = function(e)
		{
			if (!e.alpha) 
			{
				return;
			}
			var alpha = e.alpha.toFixed(2);
			var beta = e.beta.toFixed(2);
			var gamma = e.gamma.toFixed(2);
			var abs = e.absolute;

			controls = new THREE.DeviceOrientationControls(camera);
			controls.connect(clock.getDelta());
			controls.update(clock.getDelta());

			// Remove the listener, since we've already set up the controls and don't need it anymore.
			window.removeEventListener('deviceorientation', scope.setOrientationControls, true);
		}

		// -----------------------------------
		// Updates
		// -----------------------------------
		scope.resizeCanvas = function()
		{
			contW = element[0].clientWidth;
			contH = element[0].clientHeight;

			windowHalfX = contW / 2;
			windowHalfY = contH / 2;

			camera.aspect = contW / contH;
			camera.updateProjectionMatrix();

	        if (scope.vrMode == true)
	        {
		        stereoEffect.setSize(contW, contH);
	        }
	        else
	        {
				renderer.setSize(contW, contH);
	        }
		};

        // -----------------------------------
        // Draw and Animate
        // -----------------------------------
        scope.crosshairs = function()
        {
			var crosshairTexture = THREE.ImageUtils.loadTexture('assets/images/crosshairs04.png');
			var crosshairMaterial = new THREE.SpriteMaterial( { map: crosshairTexture, depthTest: false } );
			var crosshairSprite = new THREE.Sprite(crosshairMaterial);
			//scale the crosshairSprite down in size
			crosshairSprite.scale.set(15, 15, 1.0);
			//add crosshairSprite as a child of our camera object, so it will stay centered in camera's view
			camera.add(crosshairSprite);
			//position sprites by percent X:(100 is all the way to the right, 0 is left, 50 is centered)
			//                            Y:(100 is all the way to the top, 0 is bottom, 50 is centered)
//			var crosshairPercentX = 50;
//			var crosshairPercentY = 50;
//			var crosshairPositionX = (crosshairPercentX / 100) * 2 - 1;
//			var crosshairPositionY = (crosshairPercentY / 100) * 2 - 1;
			crosshairSprite.position.x = 0;
			crosshairSprite.position.y = 0;
			crosshairSprite.position.z = -100;
//			crosshairSprite.position.set(x, y, z);
        }

		scope.drawCameraOrientation = function()
		{
			if (!cameraOrientationCanvas)
			{
				cameraOrientationCanvas = document.createElement('canvas');
				cameraOrientationCanvas.id = 'cameraOrientation';
				cameraOrientationCanvas.width = 640;
				cameraOrientationCanvas.height = 25;
				element[0].appendChild(cameraOrientationCanvas);
			}

			// Get the camera orientation.
			var pLocal = new THREE.Vector3(0, 0, -1);
			var pWorld = pLocal.applyMatrix4(camera.matrixWorld);
			var dir = pWorld.sub(camera.position).normalize()
			dir = pWorld;
			var x = Math.round(dir.x * 100000) / 100000;
			var y = Math.round(dir.y * 100000) / 100000;
			var z = Math.round(dir.z * 100000) / 100000;
			var text = 'X: ' + x + '   Y: ' + y + '   Z: ' + z;

			// Draw the coordinates.
			var ctx = cameraOrientationCanvas.getContext('2d');
			ctx.clearRect(0, 0, cameraOrientationCanvas.width, cameraOrientationCanvas.height);
			ctx.rect(0, 0, cameraOrientationCanvas.width, cameraOrientationCanvas.height);
			ctx.font = '16px Open Sans';
			ctx.strokeStyle = 'black';
			ctx.lineWidth = 3;
			ctx.strokeText(text, 20, 20);
			ctx.fillStyle = 'white';
			ctx.fillText(text, 20, 20);
			window.requestAnimationFrame(scope.drawCameraOrientation);
		}

		// For example of how to detect a "touch" of an object in a scene (for desktop users), see http://stackoverflow.com/questions/25024044/three-js-raycasting-with-camera-as-origin
		// See http://jsfiddle.net/KYnyC/116/
		scope.drawRaycasterOrientation = function()
		{
			if (!raycasterOrientationCanvas)
			{
				raycasterOrientationCanvas = document.createElement('canvas');
				raycasterOrientationCanvas.id = 'raycasterOrientation',
				raycasterOrientationCanvas.width = 640;
				raycasterOrientationCanvas.height = 25;
				element[0].appendChild(raycasterOrientationCanvas);
			}

			// Get the raycaster orientation.
			if (typeof activeScene !== 'undefined' && activeScene.hasOwnProperty('background'))
			{
				var backgroundObject = scene.getObjectByName('sceneBackground');
				var directionVector = new THREE.Vector3(0, 0, -1).applyQuaternion(camera.quaternion);
				var raycaster = new THREE.Raycaster(camera.position, directionVector.normalize());
				var intersection = raycaster.intersectObject(backgroundObject, true);
/*
				if (clock.getElapsedTime() >= rayTime + 3)
				{
					rayTime = clock.getElapsedTime();
//					console.log(camera);
					console.log(intersection);
				}
*/
				if (typeof intersection[0] !== 'undefined')
				{
					var x, y, z, distance;
					var x = intersection[0].point.x.toFixed(3);
					var y = intersection[0].point.y.toFixed(3);
					var z = intersection[0].point.z.toFixed(3);
					var distance = intersection[0].distance.toFixed(3);

					cameraSphereIntersection = { x: x, y: y, z: z };

					var text = 'X: ' + x + '   Y: ' + y + '   Z: ' + z + '   Dist: ' + distance;

					// Draw the coordinates.
					var ctx = raycasterOrientationCanvas.getContext('2d');
					ctx.clearRect(0, 0, raycasterOrientationCanvas.width, raycasterOrientationCanvas.height);
					ctx.rect(0, 0, raycasterOrientationCanvas.width, raycasterOrientationCanvas.height);
					ctx.font = '16px Open Sans';
					ctx.strokeStyle = 'black';
					ctx.lineWidth = 3;
					ctx.strokeText(text, 20, 20);
					ctx.fillStyle = 'white';
					ctx.fillText(text, 20, 20);
				}
			}
			window.requestAnimationFrame(scope.drawRaycasterOrientation);
		}

		scope.animate = function(dt)
		{
			requestAnimationFrame(scope.animate);
//			camera.updateProjectionMatrix();
			controls.update(clock.getDelta());
			scope.render(clock.getDelta());
			scope.update();
		};

		// We'll want to render anything attached to a camera here, since we'll need to attach it
		// differently if VR mode is enabled.
		scope.render = function()
		{
			renderer.clear();

			// TODO: Add billboards to scene for hotspot labels that always face the camera.
			// See http://stackoverflow.com/questions/16001208/how-can-i-make-my-text-labels-face-the-camera-at-all-times-perhaps-using-sprite
/*
			if (videoPlayer.readyState === videoPlayer.HAVE_ENOUGH_DATA)
			{
				if (typeof videoCanvasContext !== 'undefined')
				{
					videoCanvasContext.drawImage(videoPlayer, 0, 0);
					if (typeof videoTexture !== 'undefined' && videoTexture)
					{
						videoTexture.needsUpdate = true;
					}
				}
			}
*/
			// Render the view.
			if (scope.vrMode == true)
			{
				stereoEffect.render(scene, camera);
				stereoEffect.clearDepth();
				stereoEffect.render(overlayScene, camera);
			}
			else
			{
				renderer.render(scene, camera);
				renderer.clearDepth();
				renderer.render(overlayScene, camera);
			}
		};

		scope.update = function()
		{
			var removeSpritemapIndexes = [];

			// Create and update any spritemaps as necessary.
			if (typeof activeScene.spritemaps !== 'undefined' && activeScene.spritemaps != null)
			{
				for (var s = 0; s < activeScene.spritemaps.length; s++)
				{
					// If the spritemap has been flagged as "complete" then add it to the removal queue.
					if (typeof activeScene.spritemaps[s].complete !== 'undefined')
					{
						if (activeScene.spritemaps[s].complete == true)
						{
							removeSpritemapIndexes.push(activeScene.spritemaps[s].id);
						}
					}

					// Update the texture.
					else
					{
						activeScene.spritemaps[s].update();
					}
				}
			}

			for (var r = 0; r < removeSpritemapIndexes.length; r++)
			{
				activeScene.spritemaps.splice(removeSpritemapIndexes[r], 1);
			}
		}

		scope.isFullScreen = function() {
			var fsElem =
				document.fullscreenElement ||
				document.msFullscreenElement ||
				document.mozFullscreenElement ||
				document.webkitFullscreenElement;
			return ((fsElem != null) && (fsElem != undefined));
		}
		
		scope.fullscreen = function() {
			if (element[0].requestFullscreen) {
				element[0].requestFullscreen();
			} else if (element[0].msRequestFullscreen) {
				element[0].msRequestFullscreen();
			} else if (element[0].mozRequestFullScreen) {
				element[0].mozRequestFullScreen();
			} else if (element[0].webkitRequestFullscreen) {
				element[0].webkitRequestFullscreen();
			}
			scope.resizeCanvas();
		}

		// Create a chroma key video material.
		scope.VideoChromaKeyMaterial = function(url, width, height, keyColor)
		{
			THREE.ShaderMaterial.call(this);
		}

		// Create a video object.
		// videoData: videoTransparencyColorRGB, videoHeight, videoWidth, videoScaleX, videoScaleY, videoVolume,
		//		videoInTransition, videoInTransitionDuration, videoOutTransition, videoOutTransitionDuration,
		// For most videos we can just play the video element directly as the texture, which makes it simple.
		// However, if there's a greenscreen alpha setting, we'll need to paint to a canvas first. This should all be
		// stored and handled within the video object, though, so the object has its own play() and stop() functions.
		scope.createVideoObject = function(videoData)
		{
		}

		// Create a spritemap animator object.
		scope.createSpritemapAnimator = function(spritemapData)
		{
		}

        // -----------------------------------
        // Watches
        // -----------------------------------
		scope.$watch('fillcontainer + width + height', function()
		{
			scope.resizeCanvas();
		});

		scope.$watch('scale', function()
		{
//			scope.resizeObject();
		});

		scope.$watch('activeScene', function()
		{
		});

		scope.$watch('vrMode', function()
		{
			scope.toggleMode();
		});

		// Start prefetching assets, beginning with the startScene. Note that on mobile browsers,
		// we require a "click" event before this can happen. Only load one scene's assets at a
		// time, then continue to the next scene in activeProject.prefetchAssets.sceneManifest.

		// When prefetching assets, we should only buffer as much audio and video as we need for a playback,
		// then open the scene. The media items can continue buffering as the user navigates the scene. This
		// will speed up load times significantly for scenes with large audio and video files.
		scope.prefetch = function(sceneid)
		{
			if (typeof preloaderManifest === 'undefined')
			{
				return;
			}

			var queue = new createjs.LoadQueue();

			// Find the scene in the manifest.
			for (var i = 0; i < preloaderManifest.length; i++)
			{
				var sceneAssets = preloaderManifest[i];
				if (sceneAssets.sceneid != sceneid)
				{
					continue;
				}
				else
				{
					queue.addEventListener('fileprogress', function(event)
					{
//						console.log(event);
					});

					queue.addEventListener('fileload', function(event)
					{
//						console.log(event);
					});

					queue.addEventListener('complete', function(event)
					{
//						console.log(event);
					});

					// Add the scene assets.
					for (var a = 0; a < sceneAssets.length; a++)
					{
						queue.loadFile(sceneAssets[a].paths);
					}
					queue.load();

					break;
				}
			}
			return;
		}

		// Initialize the viewer.
		scope.init();
		scope.animate();
      }
    };
});