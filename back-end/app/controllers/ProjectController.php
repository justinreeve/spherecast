<?php
/**
 * Created with php artisan controller:make ProjectController.
 *
 * This controller is used to retrieve entire project data, including all scenes and events.
 * Write methods are specific only to project settings. Other controllers are used for writing
 * data specific to the scope.
 */
class ProjectController extends Controller
{
	protected $prefetchFiles;
	protected $projectAssets;
	protected $projectJumpTargets;

	/**
	 * Send back all resource as JSON.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Response::json(Project::get());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$random = "";
		srand((double) microtime() * 1000000);
		$data = "0123456789abcdefghijklmnopqrstuvwxyz";

		// Create a shortname, and ensure it has alphabetic characters and isn't already being used.
		do
		{
			for ($i = 0; $i < 5; $i++)
			{
				$random .= substr($data, (rand() % (strlen($data))), 1);
			}
		}
		while (is_numeric($random) || Project::where('shortname', $random)->first());

		Project::create(array(
			'shortname' => $random,
			'title' => Input::get('title'),
			'subtitle' => '',
			'description' => Input::get('description'),
			'createdBy' => 0,
			'startScene' => 0,
		));

		return Response::json(array('success' => true));
	}

	/**
	 * Add a jump to the project jump targets.
	 */
	protected function addJumpTarget($sceneId, $targetSceneId)
	{
		if (!isset($this->projectJumpTargets))
			$this->projectJumpTargets = array();

		if (!isset($this->projectJumpTargets[$sceneId]))
			$this->projectJumpTargets[$sceneId] = array();

		// Add jump if it's not already in the array.
		if (!in_array($targetSceneId, $this->projectJumpTargets[$sceneId]))
			$this->projectJumpTargets[$sceneId][] = $targetSceneId;

		return;
	}

	/**
	 * Add an item to the project assets.
	 */
	protected function addMediaAsset($media, $sceneId, $priority)
	{
		// Create media array if it doesn't exist.
		if (!isset($this->media))
			$this->media = array();

		// Add media if it's not already in the array.
		$add = true;
		foreach ($this->media as $item)
		{
			if ($item->id == $media->id)
			{
				$add = false;
				break;
			}
		}
		if ($add == true)
			$this->media[] = $media;

		// Create assets object if it doesn't exist.
		if (!isset($this->assets))
			$this->assets = new stdClass;

		// Create the scenes object if it doesn't exist.
		if (!isset($this->assets->scenes))
			$this->assets->scenes = new stdClass;

		// Create the scene array if it doesn't exist.
		if (!isset($this->assets->scenes->$sceneId))
			$this->assets->scenes->$sceneId = array();

		// Create the priority levels for the scene assets.
		if (!isset($this->assets->priorities))
		{
			$this->assets->priorities = new stdClass;
			for ($p = 1; $p <= 5; $p++)
				$this->assets->priorities->$p = array();
		}

		// Create the media object if it doesn't exist.
		$mediaId = $media->id;
		if (!isset($this->assets->media))
			$this->assets->media = new stdClass;

		// Create asset object.
		// TODO: Use the assets object as the exclusive source for the viewer to retrieve any media.
//		echo '<pre>'; print_r($media); echo '</pre>';

		$this->assets->media->$mediaId = $media;
		if (!in_array($media->id, $this->assets->scenes->$sceneId))
			array_push($this->assets->scenes->$sceneId, $media->id);
		if (!in_array($media->id, $this->assets->priorities->$priority))
			array_push($this->assets->priorities->$priority, $media->id);
/*
		if ($priority == 1)
			$this->assets->priorities->priority1[] = $media->id;
		else if ($priority == 2)
			$this->assets->priorities->priority2[] = $media->id;
		else if ($priority == 3)
			$this->assets->priorities->priority3[] = $media->id;
		else if ($priority == 4)
			$this->assets->priorities->priority4[] = $media->id;
		else if ($priority == 5)
			$this->assets->priorities->priority5[] = $media->id;
*/
		// Sort assets in scene.
//		usort($this->projectAssets[$sceneId]->media, array('ProjectController', 'sortMediaAssets'));
	}

	/**
	 * If the priority is the same, sort by filesize.
	 */
	private static function sortMediaAssets($a, $b)
	{
		if ($a->media->priority == $b->media->priority)
		{
			if ($a->media->files[0]->filesize == $b->media->files[0]->filesize)
				return 0;
			return $a->media->files[0]->filesize < $b->media->files[0]->filesize ? -1 : 1;
		}
		return ($a->media->priority < $b->media->priority) ? -1 : 1;
	}

	/**
	 * Present the project assets in a JSON-friendly structure.
	 *
	 * @return string
	 */
	protected function organizeMediaAssets()
	{
		if (!is_array($this->assets))
			return array();

		$organizedAssets = new stdClass;
		foreach ($this->assets as $sceneId => $scene)
		{
//			echo '<pre>'; print_r($asset); echo '</pre>';
			$organizedAssets->items[] = $scene;
		}
		return $organizedAssets;
	}

	/**
	 * Re-route to the first scene in the project.
	 * TODO: We shouldn't be calling one controller from another. Fix this.
	 */
	public function startScene($id)
	{
		if (!is_numeric($id))
			$project = Project::with('scenes')
								->where('projects.shortname', $id)
								->first();
		else
			$project = Project::with('scenes')
								->where('projects.id', $id)
								->first();

		// Retrieve the specified start scene.
		if (isset($project->startScene))
		{
			for ($s = 0; $s < count($project->scenes); $s++)
			{
				if ($project->scenes[$s]->id == $project->startScene)
				{
					$shortname = $project->scenes[$s]->shortname;
					break;
				}
			}
		}

		// If we still don't have a scene, retrieve the first scene found in the project.
		if (!isset($shortname))
		{
			$scene = $project->scenes[0];
			$shortname = $scene->shortname;
		}

		// TODO: Do we want to create an initial scene if one doesn't exist?
		if (isset($shortname))
		{
			$app = app();
			$controller = $app->make('SceneController');
			return $controller->callAction('show', array('id' => $shortname));
		}
	}

	/**
	 * Retrieve basic metadata for the project.
	 */
	public function getMetaData($id)
	{
		if (!is_numeric($id))
			$project = Project::where('projects.shortname', $id)->first();
		else
			$project = Project::where('projects.id', $id)->first();

		if (isset($project->startScene))
		{
			$scene = Scene::where('scenes.id', $project->startScene)->first();
			if ($scene && isset($scene->backgroundMedia))
			{
				$imageurl = \Config::get('app.url') . '/api/media/' . $project->id . '/' . $scene->backgroundMedia . '/thumbnail/120';
				$project->imageurl = $imageurl;
			}
		}
		return $project;
	}
	 
	/**
	 * Display the specified resource.
	 * TODO: Do more of the queries within the initial query.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function show($id)
	{
		// If the shortname was passed, use that instead of the id.
		if (!is_numeric($id))
		{
			$projectCache = DB::table('projects_cache')
								->join('projects', 'projects.id', '=', 'projects_cache.projectId')
								->where('projects.shortname', $id)
								->first();
			if (isset($projectCache) && $projectCache->useCache == true && $projectCache->json != null)
			{
				// Don't escape the unicode characters, and transmit them at UTF-8.
				$headers = array('Content-type' => 'application/json; charset=utf-8');
				$response = Response::make($projectCache->json, 200, $headers, JSON_UNESCAPED_UNICODE);
				return $response;
			}
			else
			{
				$project = Project::with(array('scenes',
												'scenes.startEvents', 'scenes.startEvents.conditions', 'scenes.startEvents.actions', 'scenes.startEvents.actions.conditions',
												'scenes.endEvents', 'scenes.endEvents.conditions', 'scenes.endEvents.actions', 'scenes.endEvents.actions.conditions',
												'scenes.voiceEvents', 'scenes.voiceEvents.conditions', 'scenes.voiceEvents.actions', 'scenes.voiceEvents.actions.conditions',
												'scenes.hotspots', 'scenes.hotspots.conditions', 'scenes.hotspots.actions', 'scenes.hotspots.actions.conditions',
												'scenes.hotspots.labels',
												'globalVariables',
												))
									->where('projects.shortname', $id)
									->first();
			}
		}
		else
		{
			$projectCache = DB::table('projects_cache')
								->where('projects_cache.projectId', $id)
								->first();
			if (isset($projectCache) && $projectCache->useCache == true && $projectCache->json != null)
			{
				// Don't escape the unicode characters, and transmit them at UTF-8.
				$headers = array('Content-type' => 'application/json; charset=utf-8');
				$response = Response::make($projectCache->json, 200, $headers, JSON_UNESCAPED_UNICODE);
				return $response;
			}
			else
			{
				$project = Project::with(array('scenes',
												'scenes.startEvents', 'scenes.startEvents.conditions', 'scenes.startEvents.actions', 'scenes.startEvents.actions.conditions',
												'scenes.endEvents', 'scenes.endEvents.conditions', 'scenes.endEvents.actions', 'scenes.endEvents.actions.conditions',
												'scenes.voiceEvents', 'scenes.voiceEvents.conditions', 'scenes.voiceEvents.actions', 'scenes.voiceEvents.actions.conditions',
												'scenes.hotspots', 'scenes.hotspots.conditions', 'scenes.hotspots.actions', 'scenes.hotspots.actions.conditions',
												'scenes.hotspots.labels',
												'globalVariables',
												))
//									->join('sceneevents_hotspots', 'sceneevents_hotspots.eventId', '=', 'sceneevents.hotspots.id')
									->find($id);
			}
		}

		// Define assets to preload, sorted by scene. We'll want to calculate priorities as well.
		$projectAssets = array();

		if ($project->introMedia)
		{
			$project->introMedia = $this->getMediaObject($project->introMedia);

			// TODO: Add to assets, without a specific scene.
//			$this->addMediaAsset($project->introMedia, $scene->id, 1);
		}
		

		// Rename global variables parameter.
		$project->globalVariables = $project->global_variables;
		unset($project->global_variables);

		// Retrieve background media objects for each scene.
		foreach ($project->scenes as $scene)
		{
			// TODO: Change the object name outputs in the query above.
			$scene->startEvents = $scene->start_events;
			$scene->endEvents = $scene->end_events;
			$scene->voiceEvents = $scene->voice_events;
			unset($scene->start_events);
			unset($scene->end_events);
			unset($scene->voice_events);

			if (isset($scene->backgroundMedia) && $scene->backgroundMedia != null)
			{
				$scene->backgroundMedia = $this->getMediaObject($scene->backgroundMedia);

				// Background images are top priority when preloading.
				$this->addMediaAsset($scene->backgroundMedia, $scene->id, 1);
			}

			// Unserialize parameters and add to the object.
			if ($scene->params)
			{
				try
				{
					$params_obj = unserialize($scene->params);
					$vars = get_object_vars($params_obj);
					foreach ($vars as $var => $value)
						$scene->$var = $value;
				}
				catch (Exception $e)
				{
					// TODO: Error-handle.
				}
				unset($scene->params);
			}

			// Retrieve media objects for the actions of each event.
			foreach ($scene->startEvents as $i => $event)
			{
				foreach ($event->actions as $j => $action)
				{
					$action->conditions = $scene->startEvents[$i]->actions[$j]->conditions;		// TODO: Why is this necessary? It's not showing up in startEvents otherwise. Just use start_events in the viewer.

					// Add media objects.
					if (isset($action->mediaId) && $action->mediaId != null)
					{
						$action->media = $this->getMediaObject($action->mediaId);

						// Start event media is priority 2.
						$this->addMediaAsset($action->media, $scene->id, 2);
					}

					// Unserialize parameters and add to the object.
					if ($action->params)
					{
						try
						{
							$params_obj = unserialize($action->params);
							$vars = get_object_vars($params_obj);
							foreach ($vars as $var => $value)
								$action->$var = $value;
						}
						catch (Exception $e)
						{
							// TODO: Error-handle.
						}
						unset($action->params);
					}
						
					// Add jump targets.
					if ($action->type == 'jump' && isset($action->targetId))
						$this->addJumpTarget($scene->id, $action->targetId);
				}
			}

			foreach ($scene->endEvents as $event)
			{
				foreach ($event->actions as $action)
				{
					$action->conditions = $scene->endEvents[$i]->actions[$j]->conditions;		// TODO: Why is this necessary? It's not showing up in endEvents otherwise. Just use end_events in the viewer.

					// Add media objects.
					if (isset($action->mediaId) && $action->mediaId != null)
					{
						$action->media = $this->getMediaObject($action->mediaId);

						// End event media is last priority.
						$this->addMediaAsset($action->media, $scene->id, 10);
					}


					// Unserialize parameters and add to the object.
					if ($action->params)
					{
						try
						{
							$params_obj = unserialize($action->params);
							$vars = get_object_vars($params_obj);
							foreach ($vars as $var => $value)
								$action->$var = $value;
						}
						catch (Exception $e)
						{
							// TODO: Error-handle.
						}
						unset($action->params);
					}

					// Add jump targets.
					if ($action->type == 'jump' && isset($action->targetId))
						$this->addJumpTarget($scene->id, $action->targetId);
				}
			}

			foreach ($scene->voiceEvents as $event)
			{
				foreach ($event->actions as $action)
				{
					$action->conditions = $scene->voiceEvents[$i]->actions[$j]->conditions;		// TODO: Why is this necessary? It's not showing up in voiceEvents otherwise. Just use voice_events in the viewer.

					// Add media objects.
					if (isset($action->mediaId) && $action->mediaId != null)
					{
						$action->media = $this->getMediaObject($action->mediaId);

						// Voice media is priority 5.
						$this->addMediaAsset($action->media, $scene->id, 5);
					}


					// Unserialize parameters and add to the object.
					if ($action->params)
					{
						try
						{
							$params_obj = unserialize($action->params);
							$vars = get_object_vars($params_obj);
							foreach ($vars as $var => $value)
								$action->$var = $value;
						}
						catch (Exception $e)
						{
							// TODO: Error-handle.
						}
						unset($action->params);
					}

					// Add jump targets.
					if ($action->type == 'jump' && isset($action->targetId))
						$this->addJumpTarget($scene->id, $action->targetId);
				}
			}

			foreach ($scene->hotspots as $event)
			{
				foreach ($event->actions as $action)
				{
					// Add media objects.
					if (isset($action->mediaId) && $action->mediaId != null)
					{
						$action->media = $this->getMediaObject($action->mediaId);

						// Hotspot media is priority 3.
						$this->addMediaAsset($action->media, $scene->id, 3);
					}


					// Unserialize parameters and add to the object.
					if ($action->params)
					{
						try
						{
							$params_obj = unserialize($action->params);
							$vars = get_object_vars($params_obj);
							foreach ($vars as $var => $value)
								$action->$var = $value;
						}
						catch (Exception $e)
						{
							// TODO: Error-handle.
						}
						unset($action->params);
					}

					// Add jump targets.
					if ($action->type == 'jump' && isset($action->targetId))
						$this->addJumpTarget($scene->id, $action->targetId);
				}

				// Add label parameters.
				foreach ($event->labels as $label)
				{
					// Unserialize parameters and add to the object.
					if ($label->params)
					{
						try
						{
							$params_obj = unserialize($label->params);
							$vars = get_object_vars($params_obj);
							foreach ($vars as $var => $value)
								$label->$var = $value;
						}
						catch (Exception $e)
						{
							// TODO: Error-handle.
						}
					}

					// Add media objects.
					if (isset($label->mediaId) && $label->mediaId != null)
					{
						$label->media = $this->getMediaObject($label->mediaId);

						// Voice media is priority 2.
						$this->addMediaAsset($label->media, $scene->id, 2);
					}
				}

				// Add the extra hotspot data.
				$hotspot = DB::table('sceneevents_hotspots')->where('sceneevents_hotspots.eventId', '=', $event->id)->first();
				$event->positionX = $hotspot->positionX;
				$event->positionY = $hotspot->positionY;
				$event->positionZ = $hotspot->positionZ;
				$event->radius = $hotspot->radius;
				$event->duration = $hotspot->duration;
				$event->activateByTrigger = $hotspot->activateByTrigger;
				$event->blockOtherHotspots = $hotspot->blockOtherHotspots;

				// Add countdown media if any exist.
				if (isset($hotspot->countdownMedia))
				{
					$event->countdownSpritemap = new stdClass;
					$event->countdownSpritemap->media = $this->getMediaObject($hotspot->countdownMedia);
					if ($hotspot->countdownMediaParams)
					{
						try
						{
							$params_obj = unserialize($hotspot->countdownMediaParams);
							$vars = get_object_vars($params_obj);
							foreach ($vars as $var => $value)
								$event->countdownSpritemap->$var = $value;
						}
						catch (Exception $e)
						{
							// TODO: Error-handle.
						}
					}
				}
 			}
		}

		// Add jump targets.
		$project->jumpTargets = $this->projectJumpTargets;

		// TODO: All priority 1 and 2 assets should be loaded in ngWebgl.js before a scene starts.
		// Others can be loaded while the scene is being used.
		if (isset($this->assets))
			$project->assets = $this->assets;

		// Add the media collection to the project scope.
		if (isset($this->media))
			$project->media = $this->media;

		// If we're supposed to use the cache, but there's no JSON saved yet, save it now.
		if (isset($projectCache) && $projectCache->useCache == true && ($projectCache->json == null || $projectCache->json == ''))
		{
			$projectCache->json = json_encode($project);
//			$projectCache->save();
			DB::table('projects_cache')
					->where('projectId', $projectCache->projectId)
					->update(array('json' => $projectCache->json));
		}

		// Don't escape the unicode characters, and transmit them at UTF-8.
		$headers = array('Content-type' => 'application/json; charset=utf-8');
		$response = Response::json($project, 200, $headers, JSON_UNESCAPED_UNICODE);
		return $response;
	}

	// TODO: This is redundantly used in other controllers. Fix this so it inherits properly or uses a model.
	protected function getMediaObject($mediaId)
	{
		$media_files = DB::table('media')->where('media.id', '=', $mediaId)
										->join('media_files', 'media_files.mediaId', '=', 'media.id')
										->get();
		$mediaObj = Media::find($mediaId);
		$files = array();
		foreach ($media_files as $mf)
		{
			if ($mf->relativeSize == 'all')
			{
				$mediaObj->fileIdLow = $mf->fileId;
				$mediaObj->fileIdMedium = $mf->fileId;
				$mediaObj->fileIdHigh = $mf->fileId;
				$mediaObj->fileIdUltra = $mf->fileId;
			}
			else if ($mf->relativeSize == 'low')
				$mediaObj->fileIdLow = $mf->fileId;
			else if ($mf->relativeSize == 'medium')
				$mediaObj->fileIdMedium = $mf->fileId;
			else if ($mf->relativeSize == 'high')
				$mediaObj->fileIdHigh = $mf->fileId;
			else if ($mf->relativeSize == 'ultra')
				$mediaObj->fileIdUltra = $mf->fileId;

			if (!isset($files[$mf->fileId]))
				$files[$mf->fileId] = DB::table('files')->find($mf->fileId);

			// TODO: We may not want to construct these manually, and instead use the filepath field
			// in the files table to contain the full filepath.
			foreach ($files as $file)
			{
				$file->filepath = '/api/download/' . $file->contenthash . '/' . $file->filesize;
			}
		}
		$mediaObj->files = array_values($files);
		return $mediaObj;
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function edit($id)
	{
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		Project::destroy($id);
		return Response::json(array('success' => true));
	}
/*
	public function show(Project $project)
	{
		$scenes = $project->load('scenes');
		return $project;
	}

	public function getProject($id)
	{
		if (is_numeric($id))
			$results = DB::table('projects')->where('id', $id)->get();
		else if (is_string($id))
			$results = DB::table('projects')->where('shortname', $id)->get();
		return Response::json($project);
 	}

	public function getProject($id)
	{
		$project = Project::find($id);
//		$project->scenes = Project::find($id)->scenes;
		$project->scenes;					// Is this an acceptable way to add the $scenes array/relation to $project?
		foreach ($project->scenes as $scene)
		{
//			echo '<pre>'; print_r($scene); echo '</pre>';
		}

		$project = Project::with('scenes.hotspots')->get();

		return "{
			id: 1,
			shortname: 'ocean',
			title: 'Ocean Exploration',
			subtitle: '',
			description: 'Join these scuba divers and explore the magnificent wonders of the ocean.',
			createdby: 1,
			timecreated: 0,
			startScene: 1,
		
			globalVariables: [
			{
				name: 'playedEnteringTheDepths',
				defaultValue: '0',
			},
			{
				name: 'playedEnteringTheDepths',
				defaultValue: '0',
			},
			{
				name: 'playedSubtationCuracao',
				defaultValue: '0',
			}],
		
			prefetchAssets: [
			{
				images: [
				{
					id: 1,
					sceneid: 1,
					path: '/files/49/494679425abebbdeb7457822e9dcff2d4273de0b',
					priority: 1,										// Background media should take priority.
					type: 'image',
				},
				{
					id: 2,
					sceneid: 3,
					path: '/files/51/51d31bdbafb7223728549a270e46ef0f415abdc4',
					priority: 1,										// Background media should take priority.
					type: 'image',
				}],
		
				audio: [
				{
					id: 6,
					sceneid: 1,
					path: '/files/f5/f51cd3358db6e6f531d0e50dd86678147b19f6cc',
					priority: 1,
					type: 'audio',
				},
				{
					id: 10,
					sceneid: 2,
					path: '/files/23/2349bbe135b6e1a2790ccfe691eab0589094cdc6',
					priority: 1,
					type: 'audio',
				},
				{
					id: 9,
					sceneid: 1,
					path: '/files/89/8927300a265eb3f6a3a7f5806b37d70206b08bc0',
					priority: 2,
					type: 'audio',
				}],
		
				video: [
				],
			}],
		
			scenes: [
			{
				id: 1,
				shortname: 'bnzswbsmnswrinsj',
				projectid: '1',
				sequence: '1',
				parent: '0',
				title: 'Entering the Depths',
				subtitle: '',
				description: 'Scuba diving is a sport that is practiced recreationally all around the world, and for some, is even a profession. Once underwater, divers meet a dazzling array of fish, marine mammals, and aquatic plant life. The undersea world is vast with beauty. A trained scuba diver can swim to depths over 100 feet, and explore this amazing world that few people get to see.',
				viewType: 'spherecast',
				backgroundFile:
				{
					fileId: '1',
					filePath: '/files/49/494679425abebbdeb7457822e9dcff2d4273de0b',
					fileType: 'image',
				},
				embedid: '0',
				createdby: '1',
				lockedby: '0',
				lockedtime: '0',
				timecreated: '0',
				timemodified: '0',
		
				startEvents: [
				{
					id: 4,
					visible: '0',
					overridable: '1',							// The text and the audio can be interrupted by hotspots events.
					conditions: [
					{
						conditionType: 'variable',
						conditionVariableName: 'playedEnteringTheDepths',
						conditionVariableOperator: '!=',
						conditionVariableOperand: '0',
					}],
		
					actions: [
					{
						actionType: 'text',						// Fade in the text and fade out after 3 seconds.
						actionText: 'Welcome to the Spherecast',
						actionDuration: 1500,					// Milliseconds
						actionSequence: 1,
						actionContinue: true,
					},
					{
						actionType: 'audio',					// Play an audio file.
						actionItemId: 9,
						actionFile:
						{
							fileId: 10,
		//					filePath: '/files/f5/f51cd3358db6e6f531d0e50dd86678147b19f6cc',
							fileType: 'audio',
						},
						actionDuration: 0,						// TODO: Number of milliseconds before stopping audio file.
						actionRepeat: false,
						actionSequence: 2,
						actionContinue: false,					// Don't continue until the audio file is finished playing, or another audio action has interrupted it.
					},
					{
						actionType: 'variable',
						actionVariableName: 'playedEnteringTheDepths',
						actionVariableOperator: 'set',
						actionVariableOperand: '1',
						actionSequence: 3,
						actionContinue: false,
					}],
				}],
		
				// End events only occur immediately before a scene jump. The jump takes place when they have all finished.
				endEvents: [
				{
				}],
		
				hotspots: [
				{
					id: 3,
					positionX: '0',
					positionY: '1',
					positionZ: '0',
					radius: '0.2',
					duration: '1500',							// Milliseconds
					visible: '1',
					overridable: '0',
					toggleOtherHotspots: true,
					conditions: [
					{
						conditionType: 'variable',
						conditionVariableName: 'playedEnteringTheDepths',
						conditionVariableOperator: '!=',
						conditionVariableOperand: '0',
					}],
					actions: [
					{
						actionType: 'jump',
						actionItemId: 2,
						actionSequence: 1
					}],
				},
				{
					id: 2,
					positionX: '0',
					positionY: '-1',
					positionZ: '0',
					radius: '0.2',
					duration: '1000',							// Milliseconds
					visible: '1',
					overridable: '0',
					actions: [
					{
						actionType: 'jump',
						actionItemId: 2,
						actionSequence: 2,
						audioContinue: false,
					}],
				},
				{
					id: 1,
					positionX: '-0.067',
					positionY: '-0.209',
					positionZ: '0.976',
					radius: '0.2',
					label: 'Diver',
					duration: '1000',							// Milliseconds
					visible: '1',
					overridable: '0',
					conditions: [
					{
						conditionType: 'variable',
						conditionVariableName: 'playedEnteringTheDepths',
						conditionVariableOperator: '!=',
						conditionVariableOperand: '0',
					}],
		
					actions: [
					{
						actionType: 'text',						// Fade in the text and fade out after 3 seconds.
						actionText: 'This is a diver',
						actionDuration: 1000,					// Milliseconds
						actionSequence: 1,
						actionContinue: false,
					},
					{
						actionType: 'image',					// Display an image.
						actionPlacement: 'fixed',				// This places an image directly at the specified coordinates (which are directly over the hotspot).
						actionDuration: 1000,
						actionPositionX: '-0.067',
						actionPositionY: '-0.209',
						actionPositionZ: '-0.976',
						actionItemId: 3,
						actionFile:
						{
							fileId: 3,
							filePath: '/files/78/78fe5b67a58b211159883d6cd28dd81814b8d685',
							fileType: 'image',
						},
						actionSequence: 4,
						actionContinue: true,
					},
					{
						actionType: 'variable',					// Set a global variable.
						actionVariableName: 'playedEnteringTheDepths',
						actionVariableOperator: 'set',
						actionVariableOperand: '1',
						actionSequence: 5,
						actionContinue: false,
					},
					{
						actionType: 'audio',					// Play an audio file.
						actionItemId: 9,
						actionFile:
						{
							fileId: 9,
							filePath: '/files/89/8927300a265eb3f6a3a7f5806b37d70206b08bc0',
							fileType: 'audio',
						},
						actionDuration: 0,						// TODO: Number of milliseconds before stopping audio file.
						actionRepeat: false,
						actionSequence: 6,
						actionContinue: true,					// Don't continue until the audio file is finished playing, or another audio action has interrupted it.
					},
					{
						actionType: 'audio',					// Play an audio file.
						actionItemId: 10,
						actionFile:
						{
							fileId: 10,
							filePath: '/files/f5/f51cd3358db6e6f531d0e50dd86678147b19f6cc',
							fileType: 'audio',
						},
						actionDuration: 0,						// TODO: Number of milliseconds before stopping audio file.
						actionRepeat: true,
						actionSequence: 7,
						actionContinue: true,					// Don't continue until the audio file is finished playing, or another audio action has interrupted it.
					},
					{
						actionType: 'text',						// Fade in the text and fade out after 3 seconds.
						actionText: 'Feel free to browse around the scene',
						actionDuration: 5000,					// Milliseconds
						actionSequence: 8,
						actionContinue: true,
					}],
				}],
			},
			{
				id: 2,
				shortname: 'zpqgbfamcfjbllsv',
				projectid: '1',
				sequence: '2',
				parent: '0',
				title: 'Tubbataha Reefs',
				subtitle: '',
				description: 'The Tubbataha Reefs, located in a protected area of the Philippines, in the middle of the Sulu Sea, has one of the highest density rates of marine life anywhere in the known ocean. There are no less than 600 fish species, 360 coral species, 11 shark species, and 13 dolphin and whale species that live in the reefs. It is made up of two coral atolls, which form a spectacular 330-foot perpendicular coral wall, and was formed thousands of years ago by underwater volcanoes which are now inactive.',
				viewType: 'spherecast',
				backgroundFile:
				{
					fileId: '4',
					filePath: '/files/49/490f3190819d9ecc83180b161e83f5776b6ca9bf',
					fileType: 'image',
				},
				embedid: '0',
				createdby: '1',
				lockedby: '0',
				lockedtime: '0',
				timecreated: '0',
				timemodified: '0',
		
				// As start events are running, hotspots are disabled until they have all completed.
				// Conditions can be placed on either entire events or single actions. In this case, we don't play
				// the event if the global variable playedEnteringTheDepths flag has been set.
				startEvents: [
				{
					id: 1,
					visible: '0',
					overridable: '1',							// The text and the audio can be interrupted by hotspots events.
					actions: [
					{
						actionType: 'text',						// Fade in the text and fade out after 3 seconds.
						actionText: 'Tubbataha Reefs',
						actionDuration: 1500,					// Milliseconds
						actionSequence: 1,
						actionContinue: false,
					},
					{
						actionType: 'audio',					// Play an audio file.
						actionItemId: 10,
						actionFile:
						{
							fileId: 10,
							filePath: '/files/23/2349bbe135b6e1a2790ccfe691eab0589094cdc6',
							fileType: 'audio',
						},
						actionDuration: 0,						// TODO: Number of milliseconds before stopping audio file.
						actionRepeat: false,
						actionSequence: 1,
						actionContinue: false,					// Don't continue until the audio file is finished playing, or another audio action has interrupted it.
					}],
				}],
		
				// End events only occur immediately before a scene jump. The jump takes place when they have all finished.
				endEvents: [
				{
				}],

				hotspots: [
				{
					id: 5,
					positionX: '0',
					positionY: '-1',
					positionZ: '0',
					radius: '0.2',
					duration: '2000',				// Milliseconds
					visible: '1',
					overridable: '0',
					actions: [
					{
						actionType: 'jump',
						actionItemId: 3,
						actionSequence: 1,
						actionContinue: false,
					}],
				}],
			},
			{
				id: 3,
				shortname: 'oczvjggkxphdeopy',
				projectid: '1',
				sequence: '3',
				parent: '0',
				title: 'Substation Curacao',
				subtitle: '',
				description: 'The divers pass by the Substation Curacao. This is a mini-submarine for tourists, capable of carrying two passengers at a time. Unlike scuba diving, submarine diving has no effects of pressure change on the body. As a result, it is able to safely travel to depths of 1000 feet, deeper than the divers can reach. The bottom of the ocean is a vast and greatly unknown frontier, and these submarines can be used for scientific research as well. Studying the ocean floor can help contribute to the understanding of bio-chemical structures and substances found within marine biology.',
				viewType: 'spherecast',
				backgroundFile:
				{
					fileId: '1',
					filePath: '/files/51/51d31bdbafb7223728549a270e46ef0f415abdc4',
					fileType: 'image',
				},
				embedid: '0',
				createdby: '1',
				lockedby: '0',
				lockedtime: '0',
				timecreated: '0',
				timemodified: '0',
		
				// As start events are running, hotspots are disabled until they have all completed.
				// Conditions can be placed on either entire events or single actions. In this case, we don't play
				// the event if the global variable playedEnteringTheDepths flag has been set.
				startEvents: [
				{
					id: 10,
					visible: '0',
					overridable: '1',							// The text and the audio can be interrupted by hotspots events.
					actions: [
					{
						actionType: 'audio',					// Play an audio file.
						actionItemId: 11,
						actionFile:
						{
							fileId: 11,
							filePath: '/files/62/62d088ce5b50f00fd905e052a1287e12aa2d3d47',
							fileType: 'audio',
						},
						actionDuration: 0,						// TODO: Number of milliseconds before stopping audio file.
						actionRepeat: false,
						actionSequence: 1,
						actionContinue: false,					// Don't continue until the audio file is finished playing, or another audio action has interrupted it.
					}],
				}],
		
				// End events only occur immediately before a scene jump. The jump takes place when they have all finished.
				endEvents: [
				{
				}],
		
				hotspots: [
				{
					id: 4,
					positionX: '-0.512',
					positionY: '-0.110',
					positionZ: '-0.86',
					radius: '0.5',
					duration: '1',					// Milliseconds
					visible: '1',
					overridable: '0',
					actions: [
					{
						actionType: 'text',
						actionText: 'Substation Curacao',
						actionDuration: 1000,
						actionSequence: 1,
						actionContinue: true,
					}],
				},
				{
					id: 5,
					positionX: '0.870',
					positionY: '0.426',
					positionZ: '0.251',
					radius: '0.2',
					duration: '1',					// Milliseconds
					visible: '1',
					overridable: '0',
					actions: [
					{
						actionType: 'text',
						actionText: 'Diver',
						actionDuration: 1000,
						actionSequence: 1,
						actionContinue: true,
					}],
				},
				{
					id: 6,
					positionX: '0',
					positionY: '-1',
					positionZ: '0',
					radius: '0.2',
					duration: '2000',					// Milliseconds
					visible: '1',
					overridable: '0',
					actions: [
					{
						actionType: 'jump',
						actionItemId: 4,
						actionSequence: 1,
						actionContinue: false,
					}],
				}],
			},
			{
				id: 4,
				shortname: 'bctnxcawpgbegbuz',
				projectid: '1',
				sequence: '4',
				parent: '0',
				title: 'Bait Ball',
				subtitle: '',
				description: 'Fish swimming alone are more likely to be eaten by a predator than large groups. So these salema fish are congregating in what’s known as a “bait ball.” This is where the school of fish swarm in a tightly-packed spherical formation. It’s a defensive mechanism smaller fish use when they’re threatened by predators.',
				viewType: 'spherecast',
				backgroundFile:
				{
					fileId: '20',
					filePath: '/files/a8/a86d88e1aa2a1705fdec1de5c19087b7316b3160',
					fileType: 'image',
				},
				embedid: '0',
				createdby: '1',
				lockedby: '0',
				lockedtime: '0',
				timecreated: '0',
				timemodified: '0',
		
				// As start events are running, hotspots are disabled until they have all completed.
				// Conditions can be placed on either entire events or single actions. In this case, we don't play
				// the event if the global variable playedEnteringTheDepths flag has been set.
				startEvents: [
				{
					id: 11,
					visible: '0',
					overridable: '1',							// The text and the audio can be interrupted by hotspots events.
					actions: [
					{
						actionType: 'audio',					// Play an audio file.
						actionItemId: 12,
						actionFile:
						{
							fileId: 12,
		//					filePath: '/files/58/581afbaa612b0ed936449290cb345b02c31cc282',
							fileType: 'audio',
						},
						actionDuration: 0,						// TODO: Number of milliseconds before stopping audio file.
						actionRepeat: false,
						actionSequence: 1,
						actionContinue: false,
					}],
				}],
		
				// End events only occur immediately before a scene jump. The jump takes place when they have all finished.
				endEvents: [
				{
				}],
		
				hotspots: [
				{
					id: 6,
					positionX: '0',
					positionY: '-1',
					positionZ: '0',
					radius: '0.2',
					duration: '2000',
					visible: '1',
					overridable: '0',
					actions: [
					{
						actionType: 'jump',
						actionItemId: 10,
						actionSequence: 1,
						actionContinue: false,
					}],
				}],
			},
		
			{
				id: 10,
				shortname: 'bctnxcawpgbegbuz',
				projectid: '1',
				sequence: '4',
				parent: '0',
				title: 'Scuba Diving',
				subtitle: '',
				description: 'Experience a video of scuba diving.',
				viewType: 'spherecast',
				backgroundFile:
				{
					fileId: '17',
					filePath: '/files/86/86e595f59e6904cd3ae1b39b6ef1ab8f07e0fd49',
					fileType: 'video',
					fileMimeType: 'video/webm',
				},
				embedid: '0',
				createdby: '1',
				lockedby: '0',
				lockedtime: '0',
				timecreated: '0',
				timemodified: '0',
		
				// Background events are specific to events occuring with the background media (currently, just videos).
				backgroundEvents: [
				{
					id: 30,
					visible: 0,
					overridable: 0,
					actions: [
					{
						actionType: 'stop',
					}],
				}],
		
				// As start events are running, hotspots are disabled until they have all completed.
				// Conditions can be placed on either entire events or single actions. In this case, we don't play
				// the event if the global variable playedEnteringTheDepths flag has been set.
				startEvents: [
				{
					id: 20,
					visible: 0,
					overridable: 0,								// The event cannot be interrupted by hotspots events.
					actions: [
					{
						actionType: 'audio',					// Play an audio file.
						actionItemId: 9,
						actionFile:
						{
							fileId: 10,
							filePath: '/files/f5/f51cd3358db6e6f531d0e50dd86678147b19f6cc',
							fileType: 'audio',
						},
						actionDuration: 0,						// TODO: Number of milliseconds before stopping audio file.
						actionRepeat: false,
						actionSequence: 1,
						actionContinue: false,					// Don't continue until the audio file is finished playing, or another audio action has interrupted it.
					}],
				}],
		
				speechEvents: [
				{
					id: 100,
					words: ['stop', 'pause'],
					language: 'en',
					minConfidence: 0.75,
					overridable: 0,
					actions: [
					{
						actionType: 'mediaStop',
						actionItemId: 10,						// The file id of the audio or video to stop playing (the audio or video player should contain this in its id).
						actionSequence: 1,
						actionContinue: false,
					}],
				},
				{
					id: 101,
					words: ['play'],
					language: 'en',
					minConfidence: 0.75,
					overridable: 0,
					actions: [
					{
						actionType: 'mediaPlay',
						actionItemId: 10,						// The file id of the audio or video to start playing (the audio or video player should contain this in its id).
						actionSequence: 1,
						actionContinue: false,
					}],
				},
				{
					id: 102,
					words: ['back'],
					language: 'en',
					minConfidence: 0.75,
					overridable: 0,
					actions: [
					{
						actionType: 'mediaBack',
						actionItemId: 10,						// The file id of the audio or video to skip back.
						actionSkipDuration: '10%',				// This could be a number of seconds, or a percentage of the entire media.
						actionSequence: 1,
						actionContinue: false,
					}],
				},
				{
					id: 103,
					words: ['forward'],
					language: 'en',
					minConfidence: 0.75,
					overridable: 0,
					actions: [
					{
						actionType: 'mediaPlay',
						actionItemId: 10,						// The file id of the audio or video to skip forward.
						actionSkipDuration: '10%',				// This could be a number of seconds, or a percentage of the entire media.
						actionSequence: 1,
						actionContinue: false,
					}],
				}],
		
				// End events only occur immediately before a scene jump. The jump takes place when they have all finished.
				endEvents: [
				{
				}],

				hotspots: [
				{
				}],
			},
			{
				id: 20,
				shortname: 'abcdefghijklmop',
				projectid: '1',
				sequence: '5',
				parent: '0',
				title: 'Google Street View',
				subtitle: '',
				description: '',
				viewType: 'googlestreetview',
				backgroundUrl: '',
				embedid: '0',									// Create a separate table and link to it with the embedid?
				createdby: '1',
				lockedby: '0',
				lockedtime: '0',
				timecreated: '0',
				timemodified: '0',
		
				startEvents: [
				{
				}],
		
				endEvents: [
				{
				}],
		
				hotspots: [
				{
				}],
			},
			{
				id: 6,
				shortname: 'abcdefghijklmop',
				projectid: '1',
				sequence: '6',
				parent: '0',
				title: 'Sketchfab',
				subtitle: '',
				description: '',
				viewType: 'sketchfab',
				backgroundUrl: '',
				embedid: '0',									// Create a separate table and link to it with the embedid?
				createdby: '1',
				lockedby: '0',
				lockedtime: '0',
				timecreated: '0',
				timemodified: '0',
		
				startEvents: [
				{
				}],
		
				endEvents: [
				{
				}],
		
				hotspots: [
				{
				}],
			}],
		}";
	}

	public function getAllProjects()
	{
		// TODO: Restrict to only logged-in user's projects.
		$results = DB::table('projects')->get();
		return Response::json($results);
	}
*/
}
?>