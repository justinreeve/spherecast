<?php
class ActionParamsImage extends Eloquent
{
	protected $type;					// This can be "overlay" or "position."
	protected $positionX;				// Only used when $type is "position."
	protected $positionY;				// Only used when $type is "position."
	protected $positionZ;				// Only used when $type is "position."
	protected $duration;				// If "0" then stays up until next scene load.
	protected $width = '';				// Resized width of image; default is 75% of screen for overlay.
	protected $height = '';				// Resized height of image; default preserves aspect ratio of width.
	protected $animationStatus;			// "On" or "Off" to indicate whether the animation should be running or just holding still at the first image; this is a setting that should toggleable by an action.
	protected $file;					// File object.

	public function file()
	{
		return $this->hasOne('file', 'fileid');
	}
}
?>