<?php
class Condition extends Eloquent
{
	protected $id;
	protected $eventId;
	protected $type;
	protected $variableName;
	protected $variableOperator;			// Options: =, !=, >, <, contains.
	protected $variableOperand;

	protected $guarded = array('id');
	protected $table = 'conditions';

	public function event()
	{
		return $this->belongsTo('SceneEvent', 'eventId', 'id');
	}
}
?>