<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('projectId')->unsigned()->nullable();
			$table->string('type');								// Can be backgroundimage, backgroundvideo, image, audio, video, or spritemap.
			$table->string('filename');							// The base filename that the user will see in the file browser.
			$table->string('filepath')->default('/');			// The directory path to the file that the user will see in the file browser.
			$table->decimal('scaleX', 3, 2)->default(1.0);
			$table->decimal('scaleY', 3, 2)->default(1.0);
			$table->integer('createdBy')->default(0);
			$table->text('params')->nullable();					// Serialized parameters.

			// Indexes.
			$table->index('type');

			// Foreign keys.
			$table->foreign('projectId')
				->references('id')->on('projects')
				->onDelete('set null')
				->onUpdate('cascade');

			// created_at, updated_at DATETIME
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('media', function(Blueprint $table)
		{
		});
	}
}