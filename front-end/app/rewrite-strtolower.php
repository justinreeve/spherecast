<?php
if(isset($_GET['url'])) {
    $url = $_GET['url'];
    unset($_GET['url']);
    $params = http_build_query($_GET);
    if(strlen($params)) {
        $params = '?' . $params;
    }
    header('Location: http://' . $_SERVER['HTTP_HOST'] . '/' . strtolower($url) . $params, true, 301);
    exit;
}
header("HTTP/1.0 404 Not Found");
die('Unable to locate the Spherecast asset.');
?>