<?php
// See http://culttt.com/2013/05/13/setting-up-your-first-laravel-4-model/
// We need to eagerly collect nested relations: Project -> Scene -> Event -> Action.
// See http://stackoverflow.com/questions/22701455/laravel-collection-eager-loading-based-on-nested-relation-field-value
// See http://stackoverflow.com/questions/22868362/laravel-4-1-eager-loading-nested-relationships-with-constraints
class ProjectCache extends Eloquent
{
	protected $projectId;
	protected $useCache = false;
	protected $json;

	protected $fillable = array('projectId', 'useCache', 'json');
	protected $table = 'projects_cache';

	public function project()
	{
		return $this->belongsTo('Project', 'projectId', 'id');
	}
}
?>