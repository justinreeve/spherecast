<?php
class Label extends Eloquent
{
	protected $id;
	protected $eventId;
	protected $type;					// Options: text, image.
	protected $positionX;
	protected $positionY;
	protected $positionZ;
	protected $scaleX;
	protected $scaleY;
	protected $mediaId;
	protected $params;

	protected $guarded = array('id');
	protected $table = 'labels';

	public function hotspot()
	{
		return $this->belongsTo('Hotspot', 'eventId', 'id');
	}
}
?>