<?php
/**
 * Created with php artisan controller:make ProjectController.
 * 
 * This controller is used to read and write scene-specific data for use with the editor.
 * Currently, we just retrieve the scene and the startEvent associated with it.
 * 
 */
class SceneController extends \BaseController
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$random = "";
		srand((double) microtime() * 1000000);
		$data = "0123456789abcdefghijklmnopqrstuvwxyz";

		// Create a shortname, and ensure it has alphabetic characters and isn't already being used.
		do
		{
			for ($i = 0; $i < 6; $i++)
			{
				$random .= substr($data, (rand() % (strlen($data))), 1);
			}
		}
		while (is_numeric($random) || Scene::where('shortname', $random)->first());

		Scene::create(array(
			'shortname' => $random,
			'projectId' => Input::get('projectId'),
			'title' => Input::get('title'),
			'subtitle' => '',
			'description' => Input::get('description'),
			'createdBy' => 0,
		));

		return Response::json(array('success' => true));
	}


	/**
	 * Add an item to the project assets.
	 */
	protected function addMediaAsset($media, $sceneId, $priority)
	{
		// Create media array if it doesn't exist.
		if (!isset($this->media))
			$this->media = array();

		// Add media if it's not already in the array.
		$add = true;
		foreach ($this->media as $item)
		{
			if ($item->id == $media->id)
			{
				$add = false;
				break;
			}
		}
		if ($add == true)
			$this->media[] = $media;

		// Create assets object if it doesn't exist.
		if (!isset($this->assets))
			$this->assets = new stdClass;

		// Create the scenes object if it doesn't exist.
		if (!isset($this->assets->scenes))
			$this->assets->scenes = new stdClass;

		// Create the scene array if it doesn't exist.
		if (!isset($this->assets->scenes->$sceneId))
			$this->assets->scenes->$sceneId = array();

		// Create the priority levels for the scene assets.
		if (!isset($this->assets->priorities))
		{
			$this->assets->priorities = new stdClass;
			for ($p = 1; $p <= 5; $p++)
				$this->assets->priorities->$p = array();
		}

		// Create the media object if it doesn't exist.
		$mediaId = $media->id;
		if (!isset($this->assets->media))
			$this->assets->media = new stdClass;

		// Create asset object.
		$this->assets->media->$mediaId = $media;
		if (!in_array($media->id, $this->assets->scenes->$sceneId))
			array_push($this->assets->scenes->$sceneId, $media->id);
		if (!in_array($media->id, $this->assets->priorities->$priority))
			array_push($this->assets->priorities->$priority, $media->id);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// If the shortname was passed, use that instead of the id.
		if (!is_numeric($id))
		{
			// Retrieve the scene by its shortname.
			$scene = Scene::with('project',
									'startEvents', 'startEvents.actions',
									'startEvents', 'startEvents.conditions', 'startEvents.actions',
									'endEvents', 'endEvents.conditions', 'endEvents.actions',
									'voiceEvents', 'voiceEvents.conditions', 'voiceEvents.actions',
									'hotspots', 'hotspots.conditions', 'hotspots.actions',
									'hotspots.labels')
							->where('scenes.shortname', $id)
							->first();

			// Retrieve this scene and all the other scenes so we can refer to them in the project.
			$scene->scenes = Scene::where('projectId', $scene->projectId)
									->orderBy('sequence')
									->get();
		}
		else
		{
			// Retrieve the scene by its id.
			$scene = Scene::with('startEvents', 'startEvents.actions')
							->find($id);

			// Retrieve this scene and all the other scenes so we can refer to them in the project.
			$scene->scenes = Scene::where('projectId', $scene->projectId)
									->orderBy('sequence')
									->get();
		}

		$scene->assets = new stdClass;

		if (isset($scene->backgroundMedia) && $scene->backgroundMedia != null)
		{
//			$scene->backgroundMedia = $this->getMediaObject($scene->backgroundMedia);
			$media = $this->getMediaObject($scene->backgroundMedia);
			$this->addMediaAsset($media, $scene->id, 1);		// Background media are priority 2.
		}

		// Retrieve startEvent action media objects for the scene.
		// TODO: Figure out how to do this in the query above.
		$scene->startEvents = $scene->start_events;
		foreach ($scene->startEvents as $event)
		{
			foreach ($event->actions as $action)
			{
				// Add media objects.
				if (isset($action->mediaId) && $action->mediaId != null)
				{
					$media = $this->getMediaObject($action->mediaId);
					$this->addMediaAsset($media, $scene->id, 2);		// Start events are priority 2.
				}

				// Unserialize parameters and add to the object.
				if ($action->params)
				{
					try
					{
						$params_obj = unserialize($action->params);
						$vars = get_object_vars($params_obj);
						foreach ($vars as $var => $value)
							$action->$var = $value;
					}
					catch (Exception $e)
					{
						// TODO: Error-handle.
					}
					unset($action->params);
				}
			}
		}

		// Retrieve endEvent action media objects for the scene.
		foreach ($scene->endEvents as $event)
		{
			foreach ($event->actions as $action)
			{
				// Add media objects.
				if (isset($action->mediaId) && $action->mediaId != null)
				{
					$media = $this->getMediaObject($action->mediaId);
					$this->addMediaAsset($media, $scene->id, 2);		// Start events are priority 2.
				}

				// Unserialize parameters and add to the object.
				if ($action->params)
				{
					try
					{
						$params_obj = unserialize($action->params);
						$vars = get_object_vars($params_obj);
						foreach ($vars as $var => $value)
							$action->$var = $value;
					}
					catch (Exception $e)
					{
						// TODO: Error-handle.
					}
					unset($action->params);
				}
			}
		}

		// Retrieve hotspot action media objects for the scene.
		foreach ($scene->hotspots as $event)
		{
			foreach ($event->actions as $action)
			{
				// Add media objects.
				if (isset($action->mediaId) && $action->mediaId != null)
				{
					$media = $this->getMediaObject($action->mediaId);
					$this->addMediaAsset($media, $scene->id, 2);		// Start events are priority 2.
				}

				// Unserialize parameters and add to the object.
				if ($action->params)
				{
					try
					{
						$params_obj = unserialize($action->params);
						$vars = get_object_vars($params_obj);
						foreach ($vars as $var => $value)
							$action->$var = $value;
					}
					catch (Exception $e)
					{
						// TODO: Error-handle.
					}
					unset($action->params);
				}
			}
		}
		// Add scene assets.
		$scene->assets = $this->assets;

		return Response::json($scene);
	}

	// TODO: This is redundantly used in other controllers. Fix this so it inherits properly or uses a model.
	protected function getMediaObject($mediaId)
	{
		$media_files = DB::table('media')->where('media.id', '=', $mediaId)
										->join('media_files', 'media_files.mediaId', '=', 'media.id')
										->get();
		$mediaObj = Media::find($mediaId);
		$files = array();
		foreach ($media_files as $mf)
		{
			if ($mf->relativeSize == 'all')
			{
				$mediaObj->fileIdLow = $mf->fileId;
				$mediaObj->fileIdMedium = $mf->fileId;
				$mediaObj->fileIdHigh = $mf->fileId;
				$mediaObj->fileIdUltra = $mf->fileId;
			}
			else if ($mf->relativeSize == 'low')
				$mediaObj->fileIdLow = $mf->fileId;
			else if ($mf->relativeSize == 'medium')
				$mediaObj->fileIdMedium = $mf->fileId;
			else if ($mf->relativeSize == 'high')
				$mediaObj->fileIdHigh = $mf->fileId;
			else if ($mf->relativeSize == 'ultra')
				$mediaObj->fileIdUltra = $mf->fileId;

			if (!isset($files[$mf->fileId]))
				$files[$mf->fileId] = DB::table('files')->find($mf->fileId);
		}
		$mediaObj->files = array_values($files);
		return $mediaObj;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
	 * Get basic metadata for the scene.
	 */
	public function getMetaData($id)
	{
		if (!is_numeric($id))
			$scene = Scene::where('scenes.shortname', $id)->first();
		else
			$scene = Scene::where('scenes.id', $id)->first();

		return $scene;
	}
}