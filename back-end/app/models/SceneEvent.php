<?php
class SceneEvent extends Eloquent
{
	protected $id;
	protected $shortname;
	protected $sceneId;
	protected $type;
	protected $overridable;

//	protected $imageFileId;				// A file object that's used instead of the standard hotspot indicator. This is particularly used each time a scene has a "bottom nav" hotspot.
//	protected $allowMouseClick;
//	protected $allowMouseDoubleClick;
//	protected $allowLook;				// Allow triggering actions by looking at them.
//	protected $lookDuration;
//	protected $lookVisible;				// Only used when $type is "hotspot," and if false, then it's an invisible hotspot (no indication it's being viewed)
//	protected $allowMagnetPull;			// Device app only; won't work on the web.
//	protected $allowBluetoothAdvance;	// Device app only; won't work on the web.

	protected $guarded = array('id');
	protected $table = 'sceneevents';

	public function scene()
	{
		return $this->belongsTo('Scene', 'sceneId', 'id');
	}

	public function hotspot()
	{
		return $this->hasOne('hotspot');
	}

	public function scopeStartEvents($query)
	{
		return $query->where('type', 'start');
	}

	public function scopeEndEvents($query)
	{
		return $query->where('type', 'end');
	}

	public function scopeVoiceEvents($query)
	{
		return $query->where('type', 'voice');
	}

	public function scopeHotspots($query)
	{
		return $query->where('type', 'hotspot');
	}

	public function actions()
	{
		return $this->hasMany('Action', 'eventId', 'id')->orderBy('sequence');
	}

	public function labels()
	{
		return $this->hasMany('Label', 'eventId', 'id');
	}

	public function conditions()
	{
		return $this->hasMany('Condition', 'eventId', 'id');
	}
}
?>