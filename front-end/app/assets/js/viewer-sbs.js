'use strict';

angular.module('spherecastApp')
  .directive('sbsViewer', function () {
    return {
      restrict: 'A',
      scope: {
        'width': '=',
        'height': '=',
        'fillcontainer': '=',
        'scale': '=',
        'materialType': '=',
        'activeHotspot': '=',
        'activeEvent': '=',
		'mode': '=',
		'activeProject': '=',
		'activeScene': '=',
      },
      link: function postLink(scope, element, attrs) {

        var camera, scene, renderer,
          shadowMesh, icosahedron, light,
          mouseX = 0, mouseY = 0,
          contW = (scope.fillcontainer) ? 
            element[0].clientWidth : scope.width,
          contH = scope.height, 
          windowHalfX = contW / 2,
          windowHalfY = contH / 2,
          materials = {};
		var controls;
		var stereoEffect;
		var sphereIndex = -1;
		var currentSphere = null;
		var domElement;
		var sceneViewerCanvas;
		var cameraOrientationCanvas;
		var activeHotspot = null;			// Only one hotspot can be active (i.e. in the process of being triggered) at a time.
		var lastHotspot = null;
		var activeSceneAssets = {};			// Anything that has been loaded on to the scene via actions.
		var activeActionSequences = [];

		// TODO: Use a JSON of all hotspots retrieved from the database.
		var hotspots;
		var hotspotCountdowns = [];
		var clock = new THREE.Clock();		// TODO: Use the clock to count hotspot durations.
		var viewerMode = 'standard';		// TODO: This should be set in the scope.

		scope.init = function()
		{
			clock.start();

			// TODO: Retrieve correctly.
			scope.activeProject = activeProject;		// projectjson.js

			// Scene
			scene = new THREE.Scene();

			// Renderer
			renderer = new THREE.WebGLRenderer();
			renderer.setClearColor(0xffffff);
			renderer.setSize(contW, contH);
			domElement = renderer.domElement;

			// Camera
			camera = new THREE.PerspectiveCamera(90, windowHalfX / windowHalfY, 0.001, 10000);
			camera.position.set(0, 10, 0);
			scene.add(camera);

			// Crosshairs HUD
			scope.crosshairs();

			// Element is provided by the angular directive
			sceneViewerCanvas = element[0].appendChild(domElement);
			sceneViewerCanvas.id = 'sceneViewerCanvas';

			// Controls
			controls = new THREE.OrbitControls(camera, domElement);
			controls.rotateUp(Math.PI / 4);
			controls.target.set(
				camera.position.x + 0.1,
				camera.position.y,
				camera.position.z
			);
//			controls.connect();
            controls.update();

			window.addEventListener('resizeCanvas', scope.onWindowResize, false);

			// Device orientation controls
			window.addEventListener('deviceorientation', scope.setOrientationControls, true);

			// Continually update the camera orientation with a self-calling function.
			scope.drawCameraOrientation();
			scope.drawCountdownCanvas();
			scope.createAudioPlayer();

			scope.mode = 'vr';
			scope.toggleMode();

			scope.loadPage();
		};

		scope.loadPage = function()
		{
			// Background
			if (currentSphere)
			{
				scene.remove(currentSphere);
			}

			scope.activeScene = scope.getNextSceneInSequence();

			if (scope.activeScene.backgroundFile)
			{
				if (scope.activeScene.backgroundFile.fileType == 'image')
				{
					scope.activeScene.background = new THREE.Mesh(
						new THREE.SphereGeometry(100, 32, 32),
						new THREE.MeshBasicMaterial({
							map: THREE.ImageUtils.loadTexture(scope.activeScene.backgroundFile.filePath)
						})
					);
					scope.activeScene.background.scale.x = -1;
					scene.add(scope.activeScene.background);
				}
			}
			else
			{
				// If there's no background, show a blank screen.
			}

			// Continually check if the user is hitting any hotspots. In standard mode, the user clicks on it.
			// In VR mode, this will be triggered if the camera is looking within its radius. If they are,
			// flag it as true and start counting the number of seconds that the user holds position within
			// the radius. If the camera moves out the hotspot, flag it as false and stop counting. If the
			// flag is true and the duration of the hotspot is reached, start activating the actions in the
			// event. All actions will occur in sequential order.
//			scope.drawHotspotLabels();
			hotspots = scope.activeScene.hotspots;
			scope.scanHotspots();
			scope.executeActions();
		}

		scope.getNextSceneInSequence = function(currentSceneId)
		{
			// If a null or invalid scene is passed, start out with the first scene in the project.
			if (typeof currentSceneId == 'undefined' || isNaN(currentSceneId))
			{
				// Check the startScene variable, if it exists. If not, find the first scene in the array.
				var firstScene;
				if (scope.activeProject.startScene && !isNaN(scope.activeProject.startScene))
				{
					scope.activeProject.scenes.forEach(function(sc)
					{
						if (sc.id == scope.activeProject.startScene)
						{
							firstScene = sc;
						}
					});
				}

				if (firstScene)
				{
					scope.activeScene = firstScene;
					return scope.activeScene;
				}
				else if (scope.activeProject.scenes && scope.activeProject.scenes.length >= 1)
				{
					scope.activeScene = scope.activeProject.scenes[0];
					return scope.activeScene;
				}
			}
			else
			{
				// Find the scene id in the project. If it doesn't exist, call this function again without any parameters
				// to get the first one in the project.
				if (scope.activeProject.scenes && scope.activeProject.scenes.length >= 1)
				{
					scope.activeProject.scenes.forEach(function(sc)
					{
						if (sc.id == currentSceneId)
						{
							scope.activeScene = sc;
							return scope.activeScene;
						}
					});
					return scope.getNextSceneInSequence();
				}
			}
		}



        // -----------------------------------
        // Event listeners
        // -----------------------------------
        scope.onWindowResize = function () {

          scope.resizeCanvas();

        };
/*
		scope.drawHotspotLabels = function()
		{
			// TODO: If a custom image is specified for the hotspot, add it under the text label.

			// Add the text labels.
			// See https://gist.github.com/ekeneijeoma/1186920
			hotspots.forEach(function(hotspot)
			{
				if (typeof hotspot.label !== 'undefined')
				{
					var mesh = scope.createLabel(hotspot.label, 10, 10, 10, 14, 'white', 'black');
					scene.addObject(mesh);
				}
			});
		}

		scope.createLabel = function(text, x, y, z, size, color, backGroundColor, backgroundMargin)
		{
			if (!backgroundMargin)
				backgroundMargin = 50;

			var canvas = document.createElement("canvas");

			var context = canvas.getContext("2d");
			context.font = size + "pt Arial";

			var textWidth = context.measureText(text).width;

			canvas.width = textWidth + backgroundMargin;
			canvas.height = size + backgroundMargin;
			context = canvas.getContext("2d");
			context.font = size + "pt Arial";

			if(backGroundColor) {
				context.fillStyle = backGroundColor;
				context.fillRect(canvas.width / 2 - textWidth / 2 - backgroundMargin / 2, canvas.height / 2 - size / 2 - +backgroundMargin / 2, textWidth + backgroundMargin, size + backgroundMargin);
			}

			context.textAlign = "center";
			context.textBaseline = "middle";
			context.fillStyle = color;
			context.fillText(text, canvas.width / 2, canvas.height / 2);

			// context.strokeStyle = "black";
			// context.strokeRect(0, 0, canvas.width, canvas.height);

			var texture = new THREE.Texture(canvas);
			texture.needsUpdate = true;

			var material = new THREE.MeshBasicMaterial({
				map : texture
			});

			var mesh = new THREE.Mesh(new THREE.PlaneGeometry(canvas.width, canvas.height), material);
			// mesh.overdraw = true;
			mesh.doubleSided = true;
			mesh.position.x = x - canvas.width;
			mesh.position.y = y - canvas.height;
			mesh.position.z = z;

			return mesh;
		}
*/
		var countdownCanvas;
		var countdownContext;
		var countdownImageData;
		scope.drawCountdownCanvas = function()
		{
			countdownCanvas = document.createElement('canvas');
			countdownCanvas.id = 'hotspotCountdownCanvas';		// TODO: Randomize the id?
			countdownCanvas.width = 100;
			countdownCanvas.height = 100;
			element[0].appendChild(countdownCanvas);
	
			countdownContext = countdownCanvas.getContext('2d');
			countdownContext.beginPath();
			countdownContext.strokeStyle = 'white';
			countdownContext.lineCap = 'square';
			countdownContext.closePath();
			countdownContext.fill();
			countdownContext.lineWidth = 15;

			countdownImageData = countdownContext.getImageData(0, 0, 75, 75);
			countdownCanvas.style.display = 'none';
		}



		scope.drawCountdown = function(current)
		{
			var circ = Math.PI * 2;
			var quart = Math.PI / 2;
			countdownContext.clearRect(0, 0, countdownCanvas.width, countdownCanvas.height);
			if (current == 0)
			{
//				return;
			}

			countdownContext.putImageData(countdownImageData, 0, 0);
			countdownContext.beginPath();
			countdownContext.arc(50, 50, 25, -(quart), ((circ) * current) - (quart), false);
			countdownContext.stroke();
		}



		var audioPlayer;
		scope.createAudioPlayer = function()
		{
			audioPlayer = document.createElement('audio');
			audioPlayer.id = 'audioPlayer';					// TODO: Randomize the id?
			audioPlayer.autoplay = false;
			audioPlayer.loop = false;
			audioPlayer.width = 0;
			audioPlayer.heigth = 0;
			element[0].appendChild(audioPlayer);
		}



		// TODO: Should this be put in a scope.$watch() function?
		scope.executeActions = function()
		{
			if (activeActionSequences.length > 0)
			{
				alert('test');
			}
		}



		scope.scanHotspots = function()
		{
//			if (typeof scope.activeScene === 'undefined' || typeof scope.activeScene.hotspots === 'undefined')
			if (1 == 2)
			{
			}
			else
			{
//				hotspots = scope.activeScene.hotspots;
				hotspots.forEach(function(hotspot)
				{
					// Get the camera orientation.
					// TODO: Move this outside the forEach() loop?
					var pLocal = new THREE.Vector3(0, 0, -1);
					var pWorld = pLocal.applyMatrix4(camera.matrixWorld);
					var dir = pWorld.sub(camera.position).normalize()
					dir = pWorld;
					var x = Math.round(dir.x * 100000) / 100000;
					var y = Math.round(dir.y * 100000) / 100000;
					var z = Math.round(dir.z * 100000) / 100000;
	
					// To calculate if we're within the hotspot, we take the absolute value of the difference
					// between the current X position and the hotspot's X position, and the current Y position
					// and the hotspot's Y position. Then we sum the two. If it's less than the value of the
					// radius, we're within the hotspot.

					// TODO: Don't re-activate a hotspot if we've just barely finished activating it and are still
					// within the radius.
					if (lastHotspot && lastHotspot.id == hotspot.id)
					{
						return;
					}

					// Only focus on the active hotspot if activeHotspot is set.
					if (scope.activeHotspot)
					{
						var diffX = Math.abs(x - scope.activeHotspot.positionX);
						var diffY = Math.abs(y - scope.activeHotspot.positionY);
						var diffTotal = diffX + diffY;

						// If the camera has moved away from the hotspot, disable activeHotspot.
						if (diffTotal > hotspot.radius)
						{
							scope.activeHotspot = null;
							scope.drawCountdown(0);
							countdownCanvas.style.display = 'none';
						}
						else
						{
							countdownCanvas.style.display = 'block';

							// If we're in VR mode, run the function to detect how long the hotspot has been
							// triggered. If it reaches the hotspot's duration, start executing the actions.
//							if (viewerMode == 'vr')
							if (viewerMode == 'standard')
							{
								var timeElapsed = clock.getElapsedTime() - scope.activeHotspot.startTime;
								var percentComplete = timeElapsed / (scope.activeHotspot.duration / 1000)
								scope.percentComplete = percentComplete;
	
								// If we've reached 100%, execute the action.
								if (percentComplete >= 1)
								{
									// Track this as the last hotspot so it's not immediately reactivated.
									lastHotspot = hotspot;
									alert(lastHotspot.id);

									// Get rid of the hotspot.
									scope.drawCountdown(0);
									countdownCanvas.style.display = 'none';
									scope.percentComplete = 0;

									// Add the actions to the queue.
									activeActionSequences.push(scope.activeHotspot.actions);
									alert(activeActionSequences.length);
									scope.activeHotspot = null;
								}
								else
								{
									scope.drawCountdown(percentComplete);
								}
							}
						}
					}
					else
					{
						var diffX = Math.abs(x - hotspot.positionX);
						var diffY = Math.abs(y - hotspot.positionY);
						var diffTotal = diffX + diffY;
	
						if (diffTotal <= hotspot.radius)
						{
							scope.activeHotspot = hotspot;
	
							// We're entering a new hotspot, so start the clock.
							scope.drawCountdown(0);
							countdownCanvas.style.display = 'block';
							scope.activeHotspot = JSON.parse(JSON.stringify(hotspot));
							scope.activeHotspot.startTime = clock.getElapsedTime();
						}
						else
						{
							// Deactivate the countdown and last hotspot.
							scope.drawCountdown(0);
							countdownCanvas.style.display = 'none';
							scope.activeHotspot = null;
							lastHotspot = null;
						}
					}
				});
			}
			window.requestAnimationFrame(scope.scanHotspots);
		}



		scope.toggleMode = function()
		{
			if (scope.mode == 'vr')
			{
				// Stereoscopic effect
				stereoEffect = new THREE.StereoEffect(renderer);
				stereoEffect.separation = -6.2;
				stereoEffect.setSize(contW, contH);
			}
			else
			{
				renderer.deallocateObject(stereoEffect);
				window.removeEventListener('deviceorientation', scope.setOrientationControls, true);
			}
		}

		scope.setOrientationControls = function(e)
		{
			if (!e.alpha)
			{
//				return;
			}
	
			controls = new THREE.DeviceOrientationControls(camera, true);
			controls.connect();
			controls.update();

			// Original code forgot to add ", true", so the listener was never removed
			window.removeEventListener('deviceorientation', scope.setOrientationControls, true);
		}

		// -----------------------------------
		// Updates
		// -----------------------------------
		scope.resizeCanvas = function()
		{
			contW = (scope.fillcontainer) ? 
				element[0].clientWidth : scope.width;
			contH = scope.height;

			windowHalfX = contW / 2;
			windowHalfY = contH / 2;

			camera.aspect = contW / contH;
			camera.updateProjectionMatrix();
				renderer.setSize( contW, contH );
		};

        // -----------------------------------
        // Draw and Animate
        // -----------------------------------
        scope.crosshairs = function()
        {
			var crosshairTexture = THREE.ImageUtils.loadTexture('assets/images/crosshairs04.png');
			var crosshairMaterial = new THREE.SpriteMaterial( { map: crosshairTexture, depthTest: false } );
			var crosshairSprite = new THREE.Sprite(crosshairMaterial);
			//scale the crosshairSprite down in size
			crosshairSprite.scale.set(0.5, 0.5, 0.5	);
			//add crosshairSprite as a child of our camera object, so it will stay centered in camera's view
			camera.add(crosshairSprite);
			//position sprites by percent X:(100 is all the way to the right, 0 is left, 50 is centered)
			//                            Y:(100 is all the way to the top, 0 is bottom, 50 is centered)
			var crosshairPercentX = 50;
			var crosshairPercentY = 50;
			var crosshairPositionX = (crosshairPercentX / 100) * 2 - 1;
			var crosshairPositionY = (crosshairPercentY / 100) * 2 - 1;
			crosshairSprite.position.x = crosshairPositionX * camera.aspect;
			crosshairSprite.position.y = crosshairPositionY;
			crosshairSprite.position.z = -1.5;
        }

		scope.drawCameraOrientation = function()
		{
			if (!cameraOrientationCanvas)
			{
				cameraOrientationCanvas = document.createElement('canvas');
				cameraOrientationCanvas.id = 'cameraOrientation';
				cameraOrientationCanvas.width = 640;
				cameraOrientationCanvas.height = 25;
				element[0].appendChild(cameraOrientationCanvas);
			}

			// Get the camera orientation.
			var pLocal = new THREE.Vector3(0, 0, -1);
			var pWorld = pLocal.applyMatrix4(camera.matrixWorld);
			var dir = pWorld.sub(camera.position).normalize()
			dir = pWorld;
			var x = Math.round(dir.x * 100000) / 100000;
			var y = Math.round(dir.y * 100000) / 100000;
			var z = Math.round(dir.z * 100000) / 100000;
			var text = 'X: ' + x + '   Y: ' + y + '   Z: ' + z;

			// Draw the coordinates.
			var ctx = cameraOrientationCanvas.getContext('2d');
			ctx.clearRect(0, 0, cameraOrientationCanvas.width, cameraOrientationCanvas.height);
			ctx.rect(0, 0, cameraOrientationCanvas.width, cameraOrientationCanvas.height);
			ctx.font = '16px Arial';
			ctx.strokeStyle = 'black';
			ctx.lineWidth = 2;
			ctx.strokeText(text, 20, 20);
			ctx.fillStyle = 'white';
			ctx.fillText(text, 20, 20);
			window.requestAnimationFrame(scope.drawCameraOrientation);
		}

		scope.animate = function(dt)
		{
			requestAnimationFrame( scope.animate );
//			camera.updateProjectionMatrix();
			controls.update(dt);
			scope.render();
		};

		scope.render = function()
		{
			// TODO: Add billboards to scene for hotspot labels that always face the camera.
			// See http://stackoverflow.com/questions/16001208/how-can-i-make-my-text-labels-face-the-camera-at-all-times-perhaps-using-sprite

			// Render the view.
			if (stereoEffect)
				stereoEffect.render( scene, camera );
			else
				renderer.render( scene, camera );
		};

        // -----------------------------------
        // Watches
        // -----------------------------------
        scope.$watch('fillcontainer + width + height', function() {

			scope.resizeCanvas();
        
        });

        scope.$watch('scale', function() {
        
//			scope.resizeObject();
        
        });

		scope.$watch('activeHotspot', function() {
		});

		scope.$watch('mode', function() {
//			scope.toggleMode();
		});

        // Begin
        scope.init();
        scope.animate();
      }
    };
  });
