<?php
class Action extends Eloquent
{
	protected $id;
	protected $eventId;
	protected $type;				// Options: image, spritemap, audio, video, text, jump, variable, url, changeLabel.
	protected $sequence;
	protected $continue;
	protected $mediaId;
	protected $params;				// This object will be different, depending on $type.

	protected $guarded = array('id');
	protected $table = 'actions';

	public function event()
	{
		return $this->belongsTo('SceneEvent', 'eventId', 'id');
	}

	public function media()
	{
		return $this->hasOne('Media', 'mediaId', 'id');
	}

	public function conditions()
	{
		return $this->hasMany('ConditionsAction', 'actionId', 'id');
	}
}
?>