spherecastApp.factory('Scene', function($http)
{
	return 	{
		// Get a specific scene.
		get: function(sceneId) {
			return $http.get('/api/scene/' + sceneId);
		},

		// Save a scene.
		save: function(sceneData) {
			return $http({
				method: 'POST',
				url: '/api/scene',
				headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
				data: $.param(mediaData)
			});
		},

		// Destroy a scene.
		destroy: function(id) {
			return $http.delete('/api/scene/' + id);
		}
	}
});
