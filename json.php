<?php
/*
 * For the JSON, certain "starts" and "ends" events are created automatically to indicate how scenes
 * should progress, even if not explicitly stated. So for example, a project with scenes that auto-advance
 * when a voiceover is complete would have one "starts" event that has the voiceover, then a scene jump.
 *
 * A blank scene that waits 2 seconds before loading the next scene could be constructed by just having
 * an event with blank text displayed for an action, with a duration of 2.
 *
 * Events occur all at once, but actions are processed in sequence.
 *
 * There can only be one background per scene, and it can only be an image or a video.
 *
 * Any conditions added to an action involve global variables. If there are multiple conditions, they are
 * must ALL be true for the action to resolve (logical AND). To effectively create logical OR operators,
 * the same action can be created multiple times in an event, but with different conditions.
 */
$project = new stdClass;
$project->id = 1;
$project->name = 'Ocean Exploration';
$project->subtitle = '';
$project->description = 'Join these scuba divers and explore the magnificent wonders of the ocean.';
$project->createdBy = 1;
$project->createdTime = 0;
$project->settings = array();
$project->scenes = array();

	$settings = new stdClass;
	$settings->showTapToBeginScreen = true;
	$settings->autoAdvancePrimaryVideoEnd = true;
	$settings->autoAdvancePrimaryAudioEnd = true;
	$settings->modeDesktop = true;
	$settings->modeVR = true;
	$settings->globalVariables = array();

		// Global variables are always user-defined. They can be named anything and set to anything (numeric or string).
		// Booleans are not allowed, since 0s and 1s can just as easily be used. All variables must be given a default
		// value, which is specified here. Otherwise they're null.
		$globals = new stdClass;
		$globals->score = 0;
		$globals->viewedJellyfish = 0;
		$globals->watchedSubmarineVideo = 0;
		$globals->teacherName = 'Mr. Reeve';

$scene = new stdClass;
$scene->id = 1;
$scene->title = 'Entering the Depths';
$scene->subtitle = '';
$scene->description = '';
$scene->sequence = 1;
$scene->parent = 0;
$scene->createdBy = 1;
$scene->createdTime = 0;
$scene->modifiedTime = 0;
$scene->lockedBy = 0;
$scene->lockedTime = 0;
$scene->showGroundPanel = true;					// A nav panel at the user's feet (only on VR mode) that contains "Previous" and "Next" hotspots.
$scene->background = $imagefile;
$scene->backgroundRepeat = false;				// Only used for videos.
$scene->autoAdvancePrimaryVideoEnd = false;		// Overrides what's set in the defaults.
$scene->autoAdvancePrimaryAudioEnd = false;		// Overrides what's set in the defaults.
$scene->autoAdvanceDuration = 0;				// If higher than zero, automatically advance the scene after that many seconds no matter what.
$scene->audio = array();						// TODO: We may not need this as a $scene variable in the JSON, since it'll be in the "starts" event.
$scene->events = array();

	// Photo sphere image, the primary background image for the scene.
	$imagefile = new stdClass;
	$imagefile->id = 1;
	$imagefile->contenthash = '494679425abebbdeb7457822e9dcff2d4273de0b';
	$imagefile->filepath = '/';
	$imagefile->mimetype = 'image/jpeg';
	$imagefile->uploadedBy = 1;
	$imagefile->width = 2048;
	$imagefile->height = 1024;
	$imagefile->filename = 'ocean-exploration-01-entering-the-depths.jpg';
	$imagefile->filesize = 105054;
	$imagefile->filetype = 'jpg';
	$imagefile->title = 'Entering the Depths';
	$imagefile->timecreated = 0;

	// Two audio files: One voiceover and one ambient. The voiceover is the "primary" audio for the scene.
	$audiofile = new stdClass;
	$audiofile->id = 1;
	$audiofile->primary = true;
	$audiofile->contenthash = 'f51cd3358db6e6f531d0e50dd86678147b19f6cc';
	$audiofile->filepath = '/';
	$audiofile->mimetype = 'audio/mpeg';
	$audiofile->uploadedBy = 1;
	$audiofile->filename = 'ocean-exploration-01.mp3';
	$audiofile->filesize = 105054;
	$audiofile->filetype = 'mp3';
	$audiofile->title = 'Entering the Depths Voiceover';
	$audiofile->timecreated = 0;

	$audiofile = new stdClass;
	$audiofile->id = 2;
	$audiofile->primary = false;
	$audiofile->contenthash = 'f51cd3358db6e6f531d0e50dd86678147b19f6cc';
	$audiofile->filepath = '/';
	$audiofile->mimetype = 'audio/mpeg';
	$audiofile->uploadedBy = 1;
	$audiofile->filename = 'ocean-exploration-ambient.mp3';
	$audiofile->filesize = 105054;
	$audiofile->filetype = 'mp3';
	$audiofile->title = 'Ambient Ocean Noises';
	$audiofile->timecreated = 0;

	// Event that starts a scene with ambient audio and some pop-up text, one which displays a global variable.
	$event = new stdClass;
	$event->id = 10;
	$event->type = 'starts';
	$event->positionX = '';
	$event->positionY = '';
	$event->positionZ = '';
	$event->radius = 10;
	$event->actions = array();

		$action = new stdClass;
		$action->id = 1;
		$action->action = 'audio';
		$action->itemid = 50;
		$action->params = array();

			$actionparams = new stdClass;
			$actionparams->id = 50;
			$actionparams->filename = 'ocean-ambient.mp3';
			$actionparams->repeat = true;

		$action = new stdClass;
		$action->id = 2;
		$action->action = 'text';
		$action->itemid = '';
		$action->params = $actionparams;

			$actionparams = new stdClass;
			$actionparams->text = 'Teacher: {{teacherName}}';
			$actionparams->positionType = 'overlay';
			$actionparams->positionX = '';
			$actionparams->positionY = '';
			$actionparams->positionZ = '';
			$actionparams->duration = 5;

		$action = new stdClass;
		$action->id = 3;
		$action->action = 'text';
		$action->itemid = '';
		$action->params = $actionparams;

			$actionparams = new stdClass;
			$actionparams->text = 'The following photo spheres were taken from the Catlin Seaview Survey';
			$actionparams->positionType = 'position';
			$actionparams->positionX = 100.2;
			$actionparams->positionY = 50.5;
			$actionparams->positionZ = 25.8;
			$actionparams->duration = 5;

	// Event that's activitated by a hotspot, and shows popup text, then an image, then an animated image
	// at a specific position that doesn't stop until the scene ends.
	$event = new stdClass;
	$event->id = 10;
	$event->type = 'hotspot';
	$event->positionX = 10.1;
	$event->positionY = -20.2;
	$event->positionZ = 30.3;
	$event->radius = 10;
	$event->actions = array();
	$event->params = $eventparams;

		$eventparams = new stdClass;
		$eventparams->allowMouseClick = true;
		$eventparams->allowMouseDoubleClick = true;
		$eventparams->allowLook = true;
		$eventparams->lookDuration = 2;
		$eventparams->lookVisible = false;		// Invisible hotspot (no indication it's being viewed)
		$eventparams->allowMagnetPull = false;
		$eventparams->allowBluetoothAdvance = false;

		$action = new stdClass;
		$action->id = 1;
		$action->action = 'text';
		$action->itemid = 50;
		$action->params = array();

			$actionparams = new stdClass;
			$actionparams->text = 'Jellyfish';
			$actionparams->positionType = 'overlay';
			$actionparams->positionX = '';
			$actionparams->positionY = '';
			$actionparams->positionZ = '';
			$actionparams->duration = 5;

		$action = new stdClass;
		$action->id = 2;
		$action->action = 'image';
		$action->itemid = 60;
		$action->params = $actionparams;

			$actionparams = new stdClass;
			$actionparams->file = $imagefile;
			$actionparams->positionType = 'overlay';
			$actionparams->positionX = '';
			$actionparams->positionY = '';
			$actionparams->positionZ = '';
			$actionparams->width = '';			// Resized width of image; default is 75% of screen for overlay
			$actionparams->height = '';			// Resized height of image; default preserves aspect ratio of width
			$actionparams->duration = 5;

				// Flat image, the primary background image for the scene.
				$imagefile = new stdClass;
				$imagefile->id = 1;
				$imagefile->contenthash = '494679425abebbdeb7457822e9dcff2d4273de0b';
				$imagefile->filepath = '/';
				$imagefile->mimetype = 'image/jpeg';
				$imagefile->uploadedBy = 1;
				$imagefile->width = 640;		// Original width of image
				$imagefile->height = 480;		// Original height of image
				$imagefile->filename = 'jellyfish.jpg';
				$imagefile->filesize = 105054;
				$imagefile->filetype = 'jpg';
				$imagefile->title = 'Jellyfish';
				$imagefile->timecreated = 0;

		$action = new stdClass;
		$action->id = 3;
		$action->action = 'spritemap';
		$action->itemid = 70;
		$action->params = $actionparams;

			$actionparams = new stdClass;
			$actionparams->filename = 'jellyfish-spritemap.png';
			$actionparams->positionType = 'position';
			$actionparams->positionX = 100.18;
			$actionparams->positionY = 195.55;
			$actionparams->positionZ = 139.62;
			$actionparams->duration = 0;		// Leave the image up permanently (until the scene ends)

	// Event that's activated by a hotspot, shows some text, plays a video, then increments the
	// "viewedJellyfish" global variable by 1, and the "score" global variable by 50.
	$event = new stdClass;
	$event->id = 15;
	$event->type = 'hotspot';
	$event->positionX = -40.4;
	$event->positionY = 50.5;
	$event->positionZ = -60.6;
	$event->radius = 10;
	$event->actions = array();
	$event->params = $eventparams;

		$eventparams = new stdClass;
		$eventparams->allowMouseClick = true;
		$eventparams->allowMouseDoubleClick = true;
		$eventparams->allowLook = true;
		$eventparams->lookDuration = 2;
		$eventparams->lookVisible = false;		// Invisible hotspot (no indication it's being viewed)
		$eventparams->allowMagnetPull = false;
		$eventparams->allowBluetoothAdvance = false;
		$eventparams->imageFile;				// A file object that's used instead of the standard hotspot indicator. This is particularly used each time a scene has a "bottom nav" trigger.

		$action = new stdClass;
		$action->id = 1;
		$action->action = 'video';
		$action->itemid = 50;
		$action->params = $actionparams;

			$actionparams = new stdClass;
			$actionparams->text = 'Jellyfish';
			$actionparams->positionType = 'overlay';
			$actionparams->positionX = '';
			$actionparams->positionY = '';
			$actionparams->positionZ = '';
			$actionparams->duration = 5;

		$action = new stdClass;
		$action->id = 2;
		$action->action = 'video';
		$action->itemid = '';
		$action->params = $actionparams;

			$actionparams = new stdClass;
			$actionparams->filename = 'jellyfish.mp4';
			$actionparams->positionType = 'overlay';
			$actionparams->positionX = '';
			$actionparams->positionY = '';
			$actionparams->positionZ = '';
			$actionparams->allowRewindByVoice;
			$actionparams->allowForwardByVoice;
			$actionparams->allowPauseByVoice;
			$actionparams->allowPlayByVoice;
			$actionparams->allowStopByVoice;	// Allow using "stop" vocal command to stop playing the video altogether.
			$actionparams->autoplay;

		$action = new stdClass;
		$action->id = 3;
		$action->action = 'variable';
		$action->itemid = '';
		$action->params = $actionparams;

			$actionparams = new stdClass;
			$actionparams->variableName = 'viewedJellyfish';
			$actionparams->operation = 'add';				// Options: add, subtract, multiply, divide, modulus, set
			$actionparams->operand = '1';

		$action = new stdClass;
		$action->id = 4;
		$action->action = 'variable';
		$action->itemid = '';
		$action->params = $actionparams;

			$actionparams = new stdClass;
			$actionparams->variableName = 'score';
			$actionparams->operation = 'add';				// Options: add, subtract, multiply, divide, modulus, set
			$actionparams->operand = '50';

	// Event that's activitated by a voice command, and jumps to another scene, but only if the
	// "viewedJellyfish" global variable is set to "1."
	$event = new stdClass;
	$event->id = 10;
	$event->type = 'voice';
	$event->positionX = '';
	$event->positionY = '';
	$event->positionZ = '';
	$event->radius = '';
	$event->params = $eventparams;
	$event->actions = $actionparams;

		$eventparams = new stdClass;
		$eventparams->command = 'Next';
		$eventparams->minConfidence = 0.75;

		$action = new stdClass;
		$action->id = 20;
		$action->action = 'jump';
		$action->itemid = 72;		// Scene id
		$action->conditions = array();

			$condition = new stdClass;
			$condition->variable = 'viewedJellyfish';
			$condition->operator = '=';
			$condition->operand = '1';
?>