<?php
class MediaFolder extends Eloquent
{
	protected $id;
	protected $projectId;
	protected $name;
	protected $parentId;

	protected $fillable = array('projectId', 'name', 'parentId');
	protected $table = 'media_folders';

	function __construct()
	{
		if ($this->parentId != 0)
			$this->parent->with('parent');
	}

	public function project()
	{
		return $this->belongsTo('Project', 'projectId', 'id');
	}

	public function media()
	{
		return $this->hasMany('Media', 'id', 'folderId');
	}

	public function parent()
	{
		return $this->belongsTo('MediaFolder', 'parentId');
	}

	public function children()
	{
		return $this->hasMany('MediaFolder', 'parentId');
	}

	public function childrenRecursive()
	{
		return $this->children()->with('childrenRecursive');
	}

	public function parentRecursive()
	{
		return $this->parent()->with('parentRecursive');
	}
}
?>