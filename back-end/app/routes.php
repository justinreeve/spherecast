<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
 * Example of a project: /api/project/{id} or /api/project/{shortname}
 * Example of a scene: /api/project/{projectid}/{sceneid} or /api/scene/{id}
 */
Route::get('/', function()
{
	return View::make('index');
});

Route::get('upload', function()
{
	return View::make('upload');
});

Route::get('docs', function() { return View::make('docs.project'); });
Route::get('docs/project', function() { return View::make('docs.project'); });
Route::get('docs/scenes', function() { return View::make('docs.scenes'); });
Route::get('docs/events', function() { return View::make('docs.events'); });
Route::get('docs/hotspots', function() { return View::make('docs.hotspots'); });
Route::get('docs/actions', function() { return View::make('docs.actions'); });
Route::get('docs/conditions', function() { return View::make('docs.conditions'); });
Route::get('docs/globalvariables', function() { return View::make('docs.globalvariables'); });
Route::get('docs/assets', function() { return View::make('docs.assets'); });

// Route::get('project/{id}/startScene', 'ProjectController@startScene');
Route::get('project/{id}/startScene', 'ProjectController@startScene');
Route::get('project/{id}/metadata', 'ProjectController@getMetaData');
Route::resource('project', 'ProjectController');

Route::resource('scene', 'SceneController');
Route::get('scene/{id}/metadata', 'SceneController@getMetaData');

Route::resource('media', 'MediaController');
Route::resource('event', 'SceneEventController');
Route::resource('action', 'ActionController');
Route::resource('log', 'LogController');

Route::get('download/{contenthash}/{filesize}', 'MediaController@download');
Route::get('media/{projectId}/files', 'MediaController@getFileArray');

// Route::get('media/{projectId}/{mediaId?}/thumbnail/{size?}', 'MediaController@showThumbnail')
//	->where('mediaId', '[0-9]+');
Route::get('media/{projectId}/{path?}/thumbnail/{size?}', 'MediaController@showThumbnail')
	->where('path', '(.*)');
// Route::get('media/{projectId}/{mediaId?}', 'MediaController@show')
//	->where('mediaId', '[0-9]+');
Route::get('media/{projectId}/{path?}', 'MediaController@show')
	->where('path', '(.*)');

// All routes that are not /home or /api will be redirected to the front-end, so Angular can route them. 
// TODO: This is probably not necessary with the way we've set up Apache to always point to Laravel
// when /api is specified.
App::missing(function($exception)
{ 
	return View::make('index');
});

// App::instance();			// Make an object persist (?).

// Route::get('projects', 'ProjectController@getAllProjects');

// The {project} can be either a number ("id") or a string ("shortname").
/*
Route::model('projects', 'Project');
Route::resource('projects', 'ProjectController');
Route::bind('project', function($value, $route)
{
	if (is_numeric($value))
		return Project::find($value);
	else
		return Project::whereShortname($value)->first();
});
Route::get('projects/{project}', function($project)
{
	return $project;
});
*/

/*
Route::get('project/{project}', 'ProjectController@getProject');

Route::get('project/{projectid}/scenes', 'SceneController@getScenes');

Route::get('sceneevent/{id}', function($sceneeventid)
{
});

Route::get('action/{id}', function($actionid)
{
});
*/