var projectjson4 = {
	id: 4,
	shortname: 'littleredhen',
	title: 'The Little Red Hen',
	subtitle: '',
	description: '',
	createdby: 1,
	timecreated: 0,
	startScene: 1,

	globalVariables: [
	{
		name: 'finishedStory',
		defaultValue: '0',
	}],

	prefetchAssets: [
	{
	}],

	scenes: [
	{
		id: 1,
		shortname: '',
		projectid: 4,
		sequence: 1,
		parent: 0,
		title: '',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: 51,
			filePath: '/files/2d/2d8a608be5249ee1425ce6339d35c9ab7db54f29',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
			id: 100,
			visible: 0,
			overridable: 1,
			actions: [
			{
				actionType: 'variable',
				actionVariableName: 'finishedStory',
				actionVariableOperator: 'set',
				actionVariableOperand: '0',
				actionSequence: 1,
				actionContinue: false,					// Should always be "false" when setting a variable.
			},
			{
				actionType: 'text',
				actionText: 'Starting in 5...',
				actionDuration: 1000,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: 'Starting in 4...',
				actionDuration: 1000,
				actionSequence: 3,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: 'Starting in 3...',
				actionDuration: 1000,
				actionSequence: 4,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: 'Starting in 2...',
				actionDuration: 1000,
				actionSequence: 5,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: 'Starting in 1...',
				actionDuration: 1000,
				actionSequence: 6,
				actionContinue: false,
			},
			{
				actionType: 'jump',
				actionItemId: 2,
				actionSequence: 7,
				actionContinue: false,
			}],
		}],

		endEvents: [],

		hotspots: [],
	},
	{
		id: 2,
		shortname: '',
		projectid: 4,
		sequence: 1,
		parent: 0,
		title: '',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: 23,
			filePath: '/files/2a/2a0a397d1b2ce69b60337658ea8d3dea4f5a9d48',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
			id: 200,
			visible: 0,
			overridable: 1,
			actions: [
			{
				actionType: 'audio',
				actionItemId: 9,
				actionFile:
				{
					fileId: 37,
					filePath: '/files/a1/a1e4c1ab0ed6772294a5ef6a3e47d74de25f4662',
					fileType: 'audio',
				},
				actionVolume: 1.0,
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'text',							// For the JSON, should we make an actionType of "wait" for pauses?
				actionText: '',
				actionDuration: 2000,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'jump',
				actionDelay: 2000,
				actionItemId: 3,
				actionSequence: 3,
				actionContinue: false,
			}],
		}],

		endEvents: [],

		hotspots: [],
	},
	{
		id: 3,
		shortname: '',
		projectid: 4,
		sequence: 1,
		parent: 0,
		title: '',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: 24,
			filePath: '/files/b7/b7772e92c3c2a9365c673436a3da54f434a9c61c',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
			id: 300,
			visible: 0,
			overridable: 1,
			actions: [
			{
				actionType: 'audio',
				actionItemId: 9,
				actionFile:
				{
					fileId: 38,
					filePath: '/files/fe/fe3de32f00f7e0f99c3782982474f50f0a329a72',
					fileType: 'audio',
				},
				actionVolume: 1.0,
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionDuration: 2000,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'jump',
				actionDelay: 2000,
				actionItemId: 4,
				actionSequence: 3,
				actionContinue: false,
			}],
		}],

		endEvents: [],

		hotspots: [],
	},
	{
		id: 4,
		shortname: '',
		projectid: 4,
		sequence: 1,
		parent: 0,
		title: '',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: 25,
			filePath: '/files/3b/3bdadaf7ee25de472e107568232d6a239352469f',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
			id: 400,
			visible: 0,
			overridable: 1,
			actions: [
			{
				actionType: 'audio',
				actionItemId: 9,
				actionFile:
				{
					fileId: 39,
					filePath: '/files/d6/d6c98be7f18c499cc90c8e2ed43380c4dfe0d1c3',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionDuration: 2000,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'jump',
				actionItemId: 5,
				actionSequence: 3,
			}],
		}],

		endEvents: [],

		hotspots: [],
	},
	{
		id: 5,
		shortname: '',
		projectid: 4,
		sequence: 1,
		parent: 0,
		title: '',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: 26,
			filePath: '/files/e8/e893950fbdb34cf85e54e8fa3810dcbeeb67bb84',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
			id: 500,
			visible: 0,
			overridable: 1,
			actions: [
			{
				actionType: 'audio',
				actionItemId: 9,
				actionFile:
				{
					fileId: 40,
					filePath: '/files/2c/2c092ff4eaa8232262e3ff95e73cb33de08b7a45',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionDuration: 2000,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'jump',
				actionDelay: 2000,
				actionItemId: 6,
				actionSequence: 3,
			}],
		}],

		endEvents: [],

		hotspots: [],
	},
	{
		id: 6,
		shortname: '',
		projectid: 4,
		sequence: 1,
		parent: 0,
		title: '',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: 27,
			filePath: '/files/9a/9aee6ffa2d7ee875b0ce24f8e17767ac4a67776d',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
			id: 600,
			visible: 0,
			overridable: 1,
			actions: [
			{
				actionType: 'audio',
				actionItemId: 9,
				actionFile:
				{
					fileId: 41,
					filePath: '/files/59/594e16b86a3148e965926b26b888fdccf61e63ea	',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionDuration: 2000,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'jump',
				actionDelay: 2000,
				actionItemId: 7,
				actionSequence: 3,
			}],
		}],

		endEvents: [],

		hotspots: [],
	},
	{
		id: 7,
		shortname: '',
		projectid: 4,
		sequence: 1,
		parent: 0,
		title: '',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: 27,
			filePath: '/files/28/28f2b729e851c8d693e42554368a9254b6f681ea',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
			id: 700,
			visible: 0,
			overridable: 1,
			actions: [
			{
				actionType: 'audio',
				actionItemId: 9,
				actionFile:
				{
					fileId: 42,
					filePath: '/files/4c/4c81af2fc41f617492b60ee177825bbef0db543c',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionDuration: 2000,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'jump',
				actionDelay: 2000,
				actionItemId: 8,
				actionSequence: 3,
			}],
		}],

		endEvents: [],

		hotspots: [],
	},
	{
		id: 8,
		shortname: '',
		projectid: 4,
		sequence: 1,
		parent: 0,
		title: '',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: 29,
			filePath: '/files/ac/acad92a444351248446a57c52d4cccf557de0ea5',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
			id: 800,
			visible: 0,
			overridable: 1,
			actions: [
			{
				actionType: 'audio',
				actionItemId: 9,
				actionFile:
				{
					fileId: 43,
					filePath: '/files/b8/b81cacd08a5f815ff5226c104fae9b554d5595d1',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionDuration: 2000,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'jump',
				actionDelay: 2000,
				actionItemId: 9,
				actionSequence: 3,
			}],
		}],

		endEvents: [],

		hotspots: [],
	},
	{
		id: 9,
		shortname: '',
		projectid: 4,
		sequence: 1,
		parent: 0,
		title: '',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: 30,
			filePath: '/files/5a/5ada2342d1b4e6ac28c4ec7930406579a14c5f40',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
			id: 900,
			visible: 0,
			overridable: 1,
			actions: [
			{
				actionType: 'audio',
				actionItemId: 9,
				actionFile:
				{
					fileId: 44,
					filePath: '/files/b5/b56c3f13ba8122e7c7cd6b820c093a37c6fcbeba',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionDuration: 2000,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'jump',
				actionDelay: 2000,
				actionItemId: 10,
				actionSequence: 3,
			}],
		}],

		endEvents: [],

		hotspots: [],
	},
	{
		id: 10,
		shortname: '',
		projectid: 4,
		sequence: 1,
		parent: 0,
		title: '',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: 31,
			filePath: '/files/31/31b5cbe1d125befd32292580c523688c9191c4ba',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
			id: 1000,
			visible: 0,
			overridable: 1,
			actions: [
			{
				actionType: 'audio',
				actionItemId: 9,
				actionFile:
				{
					fileId: 45,
					filePath: '/files/83/8343742066bf78508fa004c8cfb3e0d3f9335dd5',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionDuration: 2000,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'jump',
				actionDelay: 2000,
				actionItemId: 11,
				actionSequence: 3,
			}],
		}],

		endEvents: [],

		hotspots: [],
	},
	{
		id: 11,
		shortname: '',
		projectid: 4,
		sequence: 1,
		parent: 0,
		title: '',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: 32,
			filePath: '/files/2a/2a20796d8daed31375ebdca9b5f322a0d6b94afa',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
			id: 1100,
			visible: 0,
			overridable: 1,
			actions: [
			{
				actionType: 'audio',
				actionItemId: 9,
				actionFile:
				{
					fileId: 1,
					filePath: '/files/58/581228ab587bd2816d113ac43dfb06e24c102aeb',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionDuration: 2000,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'jump',
				actionDelay: 2000,
				actionItemId: 12,
				actionSequence: 3,
			}],
		}],

		endEvents: [],

		hotspots: [],
	},
	{
		id: 12,
		shortname: '',
		projectid: 4,
		sequence: 1,
		parent: 0,
		title: '',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: 33,
			filePath: '/files/16/166a85eb7f626e88d792f2328e82950e61000d99',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
			id: 1200,
			visible: 0,
			overridable: 1,
			actions: [
			{
				actionType: 'audio',
				actionItemId: 9,
				actionFile:
				{
					fileId: 47,
					filePath: '/files/1e/1e843b45b63f86287a5644e3041f9be8a219ee88',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionDuration: 2000,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'jump',
				actionDelay: 2000,
				actionItemId: 13,
				actionSequence: 3,
			}],
		}],

		endEvents: [],

		hotspots: [],
	},
	{
		id: 13,
		shortname: '',
		projectid: 4,
		sequence: 1,
		parent: 0,
		title: '',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: 34,
			filePath: '/files/49/49e85fa95c33b1ded2f8f16ed2080f71a555ef35',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
			id: 1300,
			visible: 0,
			overridable: 1,
			actions: [
			{
				actionType: 'audio',
				actionItemId: 9,
				actionFile:
				{
					fileId: 48,
					filePath: '/files/f8/f82c38bbb8b485f9814906cc40dbff9e48987cd3',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionDuration: 2000,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'jump',
				actionDelay: 2000,
				actionItemId: 14,
				actionSequence: 3,
			}],
		}],

		endEvents: [],

		hotspots: [],
	},
	{
		id: 14,
		shortname: '',
		projectid: 4,
		sequence: 1,
		parent: 0,
		title: '',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: 35,
			filePath: '/files/8e/8e3a5ab81b21753efda69b9223e9d58b28d6215a',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
			id: 1400,
			visible: 0,
			overridable: 1,
			actions: [
			{
				actionType: 'audio',
				actionItemId: 9,
				actionFile:
				{
					fileId: 49,
					filePath: '/files/ba/ba231eee7dd995b0f8e1996784bc371eb0376f6d',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionDuration: 2000,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'jump',
				actionDelay: 2000,
				actionItemId: 15,
				actionSequence: 3,
			}],
		}],

		endEvents: [],

		hotspots: [],
	},
	{
		id: 15,
		shortname: '',
		projectid: 4,
		sequence: 1,
		parent: 0,
		title: '',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: 36,
			filePath: '/files/53/53a785b1ae5c5aa423c8469729c227d8c13ee169',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
			id: 1500,
			visible: 0,
			overridable: 1,
			actions: [
			{
				actionType: 'audio',
				actionItemId: 9,
				actionFile:
				{
					fileId: 50,
					filePath: '/files/7d/7d9206500db74cf5e2b02d14e2b01989e574aba7',
					fileType: 'audio',
				},
				actionVolume: 1.0,
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: 'Look down to replay.',
				actionDuration: 0,
				actionSequence: 2,
				actionContinue: true,
			},
			{
				actionType: 'variable',
				actionVariableName: 'finishedStory',
				actionVariableOperator: 'set',
				actionVariableOperand: '1',
				actionSequence: 3,
				actionContinue: false,					// Should always be "false" when setting a variable.
			}],
		}],

		// TODO: The endEvents are not yet implemented.
		endEvents: [
		{
			id: 2500,
			visible: 0,
			overridable: 1,
			actions: [
			{
				actionType: 'variable',
				actionVariableName: 'finishedStory',
				actionVariableOperator: 'set',
				actionVariableOperand: '0',
				actionSequence: 1,
				actionContinue: false,					// Should always be "false" when setting a variable.
			}],
		}],

		hotspots: [
		{
			id: 250,
			positionX: 0,
			positionY: -1,
			positionZ: 0,
			radius: 0.2,
			duration: 2000,								// Milliseconds
			visible: 1,
			overridable: 0,
			toggleOtherHotspots: true,					// If "true" then this hotspot is exclusive when activated. No other hotsopts in the scene can be activated until this hotspot is activated again.
			conditions: [
			{
				conditionType: 'variable',
				conditionVariableName: 'finishedStory',	// This hotspot activates when the last audio is finished.
				conditionVariableOperator: '=',
				conditionVariableOperand: '1',
			}],

			actions: [
			{
				actionType: 'jump',
				actionItemId: 2,
				actionSequence: 1
			}],
		}],
	}],
};
