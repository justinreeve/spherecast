<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConditionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conditions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('eventId');
			$table->string('type', 16);					// Can be just variable currently.

			// TODO: We may want to normalize these so they're in other tables.
			$table->string('variableName');
			$table->string('variableOperator', 8);
			$table->string('variableOperand');

			// Indexes.
			$table->index('eventId');
			$table->index('type');
			$table->index('variableName');

			// Foreign keys.
			$table->foreign('eventId')
				->references('id')->on('sceneevents')
				->onDelete('cascade')
				->onUpdate('cascade');

			// created_at, updated_at DATETIME
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('conditions', function(Blueprint $table)
		{
		});
	}

}
