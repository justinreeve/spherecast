@include('docs.docsheader')
<h1>Global Variables</h1>

<div class="attributes">
	<dl>
		<dt class="integer">id</dt>
		<dd>variable id</dd>

		<dt class="integer"><a href="/api/docs/project">projectId</a></dt>
		<dd>project id the variable is associated with</dd>

		<dt class="string">variableName</dt>
		<dd>
			the name of the variable; this cannot be any of the following reserved variables that are prefixed with "SC":
			<dl class="options">
				<dt class="string">SCProjectLanguage</dt>
				<dd>two-digit country code specifying the currently selected language for the project</dd>

				<dt class="boolean">SCShowCrosshairs</dt>
				<dd>whether crosshairs should be displayed in the viewer or not</dd>

				<dt class="integer">SCTimeElapsed</dt>
				<dd>the number of seconds the user has currently spent in the project</dd>
<!--
				<dt class="boolean">SCUseNorth</dt>
				<dd>the current setting of the <a href="#">useNorth</a> attribute for a scene; this changes with each scene load, but can be manually set</dd>
-->
				<dt class="string">SCViewerMode</dt>
				<dd>indicates which mode (standard or VR) the user currently has enabled, so interactions can be different depending on the mode; for example, an overlay appearing in the corner in standard mode, but popping up in the center in VR mode</dd>
			</dl>
		</dd>

		<dt class="string">defaultValue</dt>
		<dd>the initial value of the variable when the project first loads</dd>
	</dl>
</div>

<div class="example">
	<pre>
{
	"id": "1",
	"projectId": "1",
	"name": "showMenu",
	"defaultValue": "1",
	"created_at": "-0001-11-30 00:00:00",
	"updated_at": "-0001-11-30 00:00:00"
}
	</pre>
</div>
@include('docs.docsfooter')