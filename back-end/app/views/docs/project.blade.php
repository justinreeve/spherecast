@include('docs.docsheader')
<!--
	Use https://stripe.com/docs/api for primary layout - see https://github.com/tripit/slate and instead of programming languages on the right, use "Example 1", "Example 2", "Example 3", etc.
	Also see http://browsenpm.org/package.json
	Elixir lets you write docs and tests in the comments http://elixir-lang.org/getting-started/mix-otp/docs-tests-and-pipelines.html
-->
<h1>Project</h1>

<p class="description">This document describes the JSON structure for Spherecast project files. Examples are shown on the right.</p>

<div class="attributes">
	<dl>
		<dt class="integer">id</dt>
		<dd>project id</dd>

		<dt class="string">shortname</dt>
		<dd>unique code identifying the project</dd>

		<dt class="string">title</dt>
		<dd>title of project</dd>

		<dt class="string">subtitle</dt>
		<dd>subtitle of project</dd>

		<dt class="string">description</dt>
		<dd>text summary of project</dd>

		<dt class="integer">createdBy</dt>
		<dd>user id of project creator</dd>

		<dt class="integer">startScene</dt>
		<dd>scene id to begin project at</dd>

		<dt class="string" data-default="auto">defaultViewerMode</dt>
		<dd>
			the viewer mode in which the project is launched
			<dl class="options">
				<dt>auto</dt>
				<dd>auto-detect based on device; if DeviceOrientation is present, assume VR mode</dd>

				<dt>standard</dt>
				<dd>panoramic (mono) mode </dd>

				<dt>vr</dt>
				<dd>virtual reality (stereo) mode</dd>
			</dl>
		</dd>

		<dt class="boolean" data-default="true">allowViewerModeStandard</dt>
		<dd>allow standard mode to be selected as an option</dd>

		<dt class="boolean" data-default="true">allowViewerModeVR</dt>
		<dd>allow VR mode to be selected as an option</dd>

		<dt class="boolean" data-default="true">enableSaveSession</dt>
		<dd>allow this project to save its session, so progress can be resumed later</dd>

		<dt class="boolean" data-default="false">requireLogin</dt>
		<dd>force users to log in before viewing this project</dd>

		<dt class="string" data-default="en-en">defaultLanguage</dt>
		<dd>the default language to use for this project; this is used primarily to determine the language to use with the speech recognition API</dd>

		<dt class="integer" data-default="+0">adjustFontSize</dt>
		<dd>a positive or negative relative value for globally increasing or decreasing all text in the project</dd>

		<dt class="boolean" data-default="true">showCrosshairs</dt>
		<dd>show crosshairs during the project; this can be turned on or off during the project by by setting the "SCShowCrosshairs" global variable to true or false, respectively, via a variable <a href="/api/docs/actions">action</a></dd>

		<dt class="integer" data-default="null">introMedia</dt>
		<dd>a "splash screen" to show before the project loads on VR-enabled devices</dd>

		<dt class="array"><a href="/api/docs/globalvariables">globalVariables</a></dt>
		<dd>a list of all the global variables that can be set and changed in the project</dd>

		<dt class="array"><a href="/api/docs/jumpTargets">jumpTargets</a></dt>
		<dd>a list of all the scenes, with arrays of all possible scenes that can be jumped; used for optimizing the preloading of assets for next scenes for a seamless experience</dd>

		<dt class="array"><a href="/api/docs/assets">assets</a></dt>
		<dd>a list of all the media assets in the project, with priority levels to indicate which should load first, and file information</dd>

		<dt class="array"><a href="/api/docs/scenes">scenes</a></dt>
		<dd>a list of all the scenes in the project, with associated events</dd>
	</dl>
</div>

<div class="example">
<pre>{
	"id": "7",
	"shortname": "grafika",
	"title": "Grafika Gracer",
	"subtitle": "",
	"description": "Grafika Gracer",
	"createdBy": "0",
	"startScene": "44",
	"defaultViewerMode": "auto",
	"allowViewerModeStandard": "1",
	"allowViewerModeVR": "1",
	"enableSaveSession": "1",
	"requireLogin": "0",
	"defaultLanguage": "en-en",
	"adjustFontSize": "+0",
	"created_at": "2015-04-10 22:30:20",
	"updated_at": "2015-04-10 22:30:20",
	"showCrosshairs": "1",
	"introMedia": {...},
	"globalVariables": [{...}],
	"jumpTargets": {
		"1": [2, 10],
		"2": [12, 15, 18]
	},
	"assets": [{...}],
	"scenes": [{...}],
}</pre>
</div>
@include('docs.docsfooter')