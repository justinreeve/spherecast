<?php
class Log extends Eloquent
{
	protected $id;
	protected $url;
	protected $message;
	protected $type;
	protected $stackTrace;
	protected $cause;

	protected $guarded = array('id');
}
?>