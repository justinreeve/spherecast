<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('files', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('contenthash', 64);
			$table->string('filepath');
			$table->string('title');
			$table->string('filename');
			$table->string('mimetype', 64);
			$table->integer('width')->nullable();
			$table->integer('height')->nullable();
			$table->integer('length')->nullable();
			$table->integer('filesize')->default(0);
			$table->string('extension', 16);

			// Indexes.
			$table->unique(array('contenthash', 'filesize'));
			$table->index('contenthash');
			$table->index('mimetype');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('files', function(Blueprint $table)
		{
		});
	}

}
