<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('labels', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('eventId');
			$table->string('type');				// Can be text or image.
			$table->decimal('positionX', 5, 3)->default(0.0);
			$table->decimal('positionY', 5, 3)->default(0.0);
			$table->decimal('positionZ', 5, 3)->default(0.0);
			$table->decimal('scaleX', 3, 2)->default(1.0);
			$table->decimal('scaleY', 3, 2)->default(1.0);

			// Serialized parameters. Can be labelText, fontSize, fontStyle
			$table->text('params')->nullable();

			// Indexes.
			$table->index('eventId');
			$table->index('type');

			// Foreign keys.
			$table->foreign('eventId')
				->references('id')->on('sceneevents')
				->onDelete('cascade')
				->onUpdate('cascade');

			// created_at, updated_at DATETIME
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('labels', function(Blueprint $table)
		{
		});
	}

}
