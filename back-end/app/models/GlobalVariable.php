<?php
class GlobalVariable extends Eloquent
{
	protected $id;
	protected $projectId;
	protected $name;
	protected $defaultValue;

	protected $guarded = array('id');
	protected $table = 'variables';

	public function project()
	{
		return $this->belongsTo('Project', 'projectId', 'id');
	}
/*
	public function getVariableType()
	{
		if (isset($this->value))
		{
			if (is_numeric($this->value))
				return 'numeric';
			else if (is_string($this->value))
				return 'string';
			else if (is_numeric($this->defaultValue))
				return 'numeric';
			else if (is_string($this->defaultValue))
				return 'string';
		}
		return null;
	}
*/
}
?>