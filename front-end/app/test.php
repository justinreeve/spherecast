<?php
/*
				videoTransparencyColorRGB: '0xd400',
				videoHeight: 540,
				videoWidth: 960,
				videoScaleX: 0.2,
				videoScaleY: 0.2,
				videoVolume: 1.0,
				videoInTransition: 'fadeIn',
				videoInTransitionDuration: 1000,
				videoOutTransitionDuration: 1000,
				actionDuration: 1900,
				actionRepeat: false,
				actionSequence: 2,
				actionContinue: false,
*/

/*
$obj = new stdClass;
$obj->duration = 0;
$obj->repeat = true;
$obj->volume = 0.5;
$obj->transparencyColor = '0xd400';
$obj->height = 540;
$obj->width = 960;
$obj->scaleX = 0.2;
$obj->scaleY = 0.2;
$obj->positionX = 0.899;
$obj->positionY = -0.431;
$obj->positionZ = 0.081;
$obj->inTransition = 'fadeIn';
$obj->inTransitionDuration = 1000;
$obj->outTransition = 'fadeOut';
$obj->outTransitionDuration = 1000;
*/

/*
$obj->placement = 'overlay';
$obj->positionX = 0;
$obj->positionY = -100;
$obj->positionZ = 0;
$obj->alignment = 'bottom center';
$obj->scaleX = 0.5;
$obj->scaleY = 0.5;
$obj->duration = 0;
*/

/*
$obj->duration = 2000;
*/

/*
$obj->text = '           3           ';
$obj->fontSize = 26;
$obj->continue = false;
// $obj->fontStyle = 'italic';
$obj->duration = 1500;
*/

/*
// $obj->text = 'Backpack (Rucksack)';
// $obj->fontSize = 18;
// $obj->fontStyle = 'italic';
$obj->disappearOnCountdown = false;
$obj->opacity = 0.5;
*/


/*
$obj->targetId = 212;	// This is the action id for a fixed image, video, or spritemap.

// Specify the position to move to. These can be left blank if they should remain at the current position of the
// object being moved.
$obj->positionX = 97.938;
$obj->positionY = -14.703;
$obj->positionZ = 8.663;

// The translateDuration indicates how many milliseconds it should take to reach the destination.
$obj->translateDuration = 5000;

// If translateDestinationX/Y is specified, the animation will gradually change the object from its current scale to the one
// specified, when it reaches the end (translateDuration);
$obj->translateScaleX = 0.5;
$obj->translateScaleY = 0.5;

// If any of the spritemap (or other) parameters are specified here, they will overwrite the default settings,
// and the sprite will start animating with the new settings. Otherwise, the defaults will be used.
// We can also specify "revert" for any setting if we want it to revert to the original settings.
/*
$obj->spritemapDuration = 1500;
$obj->spritemapRepeat = true;
$obj->spritemapTileSequence = array(
	12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
);
*/


/*
$obj->targetId = 212;	// This is the action id for a fixed image, video, or spritemap.

// Specify the position to move to. These can be left blank if they should remain at the current position of the
// object being moved.
// $obj->positionX = 97.938;
// $obj->positionY = -14.703;
// $obj->positionZ = 8.663;

// The translateDuration indicates how many milliseconds it should take to reach the destination.
$obj->translateDuration = 1000;

// If translateDestinationX/Y is specified, the animation will gradually change the object from its current scale to the one
// specified, when it reaches the end (translateDuration);
// $obj->translateScaleX = 0.5;
// $obj->translateScaleY = 0.5;

// If any of the spritemap (or other) parameters are specified here, they will overwrite the default settings,
// and the sprite will start animating with the new settings. Otherwise, the defaults will be used.
// We can also specify "revert" for any setting if we want it to revert to the original settings.
$obj->spritemapDuration = 1000;
$obj->spritemapRepeat = true;
$obj->spritemapTileSequence = array(
	15, 15,
);
*/

// $obj->animateMode = 'onfocus';
// $obj->countdownMediaLabelReplace = 165;
// $obj->placement = 'fixed';
// $obj->positionX = 13.401;
// $obj->positionY = -36.403;
// $obj->positionZ = -91.735;
$obj->spritemapTilesNum = 12;
$obj->spritemapTilesHorizontal = 12;
$obj->spritemapTilesVertical = 1;
$obj->spritemapHeight = 64;
$obj->spritemapWidth = 768;
$obj->spritemapDuration = 2500;
$obj->spritemapRepeat = false;
$obj->spritemapShowFirstAsFixed = true;
$obj->spritemapTileSequence = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);

/*	
$obj->translate = array();
	$translate = new stdClass;
	// Specify the position to move to. These can be left blank if they should remain at the current position (which for the
	// first sequence in the array will be the starting position).
	$translate->positionX = 55.570;
	$translate->positionY = 24.554;
	$translate->positionZ = -79.036;

	// The duration indicates how many milliseconds it should take to reach the destination.
	$translate->duration = 1000;

	// If any of the spritemap parameter are specified here, they will overwrite the default settings, and the
	// sprite will start animating with the new settings. Otherwise, the defaults will be used.
	// We can also specify "revert" for any setting if we want it to revert to the original settings.
	$translate->spritemapDuration = 1500;
	$translate->spritemapRepeat = true;
	$translate->spritemapTileSequence = array(
		87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110,
	);
$obj->translate[] = $translate;

$obj->spritemapTileSequence = array(
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	145, 146, 145, 146, 145, 146, 145, 146, 145, 146, 145, 146,
	147, 147, 148, 148, 149, 149, 150, 150, 151, 151, 152, 152, 153, 153,
	148, 148, 149, 149, 150, 150, 151, 151, 152, 152, 153, 153,
	145, 146, 145, 146,
	155, 155, 156, 156, 157, 157, 158, 158, 159, 159, 160, 160, 161, 161,
	156, 156, 157, 157, 158, 158, 159, 159, 160, 160, 161, 161,
	145, 146, 145, 146, 145, 146, 145, 146, 145, 146, 145, 146,
	145, 146, 145, 146, 145, 146, 145, 146, 145, 146, 145, 146,
	147, 147, 148, 148, 149, 149, 150, 150, 151, 151, 152, 152, 153, 153,
	148, 148, 149, 149, 150, 150, 151, 151, 152, 152, 153, 153,
	145, 146, 145, 146,
	155, 155, 156, 156, 157, 157, 158, 158, 159, 159, 160, 160, 161, 161,
	156, 156, 157, 157, 158, 158, 159, 159, 160, 160, 161, 161,
	145, 146, 145, 146, 145, 146, 145, 146, 145, 146, 145, 146,
	0, 0, 0, 0, 0, 0, 0, 0,
	1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28,
	0, 0, 0, 0, 0, 0, 0, 0,
	87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110,
	87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110,
	29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52,
	29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52,
	0, 0, 0, 0, 0, 0, 0, 0,
	116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139,
	116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139,
	58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81,
	58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81,
);
*/
 
/*
$obj->placement = 'fixed';
$obj->positionX = 67.679;
$obj->positionY = -51.796;
$obj->positionZ = 51.504;
$obj->spritemapTilesNum = 40;
$obj->spritemapTilesHorizontal = 8;
$obj->spritemapTilesVertical = 5;
$obj->spritemapHeight = 960;
$obj->spritemapWidth = 600;
$obj->spritemapDuration = 7000;
$obj->spritemapRepeat = true;
$obj->spritemapShowFirstAsFixed = true;
$obj->spritemapTileSequence = array(
	8, 9, 10, 11, 12, 13, 14, 15,
	8, 9, 10, 11, 12, 13, 14, 15,
	8, 9, 10, 11, 12, 13, 14, 15,
	8, 9, 10, 11, 12, 13, 14, 15,
	8, 9, 10, 11, 12, 13, 14, 15,
);
*/

/*
$obj->orientationX = -0.703;
$obj->orientationY = -0.313;
$obj->orientationZ = -0.639;
*/

/*
$obj = new stdClass;
// $obj->northX = -57.522;
// $obj->northY = 0.000;
// $obj->northZ = 81.534;
$obj->northRotationOffset = 90;
*/

// echo '<meta charset="utf-8">';

/*
$obj = new stdClass;
$obj->duration = 0;
$obj->repeat = 1;
$obj->volume = 0.15;
$obj->persist = 1;
*/
/*
$obj->name = 'scene1HotspotDoor';
$obj->operator = '+';
$obj->operand = '1';
*/
echo serialize($obj);
?>