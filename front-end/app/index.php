<?php
// Retrieve the Open Graph meta tag content. Extract the project shortname/id from the URL.
$path = $_SERVER['REQUEST_URI'];
$path_parts = explode('/', $path);
$project_shortname = '';
foreach ($path_parts as $part)
{
	if ($part != '')
	{
		$project_shortname = $part;
		break;
	}
}

if ($project_shortname != '')
{
	if (isset($_SERVER['HTTPS']))
		$http_prefix = 'https://';
	else
		$http_prefix = 'http://';
	$url = $http_prefix . $_SERVER['SERVER_NAME'] . '/api/project/' . $project_shortname . '/metadata';
	$project_json = file_get_contents($url);
}

if (isset($project_json))
{
	$project = json_decode($project_json);
	if ($project)
	{
		$og_title = isset($project->title) ? str_replace('"', '', $project->title) : '';
		$og_image = isset($project->imageurl) ? $project->imageurl . '.jpg' : '';
		$og_description = isset($project->description) ? str_replace('"', '', $project->description) : '';
	}
}
?><!DOCTYPE html>
<html lang="en" xmlns:fb="http://ogp.me/ns/fb#" data-ng-app="spherecastApp">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <meta property="og:site_name" content="Spherecast" />
<?php
		if (isset($og_title))
			echo '<meta property="og:title" content="' . $og_title . '" />';
		if (isset($og_image))
			echo '<meta property="og:image" content="' . $og_image . '" />';
		if (isset($og_description))
			echo '<meta property="og:description" content="' . $og_description . '" />';
?>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

	    <base href="/" />
	    <base target="_parent" />

	    <title>Spherecast</title>

	    <link rel="icon" href="favicon.ico">
	    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
	    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<!--
	    <link href="assets/css/gridder.css" rel="stylesheet">
-->
	    <link href="assets/css/editor.css" rel="stylesheet">
	    <link href="assets/css/editor-responsive.css" rel="stylesheet">
	    <link href="assets/css/spherecast.css" rel="stylesheet">
	    <link href="assets/css/viewer.css" rel="stylesheet">
	    <link href="assets/css/viewer-options.css" rel="stylesheet">
	    <link href="assets/css/override.css" rel="stylesheet">

		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.8/slick.css"/>
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.8/slick-theme.css"/>

		<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>
<!--
		<script src="assets/js/modernizr.min.js"></script>
-->
		<script src="assets/js/Detectizr.min.js"></script>
<!--
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular-route.min.js"></script>
-->
		<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.15/angular.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.15/angular-route.min.js"></script>
		<script src="//code.createjs.com/preloadjs-0.6.0.min.js"></script>
<!--
		<script src="assets/js/angular-ui-router.min.js"></script>
-->
	    <script src="app.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
<!--
		<div ng-include src="'partials/nav.html'"></div>
-->
		<!-- Make sure the Open Sans font can load in the viewer -->
		<div class="font-preload" style="font-family: 'Open Sans'; visibility: hidden; opacity: 0.0; position: absolute; top: -9999px; left: -9999px; width: 0px; height: 0px; overflow: hidden;">.</div>

		<div id="page" data-ng-view></div>
	    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!--
	    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.min.js"></script>
	    <script src="assets/js/gridder.js"></script>
		<script src="assets/js/tween.min.js"></script>
-->
        <script src="assets/js/bootstrap.min.js"></script>
<!--
		<script src="assets/js/webgl-debug.js"></script>
-->
		<script src="assets/js/howler.min.js"></script>
		<script src="assets/js/three.r69.min.js"></script>
		<script src="assets/js/StereoEffect.js"></script>
		<script src="assets/js/DeviceOrientationControlsVR.js"></script>
		<script src="assets/js/OrbitControls.js"></script>
<?php
		// The ENVIRONMENT variable is set in the Apache spherecast.conf configuration for the production site.
		if (isset($_SERVER['ENVIRONMENT']) && $_SERVER['ENVIRONMENT'] == 'production')
		{
			$filename = $_SERVER['DOCUMENT_ROOT'] . '/assets/js/TextUtils.min.js';
			if (file_exists($filename))
			{
				echo '
					<script src="assets/js/TextUtils.min.js"></script>
				';
			}
			else
			{
				echo '
					<script src="assets/js/TextUtils.js"></script>
				';
			}

			$filename = $_SERVER['DOCUMENT_ROOT'] . '/assets/js/viewer.min.js';
			if (file_exists($filename))
			{
				echo '
					<script src="assets/js/viewer.min.js"></script>
				';
			}
			else
			{
				echo '
					<script src="assets/js/ngWebgl.js"></script>
				';
			}

		}
		else
		{
			echo '
				<script src="assets/js/TextUtils.js"></script>
				<script src="assets/js/ngWebgl.js"></script>
			';
		}
?>
		<script type="text/javascript" src="assets/js/angular-file-upload.js"></script>
		<script type="text/javascript" src="assets/js/detect-mobile.js"></script>
		<script type="text/javascript" src="controllers/ProjectController.js"></script>
		<script type="text/javascript" src="controllers/SceneController.js"></script>
		<script type="text/javascript" src="controllers/MediaController.js"></script>
		<script type="text/javascript" src="services/projects-service.js"></script>
		<script type="text/javascript" src="services/scenes-service.js"></script>
		<script type="text/javascript" src="services/media-service.js"></script>
		<script type="text/javascript" src="assets/js/spherecast-editor.js"></script>
<!--
		<script src="//cdnjs.cloudflare.com/ajax/libs/annyang/1.5.0/annyang.min.js"></script>
-->
<!--
		<script id="vertexShader" type="x-shader/x-vertex">
			varying vec2 vUv;
			void main()
			{
				vUv = uv;
				vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );
				gl_Position = projectionMatrix * mvPosition;
			}
		</script>
-->
	    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	    <!-- <script src="assets/js/ie10-viewport-bug-workaround.js"></script> -->

		<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.8/slick.min.js"></script>
<?php
		if (isset($_SERVER['ENVIRONMENT']) && $_SERVER['ENVIRONMENT'] == 'production')
		{
?>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			
			ga('create', 'UA-19755801-14', 'auto');
			ga('send', 'pageview');
		</script>
<?php
		}
?>
	</body>
</html>
