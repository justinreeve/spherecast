<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSceneEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sceneevents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('shortname');
			$table->integer('sceneId');
			$table->enum('type', array('start', 'end', 'hotspot', 'voice'))->default('start');
			$table->boolean('overridable')->default(false);				// Indicates whether the actions can be interrupted by other events.

			// Indexes.
			$table->unique('shortname');
			$table->index('sceneId');
			$table->index('type');

			// Foreign keys.
			$table->foreign('sceneId')
				->references('id')->on('scenes')
				->onDelete('cascade')
				->onUpdate('cascade');

			// created_at, updated_at DATETIME
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sceneevents', function(Blueprint $table)
		{
			//
		});
	}

}
