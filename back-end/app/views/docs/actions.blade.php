@include('docs.docsheader')
<h1>Actions</h1>

<div class="attributes">
	<dl>
		<dt class="integer">id</dt>
		<dd>action id</dd>

		<dt class="integer"><a href="/api/docs/events">eventId</a></dt>
		<dd>event id the action is associated with</dd>

		<dt class="string">type</dt>
		<dd>the type of action; different actions may require different attributes to be present, and these are listed separately <a href="#">below</a></dd>

		<dt class="integer">sequence</dt>
		<dd>the order the action executes in the event</dd>

		<dt class="boolean" data-default="true">continue</dt>
		<dd>if false, this action must complete before the next action in the sequence executes; if true, the next action will execute immediately after this action executes (this is used in situations where two or more actions must occur, such as a character speaking with subtitles)</dd>

		<dt class="integer" data-default="null">mediaId</dt>
		<dd>an id for a media object defined at the <a href="/api/docs/project">project</a> level, in the <a href="/api/docs/assets">assets</a>; not all action types require media</dd>
<!--
		<dt class="object" data-default="null">media</dt>
		<dd>a media object associated with this action; not all action types require a media object</dd>
-->
	</dl>
</div>

<div class="example">
	<pre>
{
	"id": "39",
	"eventId": "16",
	"type": "variable",
	"sequence": "1",
	"continue": "0",
	"mediaId": null,
	"created_at": "-0001-11-30 00:00:00",
	"updated_at": "-0001-11-30 00:00:00",
	"name": "listenedToIntroduction",
	"operator": "=",
	"operand": "0"
}
	</pre>
</div>

<h2>Action type: "audio"</h2>
<p class="description">Play an audio (MP3) file</p>
<div class="attributes subtype">
	<dl>
		<dt class="decimal" data-default="1.0">volume</dt>
		<dd>a volume range from 0.0 to 1.0</dd>

		<dt class="boolean" data-default="false">repeat</dt>
		<dd>if true, play the audio file on an infinite loop once it reaches the end, until a <a href="#">jump</a> or <a href="#">cancel</a> action stops the file</dd>

		<dt class="boolean" data-default="false">persist</dt>
		<dd>if true, do not stop playing the audio file after a <a href="#">jump</a> action; "audio" actions are the only action types that can persist across scenes</dd>
	</dl>
</div>

<h2>Action type: "jump"</h2>
<p class="description">Load another scene, ending all actions in the current scene (except persistent audio)</p>
<div class="attributes subtype">
	<dl>
		<dt class="integer">targetId</dt>
		<dd>the scene id to jump to</dd>
	</dl>
</div>

<h2>Action type: "video"</h2>
<p class="description">Play a video (MP4) file - <em>currently under revision</em></p>

<h2>Action type: "image"</h2>
<p class="description">Display an image (JPEG or PNG) file</em></p>
<div class="attributes subtype">
	<dl>
		<dt class="string">
			placement
		</dt>
		<dd>
			the method for showing the image
			<dl class="options">
				<dt>overlay</dt>
				<dd>show the image as a popup (heads-up display)</dd>

				<dt>fixed</dt>
				<dd>show the image at a fixed XYZ coordinate on the circumference of the scene's photosphere</dd>
			</dl>
		</dt>

		<dt class="decimal">positionX</dt>
		<dd>(fixed placement only) the X coordinate for the image on the circumference of the sphere</dd>

		<dt class="decimal">positionY</dt>
		<dd>(fixed placement only) the Y coordinate for the image on the circumference of the sphere</dd>

		<dt class="decimal">positionZ</dt>
		<dd>(fixed placement only) the Z coordinate for the image on the circumference of the sphere</dd>

		<dt class="decimal">alignment</dt>
		<dd>
			(overlay placement only) a positioning for the image when it pops up
			<table border="1">
				<tr>
					<td>top left</td>
					<td>top center</td>
					<td>top right</td>
				</tr>
				<tr>
					<td>center left</td>
					<td>center center</td>
					<td>center right</td>
				</tr>
				<tr>
					<td>bottom left</td>
					<td>bottom center</td>
					<td>bottom right</td>
				</tr>
			</table>
		</dd>

		<dt class="decimal" data-default="1.0">scaleX</dt>
		<dd>multiplicator to horizontally stretch the image</dd>

		<dt class="decimal" data-default="1.0">scaleY</dt>
		<dd>multiplicator to vertically stretch the image</dd>

		<dt class="integer">duration</dt>
		<dd>the number of milliseconds the image displays before disappearing; if "0" the image displays indefinitely, until a <a href="#">jump</a> or <a href="#">cancel</a> action removes it</dd>
	</dl>
</div>

<h2>Action type: "spritemap"</h2>
<p class="description">
	Display and animate a spritemap (JPEG or PNG) file <em>(PNG recommended)</em>.
	Spritemap attributes follow a standard format regardless of whether they occur in an action, hotspot, or other data
	structure in the Spherecast JSON specification.
	<br /><br />
	<a href="/api/docs/spritemaps">Click here for more details</a>.
</p>

<h2>Action type: "text"</h2>
<p class="description">Display a text string</p>
<div class="attributes subtype">
	<dl>
		<dt class="string">text</dt>
		<dd>the text to display (256-character limit)</dd>

		<dt class="string">placement</dt>
		<dd>
			the method for showing the text (<em>unused, currently defaults to "overlay"; for fixed text, hotspot labels can be used instead</em>)
			<dl class="options">
				<dt>overlay</dt>
				<dd>show the text as a popup (heads-up display)</dd>

				<dt>fixed</dt>
				<dd>show the text at a fixed XYZ coordinate on the circumference of the scene's photosphere</dd>
			</dl>
		</dd>

		<dt class="string">textPosition</dt>
		<dd>
			(overlay placement only) a positioning for the text when it pops up
			<table border="1">
				<tr>
					<td>top left</td>
					<td>top center</td>
					<td>top right</td>
				</tr>
				<tr>
					<td>center left</td>
					<td>center center</td>
					<td>center right</td>
				</tr>
				<tr>
					<td>bottom left</td>
					<td>bottom center</td>
					<td>bottom right</td>
				</tr>
			</table>
		</dd>

		<dt class="integer" data-default="24">textFontSize</dt>
		<dd>size for the font, defined in pixel height</dd>

		<dt class="string" data-default="white">textFontColor</dt>
		<dd>color for the font</dd>

		<dt class="string" data-default="">textFontStyle</dt>
		<dd>style for the font (leave blank, or "italic")</dd>

		<dt class="integer">duration</dt>
		<dd>the number of milliseconds the text displays before disappearing; if "0" the text displays indefinitely, until a <a href="#">jump</a> or <a href="#">cancel</a> action removes it</dd>

		<dt class="object">inTransition <em>(under revision)</em></dt>
		<dd>the starting transition method for displaying the text (e.g. "fade in")</dd>

		<dt class="object">outTransition <em>(under revision)</em></dt>
		<dd>the ending transition method for dismissing the text (e.g. "fade out")</dd>
	</dl>
</div>

<h2>Action type: "wait"</h2>
<p class="description">Waits a specified amount of time before proceeding with the next action</p>
<div class="attributes subtype">
	<dl>
		<dt class="integer">duration</dt>
		<dd>the number of milliseconds to wait; if 0 or less, the action is bypassed</dd>
	</dl>
</div>

<h2>Action type: "variable"</h2>
<p class="description">Set or modify a <a href="/api/docs/globalvariables">global variable</a></p>
<div class="attributes subtype">
	<dl>
		<dt class="string">name</dt>
		<dd>
			a global variable that has been defined at the project level; this cannot be any of the reserved variables that are prefixed with "SC", with the following exceptions:
			<dl class="options">
				<dt class="string">SCProjectLanguage</dt>
				<dd>two-digit country code specifying the currently selected language for the project</dd>

				<dt>SCShowCrosshairs</dt>
				<dd>true = display crosshairs in viewer, false = hide crosshairs in viewer</dd>
			</dl>
		</dd>

		<dt class="string">operator</dt>
		<dd>
			one of six data manipulation operators to perform on the variable:
			<dl class="options">
				<dt>set</dt>
				<dd>assign the variable to the value specified in operand</dd>

				<dt>+</dt>
				<dd>add the numeric variable by the value specified in operand; if the operand is non-numeric, this does nothing</dd>

				<dt>-</dt>
				<dd>subtract the numeric variable by the value specified in operand; if the operand is non-numeric, this does nothing</dd>

				<dt>*</dt>
				<dd>multiply the numeric variable by the value specified in operand; if the operand is non-numeric, this does nothing</dd>

				<dt>/</dt>
				<dd>divide the numeric variable by the value specified in operand; if the operand is non-numeric, this does nothing</dd>

				<dt>%</dt>
				<dd>modulo the numeric variable by the value specified in operand; if the operand is non-numeric, this does nothing</dd>
			</dl>
		</dd>

		<dt class="string">operand</dt>
		<dd>a number (integer or decimal), or a string, for which the variable is manipulated according to the operator</dd>
	</dl>
</div>

<h2>Action type: "translate"</h2>
<p class="description">
	A translation is the movement or alteration of the object in an image, video, or spritemap action, or a hotspot label.
	Attributes can be overridden as well. A "targetId" must be specified, or this action will be bypassed. Also, the original
	action must still be running, or this action will be bypassed.
	<br /><br />
	<em>(under revision)</em>
</p>
<div class="attributes subtype">
	<dl>
		<dt class="integer">targetId</dt>
		<dd>
			an action id of a currently-running action; actions do not necessarily need
			to be from the same event sequence in a scene
		</dd>
	</dl>
</div>

<h2>Action type: "cancel"</h2>
<p class="description">
	Cancel an action
</p>
<div class="attributes subtype">
	<dl>
		<dt class="integer">targetId</dt>
		<dd>
			an action id of a currently-running action; actions do not necessarily need
			to be from the same event sequence in a scene
		</dd>
	</dl>
</div>
@include('docs.docsfooter')