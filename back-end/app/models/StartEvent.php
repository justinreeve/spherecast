<?php
class StartEvent extends Eloquent
{
	protected $id;
	protected $shortname;
	protected $sceneId;
	protected $type;

//	protected $imageFileId;				// A file object that's used instead of the standard hotspot indicator. This is particularly used each time a scene has a "bottom nav" hotspot.
//	protected $allowMouseClick;
//	protected $allowMouseDoubleClick;
//	protected $allowLook;				// Allow triggering actions by looking at them.
//	protected $lookDuration;
//	protected $lookVisible;				// Only used when $type is "hotspot," and if false, then it's an invisible hotspot (no indication it's being viewed)
//	protected $allowMagnetPull;			// Device app only; won't work on the web.
//	protected $allowBluetoothAdvance;	// Device app only; won't work on the web.

	protected $guarded = array('id');
	protected $table = 'sceneevents';

	public function scene()
	{
		return $this->belongsTo('Scene', 'sceneId', 'id');
	}

	public function hotspots()
	{
		return $this->hasMany('Hotspot', 'eventId', 'id');
	}

	public function actions()
	{
		return $this->hasMany('Action', 'eventId', 'id');
	}

	public function conditions()
	{
		return $this->hasMany('Condition', 'eventId', 'id');
	}
}
?>