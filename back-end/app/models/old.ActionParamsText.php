<?php
class ActionParamsText extends Eloquent
{
	protected $type;					// This can be "overlay" or "position."
	protected $positionX;				// Only used when $type is "position."
	protected $positionY;				// Only used when $type is "position."
	protected $positionZ;				// Only used when $type is "position."
	protected $duration;				// If "0" then stays up until next scene load.

	function getText()
	{
		// Truncate to 128 characters.
		return substr($this->text, 0, 128);
	}
}
?>