<?php
class MediaFile extends Eloquent
{
	protected $mediaId;
	protected $fileId;
	protected $filename;
	protected $relativeSize;
	protected $sequence;
	protected $params;

	protected $fillable = array('mediaId', 'fileId', 'filename', 'relativeSize', 'sequence');
	protected $table = 'media_files';

	public function media()
	{
		$this->belongsTo('Media', 'mediaId', 'id');
	}
}
?>