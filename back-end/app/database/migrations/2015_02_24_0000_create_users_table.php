<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');

			/**
			 * If a user doesn't have an account, a record is still created but with a null username,
			 * until they link it to an actual account. They can access projects by using the session
			 * id in the URL until they do this, but it's not essential. For a user with a username,
			 * this field should contain the most recent session id in use.
			 */
			$table->string('sessionId');

			$table->string('username')->nullable();
			$table->string('email')->nullable();
			$table->string('firstName')->nullable();
			$table->string('lastName')->nullable();
			$table->string('ip')->nullable();

			// Indexes.
			$table->unique('sessionId');
			$table->index('username');

			// created_at, updated_at DATETIME
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
		});
	}
}