<?php
class Embed extends Eloquent
{
	protected $id;
	protected $type;				// Options: google-street-view, bing-streetside.
	protected $sourceid;			// Example: pano id for Google Street View.
	protected $userid;
	protected $url;					// The URL the user entered to receive this embed.
	protected $timecreated;
	protected $timemodified;
	protected $params;				// This object will be different, depending on $type.
}
?>