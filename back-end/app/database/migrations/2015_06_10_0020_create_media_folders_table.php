<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaFoldersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media_folders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('projectId');
			$table->string('name');
			$table->integer('parent')->nullable();

			// Indexes.
			$table->index('projectId');

			// created_at, updated_at DATETIME
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('media_folders', function(Blueprint $table)
		{
		});
	}
}
