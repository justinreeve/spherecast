spherecastApp.factory('Media', function($http)
{
	return 	{
		// Get all the files.
		get: function() {
			return $http.get('/api/media');
		},

		// Save a file.
		save: function(mediaData) {
			return $http({
				method: 'POST',
				url: '/api/media',
				headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
				data: $.param(mediaData)
			});
		},

		// Destroy a file.
		destroy: function(id) {
			return $http.delete('/api/media/' + id);
		}
	}
});
