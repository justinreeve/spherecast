var projectjson5 = {
	id: 5,
	shortname: 'scary',
	title: 'Scary',
	subtitle: '',
	description: '',
	createdby: 1,
	timecreated: 0,
	startScene: 1,

	globalVariables: [],

	prefetchAssets: [],

	scenes: [
	{
		id: 1,
		shortname: 'eribuibni45nnvuf',
		projectid: 5,
		sequence: 1,
		fadeInDuration: 3000,
		fadeOutDuration: 0,
		parent: 0,
		title: 'Scary',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: '52',
			filePath: '/files/f2/f2cb57f217fc7b97b3d04cfe93caf47b932408a8',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
			id: 1,
			visible: 0,
			overridable: 1,
			actions: [
			{
				actionType: 'audio',
				actionItemId: 53,
				actionFile:
				{
					fileId: 53,
					filePath: '/files/aa/aa52589de91b55cb4c49af44ee172eba02aa6dea',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionVolume: 0.5,
				actionRepeat: true,
				actionSequence: 1,
				actionContinue: true,
			}],
		}],

		endEvents: [
		{
		}],

		hotspots: [
		{
			id: 100,
			positionX: 0,
			positionY: -1,
			positionZ: 0,
			radius: 0.2,
			duration: 1500,
			visible: 1,
			overridable: 0,
			toggleOtherHotspots: false,
			actions: [
			{
				actionType: 'jump',
				actionItemId: 2,
				actionSequence: 1,
			}],
		}],
	},
	{
		id: 2,
		shortname: 'beibuibni45nnvuf',
		projectid: 5,
		sequence: 2,
		fadeInDuration: 3000,
		fadeOutDuration: 0,
		parent: 0,
		title: 'Scary',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: '53',
			filePath: '/files/e7/e7bcf2a7a98beb5b2dccbef22e9d42a6ffda1123',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
		}],

		endEvents: [
		{
		}],

		hotspots: [
		{
			id: 5000,
			positionX: 0,
			positionY: -1,
			positionZ: 0,
			radius: 0.2,
			duration: 1500,
			visible: 1,
			overridable: 0,
			toggleOtherHotspots: false,
			actions: [
			{
				actionType: 'jump',
				actionItemId: 3,
				actionSequence: 1,
			}],
		}],
	},
	{
		id: 3,
		shortname: 'abibuibni45nnvuf',
		projectid: 5,
		sequence: 3,
		fadeInDuration: 3000,
		fadeOutDuration: 0,
		parent: 0,
		title: 'Forest scene with blinking eyes among trees',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: '55',
			filePath: '/files/a0/a0b1eeaef54238ed05c5cf77970a5f0af015b89b',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
		}],

		endEvents: [
		{
		}],

		hotspots: [
		{
			id: 102,
			positionX: 0,
			positionY: -1,
			positionZ: 0,
			radius: 0.2,
			duration: 1500,
			visible: 1,
			overridable: 0,
			toggleOtherHotspots: false,
			actions: [
			{
				actionType: 'jump',
				actionItemId: 1,
				actionSequence: 1,
			}],
		}],
	}],
};
