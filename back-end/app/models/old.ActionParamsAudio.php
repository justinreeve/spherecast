<?php
class ActionParamsAudio extends Eloquent
{
	protected $volume;
	protected $repeat;
	protected $file;					// File object.

	public function file()
	{
		return $this->hasOne('file', 'fileid');
	}
}
?>