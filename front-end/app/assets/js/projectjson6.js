var projectjson6 = {
	id: 6,
	shortname: 'snowshoe',
	title: 'Rollercoaster',
	subtitle: '',
	description: '',
	createdby: 1,
	timecreated: 0,
	startScene: 1,

	globalVariables: [],

	prefetchAssets:
	{
		video: [
		{
			id: 57,
			sceneid: 1,
			path: '/files/4b/4b258556465a44ff32524932e837ed0f25445faf',
			priority: 1,
			type: 'video',
		}],

		sceneManifest: [
		{
			sceneid: 1,
			paths: [
			{
				id: 'video_57',
				src: '/files/4b/4b258556465a44ff32524932e837ed0f25445faf',
				assetType: 'video',
			}],
		}],
	},

	scenes: [
	{
		id: 1,
		shortname: 'eribuibni45nnvuf',
		projectid: 2,
		sequence: 1,
		parent: 0,
		title: 'Rollercoaster',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: 57,
			filePath: '/files/7f/7f70792ac7667d9a2421fa8f260d11c4b66c8f01',
			fileType: 'video',
			fileVolume: 1.0,
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
			id: 1,
			visible: 0,
			overridable: 1,
			conditions: [],
			actions: [
/*
			{
				actionType: 'text',
				actionText: 'Snowshoe Hiking',
				actionTextHorizontalAlignment: 'center',
				actionTextVerticalAlignment: 'center',
				actionTextInTransition: 'fadeIn',
				actionTextInTransitionDuration: 1000,
				actionTextOutTransition: 'fadeOut',
				actionTextOutTransitionDuration: 1000,
				actionDuration: 3000,
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: 'Pyrenee Mountains',
				actionTextHorizontalAlignment: 'center',
				actionTextVerticalAlignment: 'center',
				actionTextInTransition: '',
				actionTextInTransitionDuration: 1000,
				actionTextOutTransition: '',
				actionTextOutTransitionDuration: 1000,
				actionDuration: 3000,
				actionSequence: 2,
				actionContinue: false,
			}
*/
			],
		}],

		endEvents: [
		{
		}],

		hotspots: [
		{
		}],
	}],
};
