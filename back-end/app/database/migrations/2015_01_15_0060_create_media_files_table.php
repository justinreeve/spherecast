<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media_files', function(Blueprint $table)
		{
			$table->integer('mediaId');
			$table->integer('fileId');
			$table->string('filename')->nullable();					// A different filename identifying this version of the file that probably won't appear to the user at any point.

			// Can be low, medium, high, or ultra.
			// TODO: Define this by a numerical resolution or something?
			$table->string('relativeSize');

			// The sequence is used for large files (i.e. images) that need to be pieced together. Depends on
			// the media type how it ends up looking, but it should be left-to-right, top-to-bottom.
			// TODO: We may need to specify how many images per row and column.
			$table->integer('sequence');

			// Indexes.
			$table->primary(array('mediaId', 'fileId'));
			$table->index('relativeSize');

			// Foreign keys.
			$table->foreign('mediaId')
				->references('id')->on('media')
				->onDelete('cascade')
				->onUpdate('cascade');
			$table->foreign('fileId')
				->references('id')->on('files')
				->onDelete('cascade')
				->onUpdate('cascade');

			// created_at, updated_at DATETIME
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('media_files', function(Blueprint $table)
		{
		});
	}

}
