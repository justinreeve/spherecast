<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaThumbnailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media_thumbnails', function(Blueprint $table)
		{
			$table->integer('mediaId');
			$table->integer('fileId');
			$table->integer('width');								// Can be 80, 160, or 320.
			$table->string('filename')->nullable();					// A different filename identifying this version of the file that probably won't appear to the user at any point.

			// Indexes.
			$table->primary(array('mediaId', 'fileId'));
			$table->index(array('mediaId', 'width'));

			// Foreign keys.
			$table->foreign('mediaId')
				->references('id')->on('media')
				->onDelete('cascade')
				->onUpdate('cascade');
			$table->foreign('fileId')
				->references('id')->on('files')
				->onDelete('cascade')
				->onUpdate('cascade');

			// created_at, updated_at DATETIME
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('media_thumbnails', function(Blueprint $table)
		{
		});
	}

}
