<!DOCTYPE html>
<head>
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<style>
html, body
{
	font-family: 'Open Sans', sans-serif; 
	margin: 0px;
	padding: 0px;
}

h1
{
	background-color: #f0f0f0;
	border-top: solid 1px #d0d0d0;
	border-bottom: solid 1px #d0d0d0;
	color: #404040;
	font-weight: normal;
	margin: 0px 0px 20px 0px;
	padding: 20px 0px;
	text-align: center;
}

h2
{
	background-color: #ddeeff;
	border-top: solid 1px #bbccdd;
	border-bottom: solid 1px #bbccdd;
	clear: both;
	color: #404040;
	font-weight: normal;
	margin: 0px 0px 20px 0px;
	padding: 20px 0px 20px 260px;
	text-align: left;
	width: 100%;
}

p
{
	margin-left: 80px;
}

div.example p
{
	font-style: italic;
	margin-left: 10px;
}

ul
{
	margin-left: 60px;
}

p.description
{
	font-weight: bold;
	margin-left: 260px;
}

div.toc
{
	background-color: #f0f0f0;
	max-width: 400px;
	padding: 10px;
}

	div.toc ul
	{
		list-style-type: none;
		margin: 0px;
	}

	div.toc a
	{
	}

div.attributes
{
	float: left;
	width: 64%;
}

div.attributes.subtype
{
	clear: both;
	float: none;
	width: 60%;
}

div.example
{
	background-color: #ddeeff;
	float: right;
	width: 34%;
}

	div.example pre
	{
		font-size: 12px;
		padding: 0px 10px;
	}

p.route
{
	border-top: solid 1px #d0d0d0;
	color: #557799;
	font-size: 20px;
	font-weight: bold;
	padding-top: 20px;
}

p.desc
{
	max-width: 800px;
}

span.todo
{
	font-size: 14px;
	font-style: italic;
	font-weight: bold;
}

div.return
{
	background-color: #f0f0f0;
	border: solid 1px #e0e0e0;
	margin-left: 40px;
	padding: 10px;
	max-width: 760px;
}

dl
{
	margin: 0px;
}

dt
{
	color: green;
	clear: left;
	float: left;
	font-weight: bold;
	text-align: right;
	width: 250px;
}

	dt:after
	{
		content: ":";
	}

dd
{
	margin: 0 0 0 260px;
	padding: 0 0 0.5em 0;
}

dl.options
{
	background-color: #f0f0f0;
	padding: 10px;
}

	dl.options dt
	{
		color: red;
		width: 150px;
	}

		dl.options dt:before
		{
			content: '"';
		}

		dl.options dt:after
		{
			content: '":';
		}

	dl.options dd
	{
		margin-left: 160px;
	}

</style>
</head>

<body>
