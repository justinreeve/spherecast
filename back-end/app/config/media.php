<?php
$mediaconfig = array(
	'base_url' => 'http://app.spherecast.org',
	'temp_dir' => '/var/www/spherecast/temp/',
	'content_dir' => '/var/www/spherecast/files/',
	'content_base_url' => 'http://app.spherecast.org/files/',
	'user_content_dir' => '/var/www/spherecast/userfiles/',
	'user_content_base_url' => 'http://app.spherecast.org/userfiles/',
	'ffmpeg_binary' => '/home/lls/bin/ffmpeg',
	'mp4box_binary' => '/usr/bin/MP4Box',
	'youtube_binary' => '/usr/local/bin/youtube-dl',
	'jod_converter_www' => 'http://localhost:8080/converter/service',
	'default_video_conversion_width' => 800,
	'default_video_conversion_height' => 480,

	'allowed_filetypes' => array('asf', 'avi', 'divx', 'doc', 'docx', 'flv', 'gif', 'htm', 'html', 'jpg', 'jpeg',
								'm4v', 'moov', 'mov', 'mp4', 'mpeg', 'mpg', 'odp', 'ods', 'odt', 'pdf', 'png',
								'ppt', 'pptx', 'rm', 'rmvb', 'rtf', 'txt', 'vob', 'wmv', 'xls', 'xlsx', '3gp'),

	'image_filetypes' => array('gif', 'jpg', 'jpeg', 'png'),

	'image_mimetypes' => array('gif' => 'image/gif',
								'jpg' => 'image/jpeg',
								'jpeg' => 'image/jpeg',
								'png' => 'image/png'),

	'video_filetypes' => array('asf', 'avi', 'divx', 'flv', 'm4v', 'moov', 'mov',
								'mp4', 'mpeg', 'mpg', 'rm', 'rmvb',
								'vob', 'wmv', '3gp'),

	'document_filetypes' => array('doc', 'docx', 'htm', 'html',
								'odp', 'ods', 'odt', 'pdf', 'ppt', 'pptx',
								'rtf', 'txt', 'xls', 'xlsx'),

	'document_mimetypes' => array(
								"doc" => "application/msword",
								"docx" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
								"htm" => "text/html",
								"html" => "text/html",
								"odp" => "application/vnd.oasis.opendocument.presentation",
								"ods" => "application/vnd.oasis.opendocument.spreadsheet",
								"odt" => "application/vnd.oasis.opendocument.text",
								"pdf" => "application/pdf",
								"ppt" => "application/vnd.ms-powerpoint",
								"pptx" => "application/vnd.openxmlformats-officedocument.presentationml.presentation",
								"rtf" => "text/rtf",
								"txt" => "text/plain",
								"xls" => "application/vnd.ms-excel",
								"xlsx" => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),

	'upload_max_filesize' => (int) (ini_get('upload_max_filesize')),
	'post_max_filesize' => (int) (ini_get('post_max_size')),
	'memory_limit' => (int) (ini_get('memory_limit')),
);

$mediaconfig['max_upload'] = min($mediaconfig['upload_max_filesize'], $mediaconfig['post_max_filesize'], $mediaconfig['memory_limit']);

return $mediaconfig;
?>