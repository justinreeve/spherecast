'use strict';

angular.module('ngWebglDemo')
  .directive('ngWebgl', function () {
    return {
      restrict: 'A',
      scope: { 
        'width': '=',
        'height': '=',
        'fillcontainer': '=',
        'scale': '=',
        'materialType': '='
      },
      link: function postLink(scope, element, attrs) {

		var sphereIndex = -1;
		var currentSphere = null;
		var camera, scene, renderer;
		var effect, controls;
		var element, container;
		var clock = new THREE.Clock();
		var countdown = 15;
	
        scope.init = function () {
	    	var ambient = document.getElementById('ambient');
	    	ambient.volume = 0.1;

	    	audio = document.getElementById('audio');
	    	audio.volume = 1.0;
			audio.addEventListener('ended', function()
			{
				setTimeout(function()
				{
					scope.gotoNextSphere();
				}, 2000);
			});
	        renderer = new THREE.WebGLRenderer();
	        element = renderer.domElement;
	        container = document.getElementById('example');
	        container.appendChild(element);

	        effect = new THREE.StereoEffect(renderer);
	        effect.separation = -6.2;
	        effect.setSize((window.innerWidth/2), window.innerHeight);

	        scene = new THREE.Scene();

	        camera = new THREE.PerspectiveCamera(90, (window.innerWidth/2) / window.innerHeight, 0.001, 10000);
	        camera.position.set(0, 10, 0);
	        scene.add(camera);

	        controls = new THREE.OrbitControls(camera, element);
	        controls.rotateUp(Math.PI / 4);
	        controls.target.set(
				camera.position.x + 0.1,
				camera.position.y,
				camera.position.z
	        );
	        controls.noZoom = true;
	        controls.noPan = true;

	        scope.setOrientationControls = function(e) {
	            if (!e.alpha) {
	                return;
	            }

	            controls = new THREE.DeviceOrientationControls(camera, true);
	            controls.connect();
	            controls.update();

	            element.addEventListener('click', scope.gotoNextSphere, false);

	            //Original code forgot to add ", true", so the listener was never removed
	            window.removeEventListener('deviceorientation', scope.setOrientationControls, true);
	        }
	
	        window.addEventListener('deviceorientation', scope.setOrientationControls, true);
	
	        scope.gotoNextSphere();
	
	        window.addEventListener('resize', scope.resize, false);
	        setTimeout(resize, 1);
        };

        // -----------------------------------
        // Event listeners
        // -----------------------------------
        scope.resize = function () {
	        var width = container.offsetWidth;
	        var height = container.offsetHeight;

	        camera.aspect = width / height;
	        camera.updateProjectionMatrix();

	        renderer.setSize(width, height);
	        effect.setSize(width, height);
        };

		scope.update = function(dt) {
		    scope.resize();
		    camera.updateProjectionMatrix();
		    controls.update(dt);
		}
		
		scope.render = function(dt) {
		    effect.render(scene, camera);
		}
		
		scope.animate = function(t) {
		    requestAnimationFrame(scope.animate);
		    scope.update(clock.getDelta());
		    scope.render(clock.getDelta());
		}

		scope.addSphere = function(idx) {
		    if (currentSphere) {
		        scene.remove(currentSphere);
		    }

		    currentSphere = new THREE.Mesh(
		            new THREE.SphereGeometry(100, 32, 32),
		            new THREE.MeshBasicMaterial({
		                map: THREE.ImageUtils.loadTexture('http://spherecast.org/ocean/spheres/' + spheres[idx])
		            })
		    );
		
		    currentSphere.scale.x = -1;
		    scene.add(currentSphere);
		
			var audio = document.getElementById('audio');
			var source = document.getElementById('audio-source');
			audio.src = 'audio/' + audiofiles[idx];
			source.currentTime = 0;
			audio.play();
		  }
		
		scope.gotoNextSphere = function() {
		    if (!scope.isFullScreen()) {
		        scope.fullscreen();
		    }
		
		    sphereIndex += 1;
		    if (sphereIndex>=spheres.length) {
		        sphereIndex = 0;
		    }
		
		    scope.addSphere(sphereIndex);
		}
		
		scope.isFullScreen = function() {
		    var fsElem =
		            document.fullscreenElement ||
		            document.msFullscreenElement ||
		            document.mozFullscreenElement ||
		            document.webkitFullscreenElement;
		
		    return ((fsElem != null) && (fsElem != undefined));
		}
		
		scope.fullscreen = function() {
		    if (container.requestFullscreen) {
		        container.requestFullscreen();
		    } else if (container.msRequestFullscreen) {
		        container.msRequestFullscreen();
		    } else if (container.mozRequestFullScreen) {
		        container.mozRequestFullScreen();
		    } else if (container.webkitRequestFullscreen) {
		        container.webkitRequestFullscreen();
		    }
		}

        // Begin
		scope.init();
		scope.animate();

      }
    };
  });
