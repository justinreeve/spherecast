/**
 * The word events will be processed as follows> A hotspot trigger plays the audio. After this, a global variable
 * is set that allows a specific voice event to be activated, and an "allowHotspots" global variable 
 */
var projectjson7 = {
	id: 7,
	shortname: '',
	title: 'Architech 3di',
	subtitle: '',
	description: '',
	createdby: 1,
	timecreated: 0,
	startScene: 1,
	adjustFontSize: '+4',						// TODO: Base this on renderer.devicePixelRatio, and have a separate file containing font adjustments based on different device resolutions.
//	voiceLanguage: 'de-DE',
	hotspotProfiles: [							// These are the default hotspot profiles per mode (currently "standard" or "VR" mode); this indicate how hotspots can be triggered by default, but can be overridden in specific hotspot "modeProfiles" profiles.
	{
		modeType: 'standard',
		activateByLook: false,
		activateByClick: true,
		activateByHover: false,					// Mouse hover.
		activateByDoubleTap: false,
		activateByController: false,
		activateByVoice: false,
	},
	{
		modeType: 'vr',
		activateByLook: true,
		activateByClick: false,
		activateByHover: false,
		activateByDoubleTap: false,
		activateByController: false,
		activateByVoice: false,
	}],

	hotspotProfileStandard: 
	{
		activateByLook: false,
		activateByClick: true,
		activateByHover: false,
		activateByDoubleTap: false,
		activateByController: false,
		activateByVoice: false,
	},

	hotspotProfileVR:
	{
		activateByLook: true,
		activateByClick: false,
		activateByHover: false,
		activateByDoubleTap: false,
		activateByController: false,
		activateByVoice: false,
	},

	globalVariables: [
	{
		name: 'allowHotspots',
		defaultValue: '1',
	}],

	assets: [
	{
		sceneId: 1,
		media: [
		{
			id: 84,
			sceneid: 1,
			path: '/files/44/44a9c13006362e8bb5ed9840578923ce644a2690',
			priority: 1,
			type: 'image',
		}],
	}],

	scenes: [
	{
		id: 1,
		shortname: 'b945jaodmgio5mgg',
		projectid: 3,
		sequence: 1,
		parent: 0,
		title: 'House',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: 84,
			filePath: '/files/44/44a9c13006362e8bb5ed9840578923ce644a2690',
			fileType: 'image',
		},
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
			id: 100,
			visible: 0,
			overridable: 1,
			actions: [
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '0',
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'video',
				actionItemId: 86,
				actionMedia:
				{
					id: 86,
//					fileScaleX: 0.2,
//					fileScaleY: 0.2,
					fileType: 'video',
					fileIdLow: 86,
					fileIdMedium: 86,
					fileIdHigh: 86,
					fileIdUltra: 86,				// Unlike spritemaps, an array can't be specified here, since we're not splitting up the video.
					files: [						// An array of all the possible files involved with this media item.
					{
						fileId: 86,
						filePath: '/files/e7/e7ad0c2e32f46107e643d3b184629580f30d92fc',
						fileType: 'video',
						fileDuration: 1900,
						fileHeight: 540,
						fileWidth: 960,
						fileSize: 3639413,
						fileMimeType: 'video/mp4',
					}],
				},
				videoTransparencyColor: '0xd400',
				videoHeight: 540,
				videoWidth: 960,
				videoScaleX: 0.2,
				videoScaleY: 0.2,
				videoVolume: 1.0,
				videoInTransition: 'fadeIn',
				videoInTransitionDuration: 1000,
				videoOutTransition: 'fadeOut',
				videoOutTransitionDuration: 1000,
				actionDuration: 1900,
				actionRepeat: false,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '1',
				actionSequence: 3,
				actionContinue: false,
			}],
		}],

		endEvents: [
		{
		}],

		voiceEvents: [],

		hotspots: [
		{
			id: 300,
			positionX: -98.611,
			positionY: 7.473,
			positionZ: 10.375,
			radius: 20,
			duration: 3000,								// Milliseconds to activate by look, if applicable.
			visible: 1,
			overridable: 0,
			blockOtherHotspots: false,					// If "true" then this hotspot is exclusive when activated. No other hotspots in the scene can be activated until this hotspot is activated again.
			conditions: [],
			labels: [
			{
				labelId: 1000,
				labelType: 'text',
				labelText: '5539 Main Street',
				labelFontSize: 28,
				labelPositionX: -84.322,
				labelPositionY: -51.863,
				labelPositionZ: 10.553,
			},
			{
				labelId: 1000,
				labelType: 'text',
				labelText: 'Anytown, USA',
				labelFontSize: 28,
				labelPositionX: -80.421,
				labelPositionY: -58.462,
				labelPositionZ: 5.807,
			},
			{
				labelId: 1001,
				labelType: 'image',
				labelMedia: {},
				labelFile:
				{
					fileId: 83,
					filePath: '/files/86/864ac839e255d99ee2d26dfe9f24e95c091a5693',
					fileType: 'image',
				},
				labelPositionX: -98.611,
				labelPositionY: 7.473,
				labelPositionZ: 10.375,
				labelScaleX: 0.2,
				labelScaleY: 0.3,
			}],
			countdownSpritemap:						// Exactly one optional countdown spritemap can be present.
			{
				id: 1080,							// All spritemap animation objects should have unique ids.
				positionX: -98.611,
				positionY: 7.473,
				positionZ: 10.375,
				spritemapTilesNum: 16,
				spritemapTilesHorizontal: 4,
				spritemapTilesVertical: 4,
				spritemapHeight: 992,
				spritemapWidth: 1164,
				spritemapDuration: 3000,			// TODO: If not specified, this should simply be the "duration" of the hotspot; divide this by the number of tiles to figure out how long to display each tile.
				spritemapRepeat: false,				// Don't repeat the animation continuously.
				spritemapShowFirstAsFixed: true,	// Use the first item in the sequence as a fixed label.
				spritemapTileSequence: [			// An ordered sequence of all the tiles we'll be playing in the animation, numbered left-to-right, top-to-bottom. To just have a static image, we could just have one tile listed in this, then the spritemap could be used, for example, for a character that moves around at different intervals.
					1, 2, 3, 4, 5, 6,
					7, 8, 9, 10, 11, 12,
					13, 14, 15, 16
				],
				media:								// The media handler should use device, transfer speed, and latency detection to determine if 
				{
					id: 5001,
					fileScaleX: 0.2,
					fileScaleY: 0.2,
					fileType: 'spritemap',
					fileIdLow: 73,
					fileIdMedium: 73,
					fileIdHigh: 73,
					fileIdUltra: [74, 75, 76, 77],	// If an array is specified instead of a number, this means the total image is spliced together from different files, in left-to-right, top-to-bottom order. TODO: We may need to specify how many images per row and column.
					files: [						// An array of all the possible files involved with this media item.
					{
						fileId: 73,
						filePath: '/files/c2/c20add16e1001b0f6675373833fbb542a826dbc1',
						fileType: 'spritemap',
						fileHeight: 750,
						fileWidth: 1192,
					},
					{
						fileId: 74,
						filePath: '/files/12/123450000',
						fileType: 'image',
					},
					{
						fileId: 75,
						filePath: '/files/12/123450001',
						fileType: 'image',
					},
					{
						fileId: 76,
						filePath: '/files/12/123450002',
						fileType: 'image',
					},
					{
						fileId: 77,
						filePath: '/files/12/123450003',
						fileType: 'image',
					}],
				},
			},
			actions: [
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '0',
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'jump',
				actionItemId: 2,
				actionSequence: 2,
			}],
		}],
	}],
};
