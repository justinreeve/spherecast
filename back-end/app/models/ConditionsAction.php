<?php
class ConditionsAction extends Eloquent
{
	protected $id;
	protected $actionId;
	protected $type;
	protected $variableName;
	protected $variableOperator;			// Options: =, !=, >, <, contains.
	protected $variableOperand;

	protected $guarded = array('id');
	protected $table = 'conditions_actions';

	public function action()
	{
		return $this->belongsTo('Action', 'actionId', 'id');
	}
}
?>