<?php
class MediaThumbnail extends Eloquent
{
	protected $mediaId;
	protected $fileId;
	protected $filename;
	protected $width;

	protected $fillable = array('mediaId', 'fileId', 'filename', 'width');
	protected $table = 'media_thumbnails';

	public function media()
	{
		$this->belongsTo('Media', 'mediaId', 'id');
	}
}
?>