spherecastApp.controller('MediaController', ['$scope', '$http', '$routeParams', '$upload', function($scope, $http, $routeParams, $upload)
{
	$scope.$watch('files', function()
	{
//		console.log($scope.activeProject);
//		console.log($scope.activeScene);
		if (typeof $scope.files !== 'undefined')
		{
			for (var f = 0; f < $scope.files.length; f++)
			{
				var file = $scope.files[f];
				$scope.upload = $upload.upload(
				{
					method: 'POST',
					url: '/api/media',
					file: file,
					fields: {'projectId': $scope.projectId, 'sceneId': $scope.sceneId, 'sessionId': ''}
				}).progress(function(evt)
				{
					console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total) + '% file :'+ evt.config.file.name);
				}).success(function(data, status, headers, config)
				{
					console.log('file ' + config.file.name + ' is uploaded successfully. Response: ' + data);
				});
			}
		}
	});
}]);