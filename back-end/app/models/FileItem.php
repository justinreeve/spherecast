<?php
class FileItem extends Eloquent
{
	protected $id;
	protected $contenthash;
	protected $filepath;
	protected $title;
	protected $filename;						// Filename for display in the file manager.
	protected $mimetype;
	protected $width;							// Original width of image or video (if applicable).
	protected $height;							// Original height of image or video (if applicable).
	protected $length;							// Length of audio in seconds (if applicable).
	protected $filesize;						// Filesize in bytes.
	protected $extension;						// Extension (jpg, png, mp3, etc.)
	
	protected $guarded = array('id');
	protected $table = 'files';

	public function mediaFiles()
	{
		return $this->belongsToMany('Media', 'media_files', 'fileId', 'mediaId');
	}

	public function getId()
	{
		return $this->getAttribute('id');
	}
}
?>