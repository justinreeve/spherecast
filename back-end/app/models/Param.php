<?php
class Param extends Eloquent
{
	protected $table = 'params';

	protected $itemId;
	protected $itemType;		// action, label, media, hotspot
	protected $value;

	protected $fillable = array('itemId', 'itemType', 'value');
}
?>