@include('docs.docsheader')
<h1>Events</h1>

<div class="attributes">
	<p>Events are a series of actions that execute in sequential order. There are four types of events:</p>
	<ul>
		<li>startEvents: events that occur when the scene first loads</li>
		<li>endEvents: events that occur immediately before a jump action to another scene</li>
		<li>voiceEvents: events that occur in response to voice commands via the Chrome Web Speech API</li>
		<li>hotspots: events that occur when triggered by a hotspot placed at a specific location on the scene</li>
	</ul>

	<dl>
		<dt class="integer">id</dt>
		<dd>event id</dd>

		<dt class="string">shortname</dt>
		<dd>unique code identifying the event</dd>

		<dt class="integer"><a href="/api/docs/scenes">sceneId</a></dt>
		<dd>scene id the event is associated with</dd>

		<dt class="boolean" data-default="false">overridable</dt>
		<dd>if false, the event cannot be interrupted if another event is initiated</dd>

		<dt class="array" data-default="null"><a href="/api/docs/conditions">conditions</a></dt>
		<dd>a list of conditions which must be met before the event can be triggered</dd>

		<dt class="array" data-default="null"><a href="/api/docs/actions">actions</a></dt>
		<dd>a list of actions associated with the event, which sequentially occur when the event is triggered</dd>
	</dl>

	<p>The voiceEvents and hotspots contain additional attributes to describe how and when they are triggered. These extra attributes are listed below.</p>

</div>

<div class="example">
	<p>Start event example:</p>
	<pre>
{
	"id": "10",
	"shortname": "h5htagjj",
	"sceneId": "1",
	"type": "start",
	"overridable": "0",
	"created_at": "-0001-11-30 00:00:00",
	"updated_at": "-0001-11-30 00:00:00",
	"actions": [{...}]
}
	</pre>

	<p>Hotspot example:</p>
	<pre>
{
	"id": "1",
	"shortname": "48nvm39jf",
	"sceneId": "1",
	"type": "hotspot",
	"overridable": "0",
	"created_at": "-0001-11-30 00:00:00",
	"updated_at": "-0001-11-30 00:00:00",
	"positionX": "54.214",
	"positionY": "-56.444",
	"positionZ": "61.705",
	"radius": "40.000",
	"duration": "1500",
	"blockOtherHotspots": "0",
	"countdownSpritemap": {...},
	"conditions": [{...}],
	"actions": [{...}],
	"labels": [{...}]
}
	</pre>
</div>

<h2>VoiceEvents</h2>
<p class="description"></p>
<div class="attributes subtype">
	<dl>
		<dt class="array">commands</dt>
		<dd>a list of all possible words or phrases that can be spoken to trigger the event</dd>
	</dl>
</div>

<h2>Hotspots</h2>
<p class="description"></p>
<div class="attributes subtype">
	<dl>
		<dt class="decimal">positionX</dt>
		<dd>the X coordinate for the hotspot on the circumference of the sphere</dd>

		<dt class="decimal">positionY</dt>
		<dd>the Y coordinate for the hotspot on the circumference of the sphere</dd>

		<dt class="decimal">positionZ</dt>
		<dd>the Z coordinate for the hotspot on the circumference of the sphere</dd>

		<dt class="decimal">radius</dt>
		<dd>the radius of the circle around the XYZ position that can trigger this hotspot; if two hotspots overlap, both will be triggered (note that this will invalidate one of them if both are flagged as overridable)</dd>

		<dt class="integer">duration</dt>
		<dd>the number of milliseconds that the hotspot must be "looked at" in VR mode to be triggered</dd>

		<dt class="boolean" data-default="false">blockOtherHotspots</dt>
		<dd>if true, no other hotspot can be triggered until this hotspot's actions are complete</dd>

		<dt class="object" data-default="null"><a href="/api/docs/spritemaps">countdownSpritemap</a></dt>
		<dd>a spritemap object describing the animation to show while a hotspot is being triggered, before the <a href="#">duration</a> is reached</dd>

		<dt class="array" data-default="null"><a href="/api/docs/labels">labels</a></dt>
		<dd>a list of labels which appear along with the hotspot; these can be either text or images</dd>
	</dl>
</div>

@include('docs.docsfooter')