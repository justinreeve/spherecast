<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSceneeventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sceneevents', function(Blueprint $table)
		{
			// If true, prevent hotspots from initiating while this event's actions are executing.
			$table->boolean('blockHotspots')->default(false);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sceneevents', function(Blueprint $table)
		{
			$table->dropColumn('blockHotspots');
		});
	}
}
