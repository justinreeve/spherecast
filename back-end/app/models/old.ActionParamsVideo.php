<?php
class ActionParamsVideo extends Eloquent
{
	protected $type;					// This can be "overlay" or "position."
	protected $positionX;				// Only used when $type is "position."
	protected $positionY;				// Only used when $type is "position."
	protected $positionZ;				// Only used when $type is "position."
	protected $width = '';				// Resized width of video; default is 75% of screen for overlay
	protected $height = '';				// Resized height of video; default preserves aspect ratio of width
	protected $file;					// File object.
	protected $allowRewindByVoice;
	protected $allowForwardByVoice;
	protected $allowPauseByVoice;
	protected $allowPlayByVoice;
	protected $allowStopByVoice;		// Allow using "stop" vocal command to stop playing the video altogether.
	protected $autoplay;

	public function file()
	{
		return $this->hasOne('file', 'fileid');
	}
}
?>