/**
 * The word events will be processed as follows> A hotspot trigger plays the audio. After this, a global variable
 * is set that allows a specific voice event to be activated, and an "allowHotspots" global variable 
 */
var projectjson3 = {
	id: 3,
	shortname: 'language',
	title: 'Language Learning',
	subtitle: '',
	description: '',
	createdby: 1,
	timecreated: 0,
	startScene: 1,
	adjustFontSize: '+4',						// TODO: Base this on renderer.devicePixelRatio, and have a separate file containing font adjustments based on different device resolutions.
//	voiceLanguage: 'de-DE',
	hotspotProfiles: [							// These are the default hotspot profiles per mode (currently "standard" or "VR" mode); this indicate how hotspots can be triggered by default, but can be overridden in specific hotspot "modeProfiles" profiles.
	{
		modeType: 'standard',
		activateByLook: false,
		activateByClick: true,
		activateByHover: false,					// Mouse hover.
		activateByDoubleTap: false,
		activateByController: false,
		activateByVoice: false,
	},
	{
		modeType: 'vr',
		activateByLook: true,
		activateByClick: false,
		activateByHover: false,
		activateByDoubleTap: false,
		activateByController: false,
		activateByVoice: false,
	}],

	hotspotProfileStandard: 
	{
		activateByLook: false,
		activateByClick: true,
		activateByHover: false,
		activateByDoubleTap: false,
		activateByController: false,
		activateByVoice: false,
	},

	hotspotProfileVR:
	{
		activateByLook: true,
		activateByClick: false,
		activateByHover: false,
		activateByDoubleTap: false,
		activateByController: false,
		activateByVoice: false,
	},

	globalVariables: [
	{
		name: 'allowHotspots',
		defaultValue: '1',
	},
	{
		name: 'listenBookshelfWord',
		defaultValue: '0',
	},
	{
		name: 'listenBookshelfSentence',
		defaultValue: '0',
	}],

	prefetchAssets: [
	{
		image: [
		{
			id: 22,
			sceneid: 1,
			path: '/files/7d/7d0053558759ec36b10b4fbbc9c0a8e3e7eaa358',
			priority: 1,
			type: 'image',
		}],
	}],

	scenes: [
	{
		id: 1,
		shortname: 'b945jaodmgio5mgg',
		projectid: 3,
		sequence: 1,
		parent: 0,
		title: 'Living Room',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: '22',
			filePath: '/files/87/87263357b0caa62bde1d6ed5960c3c5734e9a1e4',
			fileType: 'image',
		},
//		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
/*
			id: 600,
			overridable: 0,
			actions: [
			{
				id: 8000,
				actionType: 'text',
				actionText: 'Big Text Big Text Big Text Big Text Big Text',
				actionTextPosition: 'bottom center',
				actionDuration: 10000,
				actionSequence: 1,
				actionContinue: false,
			}],
*/
		}],

		endEvents: [
		{
		}],

		voiceEvents: [],

		hotspots: [
		{
			id: 1,
			positionX: 0,
			positionY: -100,
			positionZ: 0,
			radius: 20,
			duration: 3000,								// Milliseconds to activate by look, if applicable.
			visible: 1,
			overridable: 0,
			blockOtherHotspots: false,					// If "true" then this hotspot is exclusive when activated. No other hotspots in the scene can be activated until this hotspot is activated again.
			conditions: [],
			countdownSpritemap:						// Exactly one optional countdown spritemap can be present.
			{
				id: 1080,							// All spritemap animation objects should have unique ids.
				positionX: 0,
				positionY: -100,
				positionZ: 0,
				spritemapTilesNum: 16,
				spritemapTilesHorizontal: 4,
				spritemapTilesVertical: 4,
				spritemapHeight: 992,
				spritemapWidth: 1164,
				spritemapDuration: 3000,			// TODO: If not specified, this should simply be the "duration" of the hotspot; divide this by the number of tiles to figure out how long to display each tile.
				spritemapRepeat: false,				// Don't repeat the animation continuously.
				spritemapShowFirstAsFixed: true,	// Use the first item in the sequence as a fixed label.
				spritemapTileSequence: [			// An ordered sequence of all the tiles we'll be playing in the animation, numbered left-to-right, top-to-bottom. To just have a static image, we could just have one tile listed in this, then the spritemap could be used, for example, for a character that moves around at different intervals.
					1, 2, 3, 4, 5, 6,
					7, 8, 9, 10, 11, 12,
					13, 14, 15, 16
				],
				media:								// The media handler should use device, transfer speed, and latency detection to determine if 
				{
					id: 5001,
					fileScaleX: 0.2,
					fileScaleY: 0.2,
					fileType: 'spritemap',
					fileIdLow: 73,
					fileIdMedium: 73,
					fileIdHigh: 73,
					fileIdUltra: [74, 75, 76, 77],	// If an array is specified instead of a number, this means the total image is spliced together from different files, in left-to-right, top-to-bottom order. TODO: We may need to specify how many images per row and column.
					files: [						// An array of all the possible files involved with this media item.
					{
						fileId: 73,
						filePath: '/files/c2/c20add16e1001b0f6675373833fbb542a826dbc1',
						fileType: 'spritemap',
						fileHeight: 750,
						fileWidth: 1192,
					},
					{
						fileId: 74,
						filePath: '/files/12/123450000',
						fileType: 'image',
					},
					{
						fileId: 75,
						filePath: '/files/12/123450001',
						fileType: 'image',
					},
					{
						fileId: 76,
						filePath: '/files/12/123450002',
						fileType: 'image',
					},
					{
						fileId: 77,
						filePath: '/files/12/123450003',
						fileType: 'image',
					}],
				},
			},
			actions: [
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '1',
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'jump',
				actionItemId: 2,
				actionSequence: 2,
			}],
		},
		{
			id: 2,
			positionX: 54.214,
			positionY: -56.444,
			positionZ: 61.705,
			radius: 40,
			duration: 1500,
			conditions: [
			{
				conditionType: 'variable',
				conditionVariableName: 'allowHotspots',
				conditionVariableOperator: '=',
				conditionVariableOperand: '1',
			}],
			labels: [
			{
				labelId: 800,
				labelType: 'text',
				labelText: 'Bücherregal',
				labelPositionX: 54.214,
				labelPositionY: -56.444,
				labelPositionZ: 61.705,
				labelDisappearOnCountdown: false,
			},
			{
				labelId: 900,
				labelType: 'text',
				labelText: 'Bookshelf',
				labelFontSize: 14,
				labelFontStyle: 'italic',
				labelPositionX: 54.214,
				labelPositionY: -61.444,
				labelPositionZ: 61.705,
				labelDisappearOnCountdown: false,
			},
/*
			{
				labelId: 1000,
				labelType: 'image',
				labelMedia: {},
				labelFile:
				{
					fileId: 58,
					filePath: '/files/7f/7fde3bfcb6dd9ec01c1295fc5463bbe982d1abbc',
					fileType: 'image',
				},
				labelPositionX: 54.214,
				labelPositionY: -56.444,
				labelPositionZ: 61.705,
				labelScaleX: 0.2,
				labelScaleY: 0.2,
				labelDisappearOnCountdown: true,
			}
*/
			],
			countdownSpritemap:						// Exactly one optional countdown spritemap can be present.
			{
				id: 1050,							// All spritemap animation objects should have unique ids.
				positionX: 54.214,
				positionY: -56.444,
				positionZ: 61.705,
				spritemapTilesNum: 16,
				spritemapTilesHorizontal: 4,
				spritemapTilesVertical: 4,
				spritemapHeight: 992,
				spritemapWidth: 1164,
				spritemapDuration: 1500,			// TODO: If not specified, this should simply be the "duration" of the hotspot; divide this by the number of tiles to figure out how long to display each tile.
				spritemapRepeat: false,				// Don't repeat the animation continuously.
				spritemapShowFirstAsFixed: true,	// Use the first item in the sequence as a fixed label.
				spritemapTileSequence: [			// An ordered sequence of all the tiles we'll be playing in the animation, numbered left-to-right, top-to-bottom. To just have a static image, we could just have one tile listed in this, then the spritemap could be used, for example, for a character that moves around at different intervals.
					1, 2, 3, 4, 5, 6,
					7, 8, 9, 10, 11, 12,
					13, 14, 15, 16
				],
				media:								// The media handler should use device, transfer speed, and latency detection to determine if 
				{
					id: 5000,
					fileScaleX: 0.2,
					fileScaleY: 0.2,
					fileType: 'spritemap',
					fileIdLow: 73,
					fileIdMedium: 73,
					fileIdHigh: 73,
					fileIdUltra: [74, 75, 76, 77],	// If an array is specified instead of a number, this means the total image is spliced together from different files, in left-to-right, top-to-bottom order. TODO: We may need to specify how many images per row and column.
					files: [						// An array of all the possible files involved with this media item.
					{
						fileId: 73,
						filePath: '/files/c2/c20add16e1001b0f6675373833fbb542a826dbc1',
						fileType: 'spritemap',
						fileHeight: 750,
						fileWidth: 1192,
					},
					{
						fileId: 74,
						filePath: '/files/12/123450000',
						fileType: 'image',
					},
					{
						fileId: 75,
						filePath: '/files/12/123450001',
						fileType: 'image',
					},
					{
						fileId: 76,
						filePath: '/files/12/123450002',
						fileType: 'image',
					},
					{
						fileId: 77,
						filePath: '/files/12/123450003',
						fileType: 'image',
					}],
				},
			},
			visible: 1,
			overridable: 1,
			blockOtherHotspots: false,

			// Disable other hotspots, change the label, play the word, prompt user to repeat the word, activate word listener.
			actions: [
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '0',
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'changeLabel',
				actionItemId: 900,
				actionText: '',
				actionDuration: 0,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'audio',
				actionItemId: 58,
				actionMedia: {},
				actionVolume: 1.0,
				actionFile:
				{
					fileId: 58,
					filePath: '/files/7f/7f6867a50c4004ea66a05726ddb855d3e0d08029',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 4,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionTextPosition: 'bottom center',
				actionDuration: 1000,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'variable',
				actionVariableName: 'listenBookshelfWord',
				actionVariableOperator: 'set',
				actionVariableOperand: '1',
				actionSequence: 5,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: 'Das Bücherregal hat viele Bücher',
				actionTextPosition: 'bottom center',
				actionDuration: 3000,
				actionSequence: 2,
				actionContinue: true,
			},
			{
				actionType: 'audio',					// German sentence
				actionItemId: 59,
				actionMedia: {},
				actionVolume: 1.0,
				actionFile:
				{
					fileId: 59,
					filePath: '/files/54/540c59f2b6c50578ef8fc3ad48a2c7871b785d72',
//					filePath: '',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 3,
				actionContinue: false,
			},
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '1',
				actionSequence: 1,
				actionContinue: false,
			}],
		},
		{
			id: 3,
			positionX: -3.790,
			positionY: -39.605,
			positionZ: 91.383,
			radius: 20,
			duration: 1500,
			visible: 1,
			overridable: 1,
			blockOtherHotspots: true,
			conditions: [
			{
				conditionType: 'variable',
				conditionVariableName: 'allowHotspots',
				conditionVariableOperator: '=',
				conditionVariableOperand: '1',
			}],
			labels: [
			{
				labelId: 801,
				labelType: 'text',
				labelText: 'Lampe',
				labelPositionX: -3.790,
				labelPositionY: -39.605,
				labelPositionZ: 91.383,
			},
			{
				labelId: 901,
				labelType: 'text',
				labelText: 'Lamp',
				labelFontSize: 14,
				labelFontStyle: 'italic',
				labelPositionX: -4.017,
				labelPositionY: -42.885,
				labelPositionZ: 89.809,
			},
/*
			{
				labelId: 1001,
				labelType: 'image',
				labelMedia: {},
				labelFile:
				{
					fileId: 58,
					filePath: '/files/7f/7fde3bfcb6dd9ec01c1295fc5463bbe982d1abbc',
					fileType: 'image',
				},
				labelPositionX: -3.790,
				labelPositionY: -39.605,
				labelPositionZ: 91.383,
				labelScaleX: 0.2,
				labelScaleY: 0.2,
			}
*/
			],
			countdownSpritemap:						// Exactly one optional countdown spritemap can be present.
			{
				id: 1051,							// All spritemap animation objects should have unique ids.
				positionX: -3.790,
				positionY:  -39.605,
				positionZ: 91.383,
				spritemapTilesNum: 16,
				spritemapTilesHorizontal: 4,
				spritemapTilesVertical: 4,
				spritemapHeight: 992,
				spritemapWidth: 1164,
				spritemapDuration: 1500,			// TODO: If not specified, this should simply be the "duration" of the hotspot; divide this by the number of tiles to figure out how long to display each tile.
				spritemapRepeat: false,				// Don't repeat the animation continuously.
				spritemapShowFirstAsFixed: true,	// Use the first item in the sequence as a fixed label.
				spritemapTileSequence: [			// An ordered sequence of all the tiles we'll be playing in the animation, numbered left-to-right, top-to-bottom. To just have a static image, we could just have one tile listed in this, then the spritemap could be used, for example, for a character that moves around at different intervals.
					1, 2, 3, 4, 5, 6,
					7, 8, 9, 10, 11, 12,
					13, 14, 15, 16
				],
				media:								// The media handler should use device, transfer speed, and latency detection to determine if 
				{
					id: 5001,
					fileScaleX: 0.2,
					fileScaleY: 0.2,
					fileType: 'spritemap',
					fileIdLow: 73,
					fileIdMedium: 73,
					fileIdHigh: 73,
					fileIdUltra: [74, 75, 76, 77],	// If an array is specified instead of a number, this means the total image is spliced together from different files, in left-to-right, top-to-bottom order. TODO: We may need to specify how many images per row and column.
					files: [						// An array of all the possible files involved with this media item.
					{
						fileId: 73,
						filePath: '/files/c2/c20add16e1001b0f6675373833fbb542a826dbc1',
						fileType: 'spritemap',
						fileHeight: 750,
						fileWidth: 1192,
					},
					{
						fileId: 74,
						filePath: '/files/12/123450000',
						fileType: 'image',
					},
					{
						fileId: 75,
						filePath: '/files/12/123450001',
						fileType: 'image',
					},
					{
						fileId: 76,
						filePath: '/files/12/123450002',
						fileType: 'image',
					},
					{
						fileId: 77,
						filePath: '/files/12/123450003',
						fileType: 'image',
					}],
				},
			},
			actions: [
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '0',
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'audio',
				actionItemId: 63,
				actionMedia: {},
				actionFile:
				{
					fileId: 63,
					filePath: '/files/29/29d94d65fa515a9ee16c559ffea7f7fb425dca52',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionTextPosition: 'bottom center',
				actionDuration: 1000,
				actionSequence: 3,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: 'Bitte schalten Sie die Lampe.',
				actionTextPosition: 'bottom center',
				actionDuration: 3000,
				actionSequence: 4,
				actionContinue: true,
			},
			{
				actionType: 'audio',					// German sentence
				actionItemId: 59,
				actionMedia: {},
				actionFile:
				{
					fileId: 64,
					filePath: '/files/20/209ac10bf646b0d3862d2a2c967650ac6f235ec1',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 5,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionTextPosition: 'bottom center',
				actionDuration: 1000,
				actionSequence: 6,
				actionContinue: false,
			},
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '1',
				actionSequence: 7,
				actionContinue: false,
			}],
		},
		{
			id: 4,
			positionX: -92,
			positionY: -42,
			positionZ: -8,
			radius: 30,
			duration: 1500,
			visible: 1,
			overridable: 1,
			blockOtherHotspots: true,
			conditions: [
			{
				conditionType: 'variable',
				conditionVariableName: 'allowHotspots',
				conditionVariableOperator: '=',
				conditionVariableOperand: '1',
			}],
			labels: [
			{
				labelId: 802,
				labelType: 'text',
				labelText: 'Fernsehen',
				labelPositionX: -92,
				labelPositionY: -37,
				labelPositionZ: -8,
			},
			{
				labelId: 902,
				labelType: 'text',
				labelText: 'Television',
				labelFontSize: 14,
				labelFontStyle: 'italic',
				labelPositionX: -92,
				labelPositionY: -42,
				labelPositionZ: -8,
			},
/*
			{
				labelId: 903,
				labelType: 'image',
				labelMedia: {},
				labelFile:
				{
					fileId: 58,
					filePath: '/files/7f/7fde3bfcb6dd9ec01c1295fc5463bbe982d1abbc',
					fileType: 'image',
				},
				labelPositionX: -82,
				labelPositionY: -37,
				labelPositionZ: -8,
				labelScaleX: 0.2,
				labelScaleY: 0.2,
			}
*/
			],
			countdownSpritemap:						// Exactly one optional countdown spritemap can be present.
			{
				id: 1052,							// All spritemap animation objects should have unique ids.
				positionX: -92,
				positionY:  -42,
				positionZ: -8,
				spritemapTilesNum: 16,
				spritemapTilesHorizontal: 4,
				spritemapTilesVertical: 4,
				spritemapHeight: 992,
				spritemapWidth: 1164,
				spritemapDuration: 1500,			// TODO: If not specified, this should simply be the "duration" of the hotspot; divide this by the number of tiles to figure out how long to display each tile.
				spritemapRepeat: false,				// Don't repeat the animation continuously.
				spritemapShowFirstAsFixed: true,	// Use the first item in the sequence as a fixed label.
				spritemapTileSequence: [			// An ordered sequence of all the tiles we'll be playing in the animation, numbered left-to-right, top-to-bottom. To just have a static image, we could just have one tile listed in this, then the spritemap could be used, for example, for a character that moves around at different intervals.
					1, 2, 3, 4, 5, 6,
					7, 8, 9, 10, 11, 12,
					13, 14, 15, 16
				],
				media:								// The media handler should use device, transfer speed, and latency detection to determine if 
				{
					id: 5002,
					fileScaleX: 0.2,
					fileScaleY: 0.2,
					fileType: 'spritemap',
					fileIdLow: 73,
					fileIdMedium: 73,
					fileIdHigh: 73,
					fileIdUltra: [74, 75, 76, 77],	// If an array is specified instead of a number, this means the total image is spliced together from different files, in left-to-right, top-to-bottom order. TODO: We may need to specify how many images per row and column.
					files: [						// An array of all the possible files involved with this media item.
					{
						fileId: 73,
						filePath: '/files/c2/c20add16e1001b0f6675373833fbb542a826dbc1',
						fileType: 'spritemap',
						fileHeight: 750,
						fileWidth: 1192,
					},
					{
						fileId: 74,
						filePath: '/files/12/123450000',
						fileType: 'image',
					},
					{
						fileId: 75,
						filePath: '/files/12/123450001',
						fileType: 'image',
					},
					{
						fileId: 76,
						filePath: '/files/12/123450002',
						fileType: 'image',
					},
					{
						fileId: 77,
						filePath: '/files/12/123450003',
						fileType: 'image',
					}],
				},
			},
			actions: [
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '0',
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'audio',
				actionItemId: 63,
				actionMedia: {},
				actionFile:
				{
					fileId: 71,
					filePath: '/files/3e/3e3331fe041bfe8f9b3268c31d39e732f2d4d363',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionTextPosition: 'bottom center',
				actionDuration: 1000,
				actionSequence: 3,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: 'Bitte mach den Fernseher lauter!',
				actionTextPosition: 'bottom center',
				actionDuration: 3000,
				actionSequence: 4,
				actionContinue: true,
			},
			{
				actionType: 'audio',					// German sentence
				actionItemId: 59,
				actionMedia: {},
				actionFile:
				{
					fileId: 72,
					filePath: '/files/f1/f1e132f6ce970811ba350ffba96786377e713754',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 5,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionTextPosition: 'bottom center',
				actionDuration: 1000,
				actionSequence: 6,
				actionContinue: false,
			},
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '1',
				actionSequence: 7,
				actionContinue: false,
			}],
		},
		{
			id: 5,
			positionX: -2,
			positionY: -17,
			positionZ: -98,
			radius: 20,
			duration: 1500,
			visible: 1,
			overridable: 1,
			blockOtherHotspots: true,
			conditions: [
			{
				conditionType: 'variable',
				conditionVariableName: 'allowHotspots',
				conditionVariableOperator: '=',
				conditionVariableOperand: '1',
			}],
			labels: [
			{
				labelId: 803,
				labelType: 'text',
				labelText: 'Spiegel',
				labelPositionX: -2,
				labelPositionY: -17,
				labelPositionZ: -98,
			},
			{
				labelId: 903,
				labelType: 'text',
				labelText: 'Mirror',
				labelFontSize: 14,
				labelFontStyle: 'italic',
				labelPositionX: -2,
				labelPositionY: -22,
				labelPositionZ: -98,
			},
/*
			{
				labelId: 903,
				labelType: 'image',
				labelMedia: {},
				labelFile:
				{
					fileId: 58,
					filePath: '/files/7f/7fde3bfcb6dd9ec01c1295fc5463bbe982d1abbc',
					fileType: 'image',
				},
				labelPositionX: -2,
				labelPositionY: -17,
				labelPositionZ: -98,
				labelScaleX: 0.2,
				labelScaleY: 0.2,
			},
*/
			],
			countdownSpritemap:						// Exactly one optional countdown spritemap can be present.
			{
				id: 1053,							// All spritemap animation objects should have unique ids.
				positionX: -2,
				positionY:  -17,
				positionZ: -98,
				spritemapTilesNum: 16,
				spritemapTilesHorizontal: 4,
				spritemapTilesVertical: 4,
				spritemapHeight: 992,
				spritemapWidth: 1164,
				spritemapDuration: 1500,			// TODO: If not specified, this should simply be the "duration" of the hotspot; divide this by the number of tiles to figure out how long to display each tile.
				spritemapRepeat: false,				// Don't repeat the animation continuously.
				spritemapShowFirstAsFixed: true,	// Use the first item in the sequence as a fixed label.
				spritemapTileSequence: [			// An ordered sequence of all the tiles we'll be playing in the animation, numbered left-to-right, top-to-bottom. To just have a static image, we could just have one tile listed in this, then the spritemap could be used, for example, for a character that moves around at different intervals.
					1, 2, 3, 4, 5, 6,
					7, 8, 9, 10, 11, 12,
					13, 14, 15, 16
				],
				media:								// The media handler should use device, transfer speed, and latency detection to determine if 
				{
					id: 5001,
					fileScaleX: 0.2,
					fileScaleY: 0.2,
					fileType: 'spritemap',
					fileIdLow: 73,
					fileIdMedium: 73,
					fileIdHigh: 73,
					fileIdUltra: [74, 75, 76, 77],	// If an array is specified instead of a number, this means the total image is spliced together from different files, in left-to-right, top-to-bottom order. TODO: We may need to specify how many images per row and column.
					files: [						// An array of all the possible files involved with this media item.
					{
						fileId: 73,
						filePath: '/files/c2/c20add16e1001b0f6675373833fbb542a826dbc1',
						fileType: 'spritemap',
						fileHeight: 750,
						fileWidth: 1192,
					},
					{
						fileId: 74,
						filePath: '/files/12/123450000',
						fileType: 'image',
					},
					{
						fileId: 75,
						filePath: '/files/12/123450001',
						fileType: 'image',
					},
					{
						fileId: 76,
						filePath: '/files/12/123450002',
						fileType: 'image',
					},
					{
						fileId: 77,
						filePath: '/files/12/123450003',
						fileType: 'image',
					}],
				},
			},
			actions: [
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '0',
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'audio',
				actionItemId: 63,
				actionMedia: {},
				actionFile:
				{
					fileId: 69,
					filePath: '/files/b5/b51f34a0d0d883063f7061c7b3550f261ee1653e',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionTextPosition: 'bottom center',
				actionDuration: 1000,
				actionSequence: 3,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: 'Schau mal in den Spiegel.',
				actionTextPosition: 'bottom center',
				actionDuration: 3000,
				actionSequence: 4,
				actionContinue: true,
			},
			{
				actionType: 'audio',					// German sentence
				actionItemId: 59,
				actionMedia: {},
				actionFile:
				{
					fileId: 70,
					filePath: '/files/b7/b7a3a393c1521dc47712531bb5ad958e61291475',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 5,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionTextPosition: 'bottom center',
				actionDuration: 1000,
				actionSequence: 6,
				actionContinue: false,
			},
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '1',
				actionSequence: 7,
				actionContinue: false,
			}],
		},
		{
			id: 6,
			positionX: 86,
			positionY: -47,
			positionZ: -19,
			radius: 40,
			duration: 1500,
			visible: 1,
			overridable: 1,
			blockOtherHotspots: true,
			conditions: [
			{
				conditionType: 'variable',
				conditionVariableName: 'allowHotspots',
				conditionVariableOperator: '=',
				conditionVariableOperand: '1',
			}],
			labels: [
			{
				labelId: 804,
				labelType: 'text',
				labelText: 'Pflanze',
				labelPositionX: 84.280,
				labelPositionY: -51.958,
				labelPositionZ: -10.396,
			},
			{
				labelId: 904,
				labelType: 'text',
				labelText: 'Plant',
				labelFontSize: 14,
				labelFontStyle: 'italic',
				labelPositionX: 83.617,
				labelPositionY: -57.243,
				labelPositionZ: -10.378,
			},
/*
			{
				labelId: 904,
				labelType: 'image',
				labelMedia: {},
				labelFile:
				{
					fileId: 58,
					filePath: '/files/7f/7fde3bfcb6dd9ec01c1295fc5463bbe982d1abbc',
					fileType: 'image',
				},
				labelPositionX: 86,
				labelPositionY: -47,
				labelPositionZ: -19,
				labelScaleX: 0.2,
				labelScaleY: 0.2,
			}
*/
			],
			countdownSpritemap:						// Exactly one optional countdown spritemap can be present.
			{
				id: 1054,							// All spritemap animation objects should have unique ids.
				positionX: 86,
				positionY: -47,
				positionZ: -19,
				spritemapTilesNum: 16,
				spritemapTilesHorizontal: 4,
				spritemapTilesVertical: 4,
				spritemapHeight: 992,
				spritemapWidth: 1164,
				spritemapDuration: 1500,			// TODO: If not specified, this should simply be the "duration" of the hotspot; divide this by the number of tiles to figure out how long to display each tile.
				spritemapRepeat: false,				// Don't repeat the animation continuously.
				spritemapShowFirstAsFixed: true,	// Use the first item in the sequence as a fixed label.
				spritemapTileSequence: [			// An ordered sequence of all the tiles we'll be playing in the animation, numbered left-to-right, top-to-bottom. To just have a static image, we could just have one tile listed in this, then the spritemap could be used, for example, for a character that moves around at different intervals.
					1, 2, 3, 4, 5, 6,
					7, 8, 9, 10, 11, 12,
					13, 14, 15, 16
				],
				media:								// The media handler should use device, transfer speed, and latency detection to determine if 
				{
					id: 5001,
					fileScaleX: 0.2,
					fileScaleY: 0.2,
					fileType: 'spritemap',
					fileIdLow: 73,
					fileIdMedium: 73,
					fileIdHigh: 73,
					fileIdUltra: [74, 75, 76, 77],	// If an array is specified instead of a number, this means the total image is spliced together from different files, in left-to-right, top-to-bottom order. TODO: We may need to specify how many images per row and column.
					files: [						// An array of all the possible files involved with this media item.
					{
						fileId: 73,
						filePath: '/files/c2/c20add16e1001b0f6675373833fbb542a826dbc1',
						fileType: 'spritemap',
						fileHeight: 750,
						fileWidth: 1192,
					},
					{
						fileId: 74,
						filePath: '/files/12/123450000',
						fileType: 'image',
					},
					{
						fileId: 75,
						filePath: '/files/12/123450001',
						fileType: 'image',
					},
					{
						fileId: 76,
						filePath: '/files/12/123450002',
						fileType: 'image',
					},
					{
						fileId: 77,
						filePath: '/files/12/123450003',
						fileType: 'image',
					}],
				},
			},
			actions: [
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '0',
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'audio',
				actionItemId: 63,
				actionMedia: {},
				actionFile:
				{
					fileId: 67,
					filePath: '/files/0a/0a88644fbaf82a2cb9de63c2a12eb6fb968dd9be',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionTextPosition: 'bottom center',
				actionDuration: 1000,
				actionSequence: 3,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: 'Ich gieße die Pflanze.',
				actionTextPosition: 'bottom center',
				actionDuration: 3000,
				actionSequence: 4,
				actionContinue: true,
			},
			{
				actionType: 'audio',					// German sentence
				actionItemId: 59,
				actionMedia: {},
				actionFile:
				{
					fileId: 68,
					filePath: '/files/29/291b34a866db0ea1420ea9a0c5760e19c4c4f3a8',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 5,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionTextPosition: 'bottom center',
				actionDuration: 1000,
				actionSequence: 6,
				actionContinue: false,
			},
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '1',
				actionSequence: 7,
				actionContinue: false,
			}],
		},
		{
			id: 7,
			positionX: -39,
			positionY: -16,
			positionZ: 90,
			radius: 30,
			duration: 1500,
			visible: 1,
			overridable: 1,
			blockOtherHotspots: true,
			conditions: [
			{
				conditionType: 'variable',
				conditionVariableName: 'allowHotspots',
				conditionVariableOperator: '=',
				conditionVariableOperand: '1',
			}],
			labels: [
			{
				labelId: 805,
				labelType: 'text',
				labelText: 'Gemälde',
				labelPositionX: -39,
				labelPositionY: -16,
				labelPositionZ: 90,
			},
			{
				labelId: 905,
				labelType: 'text',
				labelText: 'Painting',
				labelFontSize: 14,
				labelFontStyle: 'italic',
				labelPositionX: -39,
				labelPositionY: -21,
				labelPositionZ: 90,
			},
/*
			{
				labelId: 1005,
				labelType: 'image',
				labelMedia: {},
				labelFile:
				{
					fileId: 58,
					filePath: '/files/7f/7fde3bfcb6dd9ec01c1295fc5463bbe982d1abbc',
					fileType: 'image',
				},
				labelPositionX: -39,
				labelPositionY: -16,
				labelPositionZ: 90,
				labelScaleX: 0.2,
				labelScaleY: 0.2,
			}
*/
			],
			countdownSpritemap:						// Exactly one optional countdown spritemap can be present.
			{
				id: 1055,							// All spritemap animation objects should have unique ids.
				positionX: -39,
				positionY:  -16,
				positionZ: 90,
				spritemapTilesNum: 16,
				spritemapTilesHorizontal: 4,
				spritemapTilesVertical: 4,
				spritemapHeight: 992,
				spritemapWidth: 1164,
				spritemapDuration: 1500,			// TODO: If not specified, this should simply be the "duration" of the hotspot; divide this by the number of tiles to figure out how long to display each tile.
				spritemapRepeat: false,				// Don't repeat the animation continuously.
				spritemapShowFirstAsFixed: true,	// Use the first item in the sequence as a fixed label.
				spritemapTileSequence: [			// An ordered sequence of all the tiles we'll be playing in the animation, numbered left-to-right, top-to-bottom. To just have a static image, we could just have one tile listed in this, then the spritemap could be used, for example, for a character that moves around at different intervals.
					1, 2, 3, 4, 5, 6,
					7, 8, 9, 10, 11, 12,
					13, 14, 15, 16
				],
				media:								// The media handler should use device, transfer speed, and latency detection to determine if 
				{
					id: 5001,
					fileScaleX: 0.2,
					fileScaleY: 0.2,
					fileType: 'spritemap',
					fileIdLow: 73,
					fileIdMedium: 73,
					fileIdHigh: 73,
					fileIdUltra: [74, 75, 76, 77],	// If an array is specified instead of a number, this means the total image is spliced together from different files, in left-to-right, top-to-bottom order. TODO: We may need to specify how many images per row and column.
					files: [						// An array of all the possible files involved with this media item.
					{
						fileId: 73,
						filePath: '/files/c2/c20add16e1001b0f6675373833fbb542a826dbc1',
						fileType: 'spritemap',
						fileHeight: 750,
						fileWidth: 1192,
					},
					{
						fileId: 74,
						filePath: '/files/12/123450000',
						fileType: 'image',
					},
					{
						fileId: 75,
						filePath: '/files/12/123450001',
						fileType: 'image',
					},
					{
						fileId: 76,
						filePath: '/files/12/123450002',
						fileType: 'image',
					},
					{
						fileId: 77,
						filePath: '/files/12/123450003',
						fileType: 'image',
					}],
				},
			},
			actions: [
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '0',
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'audio',
				actionItemId: 65,
				actionMedia: {},
				actionFile:
				{
					fileId: 65,
					filePath: '/files/81/81b52d376ec35162df1f9bb3c3bafe2750a4fe50',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionTextPosition: 'bottom center',
				actionDuration: 1000,
				actionSequence: 3,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: 'Dieses Gemälde ist schön.',
				actionTextPosition: 'bottom center',
				actionDuration: 3000,
				actionSequence: 4,
				actionContinue: true,
			},
			{
				actionType: 'audio',					// German sentence
				actionItemId: 66,
				actionMedia: {},
				actionFile:
				{
					fileId: 66,
					filePath: '/files/27/277255403a64a7b4a8ad51f37ed39809a06089da',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 5,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionTextPosition: 'bottom center',
				actionDuration: 2000,
				actionSequence: 6,
				actionContinue: false,
			},
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '1',
				actionSequence: 7,
				actionContinue: false,
			}],
		}],
	},
	{
		id: 2,
		shortname: 'b945jaodmgio5mgh',
		projectid: 3,
		sequence: 3,
		parent: 0,
		title: 'Lake Louise',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: '75',
			filePath: '/files/64/64dfa3440cfea080b81f886dfaaf428cf528527a',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
		}],

		endEvents: [
		{
		}],

		hotspots: [
		{
			id: 8,
			positionX: 0,
			positionY: -100,
			positionZ: 0,
			radius: 20,
			duration: 3000,								// Milliseconds
			visible: 1,
			overridable: 0,
			blockOtherHotspots: false,					// If "true" then this hotspot is exclusive when activated. No other hotspots in the scene can be activated until this hotspot is activated again.
			conditions: [],
			countdownSpritemap:						// Exactly one optional countdown spritemap can be present.
			{
				id: 1060,							// All spritemap animation objects should have unique ids.
				positionX: 0,
				positionY: -100,
				positionZ: 0,
				spritemapTilesNum: 16,
				spritemapTilesHorizontal: 4,
				spritemapTilesVertical: 4,
				spritemapHeight: 992,
				spritemapWidth: 1164,
				spritemapDuration: 3000,			// TODO: If not specified, this should simply be the "duration" of the hotspot; divide this by the number of tiles to figure out how long to display each tile.
				spritemapRepeat: false,				// Don't repeat the animation continuously.
				spritemapShowFirstAsFixed: true,	// Use the first item in the sequence as a fixed label.
				spritemapTileSequence: [			// An ordered sequence of all the tiles we'll be playing in the animation, numbered left-to-right, top-to-bottom. To just have a static image, we could just have one tile listed in this, then the spritemap could be used, for example, for a character that moves around at different intervals.
					1, 2, 3, 4, 5, 6,
					7, 8, 9, 10, 11, 12,
					13, 14, 15, 16
				],
				media:								// The media handler should use device, transfer speed, and latency detection to determine if 
				{
					id: 5001,
					fileScaleX: 0.2,
					fileScaleY: 0.2,
					fileType: 'spritemap',
					fileIdLow: 73,
					fileIdMedium: 73,
					fileIdHigh: 73,
					fileIdUltra: [74, 75, 76, 77],	// If an array is specified instead of a number, this means the total image is spliced together from different files, in left-to-right, top-to-bottom order. TODO: We may need to specify how many images per row and column.
					files: [						// An array of all the possible files involved with this media item.
					{
						fileId: 73,
						filePath: '/files/c2/c20add16e1001b0f6675373833fbb542a826dbc1',
						fileType: 'spritemap',
						fileHeight: 750,
						fileWidth: 1192,
					},
					{
						fileId: 74,
						filePath: '/files/12/123450000',
						fileType: 'image',
					},
					{
						fileId: 75,
						filePath: '/files/12/123450001',
						fileType: 'image',
					},
					{
						fileId: 76,
						filePath: '/files/12/123450002',
						fileType: 'image',
					},
					{
						fileId: 77,
						filePath: '/files/12/123450003',
						fileType: 'image',
					}],
				},
			},
			actions: [
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '1',
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'jump',
				actionItemId: 3,
				actionSequence: 2,
			}],
		},
		{
			id: 13,
			positionX: -88.744,
			positionY: -43.125,
			positionZ: 13.679,
			radius: 30,
			duration: 1500,
			visible: 1,
			overridable: 1,
			blockOtherHotspots: true,
			conditions: [
			{
				conditionType: 'variable',
				conditionVariableName: 'allowHotspots',
				conditionVariableOperator: '=',
				conditionVariableOperand: '1',
			}],
			labels: [
			{
				labelId: 810,
				labelType: 'text',
				labelText: 'See',
				labelPositionX: -88.744,
				labelPositionY: -43.125,
				labelPositionZ: 13.679,
			},
			{
				labelId: 910,
				labelType: 'text',
				labelText: 'Lake',
				labelFontSize: 14,
				labelFontStyle: 'italic',
				labelPositionX: -84.212,
				labelPositionY: -48.646,
				labelPositionZ: 12.981,
			},
			{
				labelId: 1010,
				labelType: 'image',
				labelMedia: {},
				labelFile:
				{
					fileId: 58,
					filePath: '/files/7f/7fde3bfcb6dd9ec01c1295fc5463bbe982d1abbc',
					fileType: 'image',
				},
				labelPositionX: -88.744,
				labelPositionY: -43.125,
				labelPositionZ: 13.679,
				labelScaleX: 0.2,
				labelScaleY: 0.2,
			}],
			actions: [
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '0',
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'audio',
				actionItemId: 63,
				actionMedia: {},
				actionFile:
				{
					fileId: 76,
					filePath: '/files/f4/f4c66ea9a7ee8d8ed233409949a3780c2025df17',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionTextPosition: 'bottom center',
				actionDuration: 1000,
				actionSequence: 3,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: 'Der See ist klar und blau.',
				actionTextPosition: 'bottom center',
				actionDuration: 3000,
				actionSequence: 4,
				actionContinue: true,
			},
			{
				actionType: 'audio',					// German sentence
				actionItemId: 59,
				actionMedia: {},
				actionFile:
				{
					fileId: 77,
					filePath: '/files/7a/7aa6f27495a809dd1ba23b871ec93284ede61762',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 5,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionTextPosition: 'bottom center',
				actionDuration: 1000,
				actionSequence: 6,
				actionContinue: false,
			},
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '1',
				actionSequence: 7,
				actionContinue: false,
			}],
		},
		{
			id: 15,
			positionX: 16.279,
			positionY: -8.215,
			positionZ: 97.993,
			radius: 20,
			duration: 1000,
			visible: 1,
			overridable: 1,
			blockOtherHotspots: true,
			conditions: [
			{
				conditionType: 'variable',
				conditionVariableName: 'allowHotspots',
				conditionVariableOperator: '=',
				conditionVariableOperand: '1',
			}],
			labels: [
			{
				labelId: 811,
				labelType: 'text',
				labelText: 'Rucksack',
				labelPositionX: 16.279,
				labelPositionY: -8.215,
				labelPositionZ: 97.993,
			},
			{
				labelId: 911,
				labelType: 'text',
				labelText: 'Backpack',
				labelFontSize: 14,
				labelFontStyle: 'italic',
				labelPositionX: 15.858,
				labelPositionY: -13.369,
				labelPositionZ: 97.427,
			},
			{
				labelId: 1011,
				labelType: 'image',
				labelMedia: {},
				labelFile:
				{
					fileId: 58,
					filePath: '/files/7f/7fde3bfcb6dd9ec01c1295fc5463bbe982d1abbc',
					fileType: 'image',
				},
				labelPositionX: 16.279,
				labelPositionY: -8.215,
				labelPositionZ: 97.993,
				labelScaleX: 0.2,
				labelScaleY: 0.2,
			}],
			actions: [
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '0',
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'audio',
				actionItemId: 63,
				actionMedia: {},
				actionFile:
				{
					fileId: 78,
					filePath: '/files/1e/1eb582b815c523b9840fc61290985f4432dfe0be',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionTextPosition: 'bottom center',
				actionDuration: 1000,
				actionSequence: 3,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: 'Ich halte meine Kamera im Rucksack.',
				actionTextPosition: 'bottom center',
				actionDuration: 3000,
				actionSequence: 4,
				actionContinue: true,
			},
			{
				actionType: 'audio',					// German sentence
				actionItemId: 59,
				actionMedia: {},
				actionFile:
				{
					fileId: 79,
					filePath: '/files/f2/f2a360c8ba1319ea9a09f3c78c801ee86dc93367',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 5,
				actionContinue: false,
			},
			{
				actionType: 'text',
				actionText: '',
				actionTextPosition: 'bottom center',
				actionDuration: 1000,
				actionSequence: 6,
				actionContinue: false,
			},
			{
				actionType: 'variable',
				actionVariableName: 'allowHotspots',
				actionVariableOperator: 'set',
				actionVariableOperand: '1',
				actionSequence: 7,
				actionContinue: false,
			}],
		}],
	},
	{
		id: 3,
		shortname: 'b945jaodmgio5mgg',
		projectid: 3,
		sequence: 2,
		parent: 0,
		title: 'Vespucci Wohnzimmer',
		subtitle: '',
		description: '',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: '56',
			filePath: '/files/fe/feda8568a0646ae58769bba32dc3efb52ccd5aab',
			fileType: 'image',
		},
		embedid: 0,
		createdby: 1,
		lockedby: 0,
		lockedtime: 0,
		timecreated: 0,
		timemodified: 0,

		startEvents: [
		{
		}],

		endEvents: [
		{
		}],

		hotspots: [
		{
			id: 18,
			positionX: 0,
			positionY: -100,
			positionZ: 0,
			radius: 20,
			duration: 3000,								// Milliseconds
			visible: 1,
			overridable: 0,
			blockOtherHotspots: false,					// If "true" then this hotspot is exclusive when activated. No other hotspots in the scene can be activated until this hotspot is activated again.
			conditions: [],
			countdownSpritemap:						// Exactly one optional countdown spritemap can be present.
			{
				id: 1070,							// All spritemap animation objects should have unique ids.
				positionX: 0,
				positionY: -100,
				positionZ: 0,
				spritemapTilesNum: 16,
				spritemapTilesHorizontal: 4,
				spritemapTilesVertical: 4,
				spritemapHeight: 992,
				spritemapWidth: 1164,
				spritemapDuration: 1500,			// TODO: If not specified, this should simply be the "duration" of the hotspot; divide this by the number of tiles to figure out how long to display each tile.
				spritemapRepeat: false,				// Don't repeat the animation continuously.
				spritemapShowFirstAsFixed: true,	// Use the first item in the sequence as a fixed label.
				spritemapTileSequence: [			// An ordered sequence of all the tiles we'll be playing in the animation, numbered left-to-right, top-to-bottom. To just have a static image, we could just have one tile listed in this, then the spritemap could be used, for example, for a character that moves around at different intervals.
					1, 2, 3, 4, 5, 6,
					7, 8, 9, 10, 11, 12,
					13, 14, 15, 16
				],
				media:								// The media handler should use device, transfer speed, and latency detection to determine if 
				{
					id: 5009,
					fileScaleX: 0.2,
					fileScaleY: 0.2,
					fileType: 'spritemap',
					fileIdLow: 73,
					fileIdMedium: 73,
					fileIdHigh: 73,
					fileIdUltra: [74, 75, 76, 77],	// If an array is specified instead of a number, this means the total image is spliced together from different files, in left-to-right, top-to-bottom order. TODO: We may need to specify how many images per row and column.
					files: [						// An array of all the possible files involved with this media item.
					{
						fileId: 73,
						filePath: '/files/c2/c20add16e1001b0f6675373833fbb542a826dbc1',
						fileType: 'spritemap',
						fileHeight: 750,
						fileWidth: 1192,
					},
					{
						fileId: 74,
						filePath: '/files/12/123450000',
						fileType: 'image',
					},
					{
						fileId: 75,
						filePath: '/files/12/123450001',
						fileType: 'image',
					},
					{
						fileId: 76,
						filePath: '/files/12/123450002',
						fileType: 'image',
					},
					{
						fileId: 77,
						filePath: '/files/12/123450003',
						fileType: 'image',
					}],
				},
			},
			actions: [
			{
				actionType: 'jump',
				actionItemId: 1,
				actionSequence: 1,
			}],
		},
		{
			id: 19,
			positionX: 0.937,
			positionY: -0.126,
			positionZ: 0.326,
			radius: 0.25,
			duration: 1500,
			labels: [
			{
				labelType: 'text',
				labelText: 'Schlafzimmer',
				labelPositionX: 120,
				labelPositionY: -25,
				labelPositionZ: 75,
			}],
			visible: 1,
			overridable: 1,
			blockOtherHotspots: true,
			actions: [
			{
				actionType: 'changeLabel',
				actionText: 'Bedroom',
				actionDuration: 0,
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'audio',
				actionItemId: 9,
				actionMedia: {},
				actionFile:
				{
					fileId: 9,
					filePath: '',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'changeLabel',
				actionText: 'Schlafzimmer',
				actionDuration: 0,
				actionSequence: 3,
				actionContinue: false,
			}],
		},
		{
			id: 20,
			positionX: 0.543,
			positionY: -0.124,
			positionZ: -0.830,
			radius: 0.25,
			duration: 1500,
			labels: [
			{
				labelType: 'text',
				labelText: 'Badezimmer',
				labelPositionX: 90,
				labelPositionY: -25,
				labelPositionZ: -90,
			}],
			visible: 1,
			overridable: 1,
			blockOtherHotspots: true,
			actions: [
			{
				actionType: 'changeLabel',
				actionText: 'Bathroom',
				actionDuration: 0,
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'audio',
				actionItemId: 9,
				actionMedia: {},
				actionFile:
				{
					fileId: 9,
					filePath: '',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'changeLabel',
				actionText: 'Badezimmer',
				actionDuration: 0,
				actionSequence: 3,
				actionContinue: false,
			}],
		},
		{
			id: 21,
			positionX: 0.909,
			positionY: -0.144,
			positionZ: -0.390,
			radius: 0.25,
			duration: 1500,
			labels: [
			{
				labelType: 'text',
				labelText: 'Küche',
				labelPositionX: 150,
				labelPositionY: -25,
				labelPositionZ: -30,
			}],
			visible: 1,
			overridable: 1,
			blockOtherHotspots: true,
			actions: [
			{
				actionType: 'changeLabel',
				actionText: 'Kitchen',
				actionDuration: 0,
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'audio',
				actionItemId: 9,
				actionMedia: {},
				actionFile:
				{
					fileId: 9,
					filePath: '',
					fileType: 'audio',
				},
				actionDuration: 0,
				actionRepeat: false,
				actionSequence: 2,
				actionContinue: false,
			},
			{
				actionType: 'changeLabel',
				actionText: 'Küche',
				actionDuration: 0,
				actionSequence: 3,
				actionContinue: false,
			}],
		}],
	},
],
};
