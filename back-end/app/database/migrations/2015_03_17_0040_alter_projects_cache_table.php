<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProjectsCacheTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('projects_cache', function(Blueprint $table)
		{
			$table->dropColumn('id');
			$table->integer('projectId');

			// Indexes.
			$table->primary('projectId');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('projects_cache', function(Blueprint $table)
		{
			//
		});
	}

}
