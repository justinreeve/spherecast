<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('actions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('eventId');

			// The type can be image, spritemap, audio, video, text, jump, variable, url, changeLabel, or cancel (to mark another action as complete)
			$table->string('type');

			$table->integer('sequence')->default(1);
			$table->boolean('continue')->default(false);	// If false, wait until this action is complete before continuing the sequence.
			$table->boolean('repeat')->default(false);
			$table->integer('mediaId')->nullable();
			$table->text('params')->nullable();				// Serialized parameters.

			// Indexes.
			$table->index('eventId');
			$table->index('type');
			$table->index('mediaId');

			// Foreign keys.
			$table->foreign('eventId')
				->references('id')->on('sceneevents')
				->onDelete('cascade')
				->onUpdate('cascade');

			// If the media item is deleted, the action won't work, but we want to set mediaId as null, so if
			// the type is still image, audio, or video, we can pop up a warning saying that the action item
			// needs to be fixed.
			$table->foreign('mediaId')
				->references('id')->on('media')
				->onDelete('set null')
				->onUpdate('cascade');

			// created_at, updated_at DATETIME
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('actions', function(Blueprint $table)
		{
		});
	}

}
