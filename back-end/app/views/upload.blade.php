{{ Form::open(['url' => '/media', 'files' => true]) }}
{{ Form::text('projectId', '', array('id' => 'projectId', 'placeholder' => 'Project ID')) }}
{{ Form::file('file', '', array('id' => 'file')) }}
{{ Form::submit('Upload') }}
{{ Form::close() }}