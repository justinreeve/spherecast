@include('docs.docsheader')
<h1>Scenes</h1>

<div class="attributes">
	<dl>
		<dt class="integer">id</dt>
		<dd>scene id</dd>

		<dt class="string">shortname</dt>
		<dd>unique code identifying the scene</dd>

		<dt class="integer"><a href="/api/docs/project">projectId</a></dt>
		<dd>project id the scene is associated with</dd>

		<dt class="integer">sequence</dt>
		<dd>the order the scene appears in the management tool</dd>

		<dt class="integer unused" data-default="0">parent</dt>
		<dd>associates scene with a "parent" scene</dd>

		<dt class="string">subtitle</dt>
		<dd>subtitle of scene</dd>

		<dt class="string">description</dt>
		<dd>text summary of project</dd>

		<dt class="string" data-default="spherecast">viewType</dt>
		<dd>
			the type of scene being viewed, which determines the renderer that processes the scene
			<dl class="options">
				<dt>spherecast</dt>
				<dd>the scene is an equirectangular image or video, and is rendered using the main Spherecast tool</dd>

				<dt>googlestreetview</dt>
				<dd>the scene is a Google Street View, and is rendered in Google's framework with head-tracking added</dd>

				<dt>3d</dt>
				<dd>the scene is a 3-dimensional workspace, in which 3d objects can be placed</dd>
			</dl>
		</dd>

		<dt class="integer">createdBy</dt>
		<dd>user id of project creator</dd>

		<dt class="integer" data-default="0">lockedBy</dt>
		<dd>user id of the person who has a lock on the scene, making it unable to be edited by others</dd>

		<dt class="integer" data-default="0">showGroundPanel</dt>
		<dd>show a default navigation image at the bottom of the scene which allows moving between scenes based on <a href="#">sequence</a></dd>

		<dt class="boolean" data-default="false">introScene</dt>
		<dd>indicates if this designated as an introductory scene for the project, which appears before the main project runs</dd>

		<dt class="decimal" data-default="null">initialOrientation</dt>
		<dd>a radian trajectory for the initial direction the user always faces when the scene first loads; this overrides any setting in <a href="#">northOrientation</a>, if <a href="#">useNorth</a> is set to true.</dd>

		<dt class="decimal" data-default="null">northOrientation</dt>
		<dd>a radian trajectory for the "north" direction of the scene; this is used if <a href="#">useNorthOrientation</a> is set to true</dd>

		<dt class="boolean" data-default="true">useNorth</dt>
		<dd>indicates whether to use the <a href="#">northOrientation</a> trajectory for the scene</dd>

		<dt class="object" data-default="null"><a href="/api/docs/media">backgroundMedia</a></dt>
		<dd>a media object describing the background image or video for the scene</dd>

		<dt class="array" data-default="null">params</dt>
		<dd>a list of arbitrary parameters that may further define the scene</dd>

		<dt class="array"><a href="/api/docs/events">startEvents</a></dt>
		<dd>a list of events which describe actions that occur when the scene first loads</dd>

		<dt class="array"><a href="/api/docs/events">endEvents</a></dt>
		<dd>a list of events which describe actions that occur immediately before a jump action to another scene</dd>

		<dt class="array"><a href="/api/docs/events">voiceEvents</a></dt>
		<dd>a list of events which describe actions that occur in response to voice commands via the Chrome Web Speech API</dd>

		<dt class="array"><a href="/api/docs/events">hotspots</a></dt>
		<dd>a list of events which describe actions that occur when triggered by a hotspot placed at a specific location on the scene</dd>
	</dl>
</div>

<div class="example">
	<pre>
{
	"id": "46",
	"shortname": "ho5o5iok",
	"projectId": "7",
	"sequence": "0",
	"parent": "0",
	"title": "Factory 3",
	"subtitle": "",
	"description": "",
	"viewType": "spherecast",
	"createdBy": "0",
	"lockedBy": "0",
	"showGroundPanel": "0",
	"introScene": "0",
	"initialOrientation": null,
	"northOrientation": 0.35,
	"useNorth": true,
	"created_at": "-0001-11-30 00:00:00",
	"updated_at": "-0001-11-30 00:00:00",
	"backgroundMedia": {...}
	"params": [{...}],
	"startEvents": [{...}],
	"endEvents": [{...}],
	"voiceEvents": [{...}],
	"hotspots": [{...}],
}
	</pre>
</div>
@include('docs.docsfooter')