spherecastApp.controller('SceneController', function($scope, $http, $routeParams, Media)
{
	$scope.scene = {};
    $scope.mediaData = {};

     // loading variable to show the spinning loading icon
    $scope.loading = true;

    Media.get().success(function(data)
    {
    	$scope.media = data;
    	console.log($scope.media);
    });
});