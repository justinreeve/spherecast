<?php

class ActionController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// Get all the floorplan media.
		$backgrounds = Media::where('filename', 'LIKE', 'story-the-little-red-hen%.jpg')
							->orderBy('filename')
							->get();
		foreach ($backgrounds as $bgindex => $bg)
		{
			echo $bgindex . ' ' . $bg->filename . ' ' . $bg->id . '<br>';
/*
			Scene::create(array(
				'shortname' => $this->random(),
				'projectId' => 8,
				'title' => $bg->filename,
				'backgroundMedia' => $bg->id,
			));
*/
			// If there's a previous scene, add a jump to the current scene.
			
			// Get the audio and add a start event and action to play it.
		}

/*
		foreach ($floorplans as $media)
		{
			if ($media->id < 500)
				continue;

			echo $media->id . ' ';

			$sceneId = trim(str_replace('.png', '', substr($media->filename, 10)));
//			echo '<pre>'; print_r($fp); echo '</pre>';

			$duration = 1500;

			$event = SceneEvent::create(array(
				'shortname' => $this->random(8),
				'sceneId' => $sceneId,
				'type' => 'hotspot',
			));

			$params = new stdClass;
			$params->spritemapTilesNum = 18;
			$params->spritemapTilesHorizontal = 6;
			$params->spritemapTilesVertical = 3;
			$params->spritemapHeight = 432;
			$params->spritemapWidth = 216;
			$params->spritemapDuration = $duration;
			$params->spritemapRepeat = false;
			$params->spritemapShowFirstAsFixed = true;
			$params->spritemapTileSequence = array(
				1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
			);
	
			Hotspot::create(array(
				'eventId' => $event->id,
				'positionX' => 0,
				'positionY' => -99.999,
				'positionZ' => 0,
				'radius' => 20.000,
				'duration' => $duration,
				'countdownMedia' => 257,
				'countdownMediaParams' => serialize($params),
			));

			$params = new stdClass;
			$params->disappearOnCountdown = false;
			$params->opacity = 0.75;
			
			Label::create(array(
				'eventId' => $event->id,
				'mediaId' => 286,
				'type' => 'image',
				'positionX' => 0,
				'positionY' => -99.999,
				'positionZ' => 0,
				'scaleX' => '0.5',
				'scaleY' => '0.5',
				'params' => serialize($params),
			));

			$params = new stdClass;
			$params->placement = 'overlay';
			$params->positionX = 0;
			$params->positionY = -100;
			$params->positionZ = 0;
			$params->alignment = 'bottom center';
			$params->scaleX = 0.5;
			$params->scaleY = 0.5;
			$params->duration = 8000;

			Action::create(array(
				'eventId' => $event->id,
				'type' => 'image',
				'sequence' => 1,
				'continue' => 0,
				'mediaId' => $media->id,
				'params' => serialize($params),
			));
		}
*/
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	}


	protected function random($char = 8)
	{
		$random = "";
		srand((double) microtime() * 1000000);
		$data = "0123456789abcdefghijklmnopqrstuvwxyz";

		// Create a shortname, and ensure it has alphabetic characters and isn't already being used.
		do
		{
			for ($i = 0; $i < $char; $i++)
			{
				$random .= substr($data, (rand() % (strlen($data))), 1);
			}
		}
		while (is_numeric($random) || SceneEvent::where('shortname', $random)->first());
		return $random;
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
