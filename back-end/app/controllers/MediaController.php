<?php
// Created with php artisan controller:make MediaController.
class MediaController extends BaseController
{
	/**
	 * Save an uploaded file, and return the results.
	 */
	protected function saveFile($file, $filepath = '/', $projectId, $userId = 0, $overwrite = false)
	{
		// TODO: Add validation.
//		$rules = array();
		if ($file->isValid())
		{
			// Make sure the filename and filepath aren't the same as an existing record.
			// If they are, don't do anything.
			$mediaItem = Media::where('filepath', $filepath)
								->where('filename', $file->getClientOriginalName())
								->get()
								->first();
			if ($mediaItem)
				return Response::json(array('success' => false, 'error' => 'Filename already exists.'));

			$type = null;
			if ($this->isAnimation($file))	// Check this first before seeing if it's a different kind of image.
			{
				// TODO: Check if we have an animated GIF and produce a spritemap PNG.
				$type = 'spritemap';
			}
			else if ($this->isBackgroundImage($file))
				$type = 'backgroundimage';
			else if ($this->isNormalImage($file))
				$type = 'image';
			else if ($this->isAudio($file))
				$type = 'audio';
			else if ($this->isBackgroundVideo($file))
				$type = 'backgroundvideo';
			else if ($this->isNormalVideo($file))
				$type = 'video';

			if ($type != null)
			{
				$media = Media::create(array(
					'projectId' => $projectId,
					'type' => $type,
					'filename' => $file->getClientOriginalName(),
					'filepath' => $filepath,
					'createdBy' => $userId,
				));

				if ($media->id)
				{
					$contenthash = sha1_file($file->getRealPath());
					if (strlen($contenthash) >= 16)
					{
						$filedir = \Config::get('media.content_dir') . substr($contenthash, 0, 2);
						$filepath = '/files/' . substr($contenthash, 0, 2) . '/' . $contenthash;
						$filename = $contenthash;

						// TODO: Get width, height, and length.

						// If the file already exists and is the same size, this file has been uploaded
						// before, and we can just retrieve the file id from the record and link it to
						// the media.
						// TODO: In the unlikely event the hash is the same but the size isn't, add an
						// underscore at the end of the filename.
						$fileItem = FileItem::where('contenthash', $contenthash)->get()->first();
						if ($fileItem)
						{
							$fileId = $fileItem->getId();

							// TODO: Make sure the file exists.
							if (!file_exists($filedir . '/' . $filename))
							{
								if (!$file->move($filedir, $filename))
								{
									return Response::json(array('success' => false, 'error' => 'Could not save existing file.'));
								}
							}
						}
						else
						{
							$fileItem = FileItem::create(array(
								'contenthash' => $contenthash,
								'filepath' => $filepath,
								'title' => $file->getClientOriginalName(),
								'filename' => $file->getClientOriginalName(),
								'mimetype' => $file->getMimeType(),
								'filesize' => $file->getSize(),
								'extension' => $file->getClientOriginalExtension(),
							));
							$fileId = $fileItem->id;

							// Move the file.
							if (!$file->move($filedir, $filename))
							{
								return Response::json(array('success' => false, 'error' => 'Could not save new file.'));
							}
						}

						if (isset($fileId))
						{
							$mediafile = MediaFile::create(array(
								'mediaId' => $media->id,
								'fileId' => $fileId,
								'filename' => $file->getClientOriginalName(),
								'relativeSize' => 'all',
								'sequence' => 0,
							));

							if ($mediafile)
							{
							}
						}

						return Response::json(array('success' => true));
					}
				}
				else
					return Response::json(array('success' => false, 'error' => 'Could not create media record.'));
			}
		}
		else
			return Response::json(array('success' => false, 'error' => 'Uploaded file is not valid.'));
	}

	protected function resizeAndPad($file)
	{
		// If the width is more than 3000px, resize it to a width of 3000px.
		$size = getimagesize($target_file . '.jpg');
		if ($size[0] > 3000 || filesize($target_file . '.jpg') > 1000000)
		{
			$ratio = $size[0] / $size[1]; // width/height
			if ($ratio > 1)
			{
				$width = 3000;
				$height = 3000 / $ratio;
			}
			else
			{
				$width = 3000 * $ratio;
				$height = 3000;
			}
	
			$cmd = 'convert -limit memory 128MiB -limit map 128MiB "' . $target_file . '.jpg" -resize ' . $width . 'x' . $height . ' "' . $target_file . '_resize.jpg"';
			exec($cmd);
			$target_file .= '_resize';
		}
	
		if (!file_exists($target_file . '.jpg'))
		{
			$errors[] = 'Could not resize file.';
		}
		else
		{
			// Make sure the height is padded to width / 2.
			$size = getimagesize($target_file . '.jpg');
			if ($size[1] != $size[0] / 2)
			{
				$newWidth = $size[0];
				$newHeight = $size[0] / 2;
				$img = getimagesize($target_file . '.jpg');
				$width = $size[0];
				$height = $size[1];
	
				// Create the blank canvas.
				$cmd = 'convert -limit memory 128MiB -limit map 128MiB -size ' . $newWidth . 'x' . $newHeight . ' xc:black "' . $target_file . '_canvas.jpg"';
				exec($cmd);
	
				// Merge the image and canvas.
				$cmd = 'composite -gravity center "' . $target_file . '.jpg" "' . $target_file . '_canvas.jpg" "' . $target_file . '_padded.jpg"';
				exec($cmd);
				unlink($target_file . '_canvas.jpg');
				$target_file .= '_padded';
			}
	
			if (!file_exists($target_file . '.jpg'))
			{
				$errors[] = 'Could not pad file.';
			}
			else
			{
				// Add the record to the database.
				// TODO: Check that the code is actually random.
				$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
				$code = '';
				for ($i = 0; $i < 5; $i++)
				{
					$code .= $characters[rand(0, strlen($characters) - 1)];
				}
	
				$sql = "INSERT INTO photospheres (code, filepath, timeuploaded, ip)
						VALUES (:code, :filepath, :timeuploaded, :ip)";
				$stmt = $db->prepare($sql);
				$stmt->execute(array('code' => $code, 'filepath' => '/' . $target_file . '.jpg', 'timeuploaded' => time(), 'ip' => $_SERVER['REMOTE_ADDR']));
				if ($stmt->rowCount() > 0)
				{
					// Insert was successful. Redirect.
					header('location: http://sphcst.com/' . $code);
				}
			}
		}
	}

	/**
	 * For starters, we should only use animated GIFs as spritemaps.
	 */
	protected function isAnimation($file)
	{
		return false;
	}

	/**
	 * The process for detecting if we're using a background image is to check that it's a 2:1 aspect ratio
	 * (or reasonably close), and to check the resolution (at least 2000x1000).
	 */
	protected function isBackgroundImage($file)
	{
		// Check the mimetype.
		if (MediaType::isImage($file->getMimeType()))
		{
			// Check if we have a 2:1 aspect ratio, and get the width.
			$cmd = 'convert ' . escapeshellarg($file->getRealPath()) . ' -format "%[fx:w/h] %w" info:';
			$output = shell_exec($cmd);
			if ($output != '')
			{
				$width = 0;
				$arr = explode(' ', $output);
				if (isset($arr[0]) && is_numeric(trim($arr[0])))
					$aspect_ratio = trim($arr[0]);
				if (isset($arr[1]) && is_numeric(trim($arr[1])))
					$width = trim($arr[1]);

				if (isset($aspect_ratio) && isset($width) && $aspect_ratio >= 1.95 && $aspect_ratio <= 2.05 && $width >= 2000)
					return true;
			}
		}
		return false;
	}

	/**
	 * Detect if we have an image. If we need to detect a background image, we should call that before this method.
	 */
	protected function isNormalImage($file)
	{
		// Check the mimetype.
		return MediaType::isImage($file->getMimeType());
	}

	/**
	 * The process for detecting if we're using a background video is to check that it's a 2:1 aspect ratio
	 * (or reasonably close), and to check the resolution (at least 2000x1000).
	 */
	protected function isBackgroundVideo($file)
	{
		// Check the mimetype.
		if (MediaType::isVideo($file->getMimeType()))
		{
			// Check if we have a 2:1 aspect ratio, and get the width.
			$cmd = 'convert ' . escapeshellarg($file->getRealPath()) . ' -format "%[fx:w/h] %w" info:';
			$output = shell_exec($cmd);
			if ($output != '')
			{
				$arr = explode(' ', $output);
				if (isset($arr[0]) && is_numeric($arr[0]))
					$aspect_ratio = $arr[0];
				if (isset($arr[1]) && is_numeric($arr[1]))
					$width = $arr[1];

				if (isset($aspect_ratio) && isset($width) && $aspect_ratio >= 1.95 && $aspect_ratio <= 2.05 && $width >= 2000)
					return true;
			}
		}
		return false;
	}

	/**
	 * Detect if we have a video. If we need to detect a background video, we should call that before this method.
	 */
	protected function isNormalVideo($file)
	{
		// Check the mimetype.
		return MediaType::isVideo($file->getMimeType());
	}

	/**
	 * Detect if we have an audio file.
	 */
	protected function isAudio($file)
	{
		// Check the mimetype.
		return MediaType::isAudio($file->getMimeType());
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Response::json(Media::get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage. If a mediaId
	 * Required: filename, filepath, filedata
	 * Optional: type, mediaId, createdBy
	 *
	 * @return Response
	 */
	public function store()
	{
		// TODO: We want to check if we have multiple files or single files, because both should
		// be able to be uploaded through this controller.
		$file = Input::file('file');
		if ($file->isValid())
		{
			$projectId = Input::get('projectId');
			if ($projectId)
				$this->saveFile($file, '/', $projectId);
		}
/*
		Project::create(array(
			'shortname' => $random,
			'title' => Input::get('title'),
			'subtitle' => '',
			'description' => Input::get('description'),
			'createdBy' => 0,
			'startScene' => 0,
		));
*/
		return Response::json(array('success' => true));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param mixed $projectId
	 * @param string $path
	 * @return Response
	 */
	public function show($projectId, $path)
	{
		$path_parts = pathinfo($path);

		if (is_numeric($projectId))
		{
			$media = DB::table('media')
							->join('media_files', 'media.id', '=', 'media_files.mediaId')
							->join('files', 'media_files.fileId', '=', 'files.id')
							->select('media.*', 'files.mimetype', 'files.extension', 'files.filepath AS rawfilepath')
							->where('media.projectId', $projectId)
							->where('media.filepath', ('/' . $path_parts['dirname']))
							->where('media.filename', $path_parts['basename'])
							->first();
		}
		else
		{
			$media = DB::table('media')
							->join('projects', 'media.projectId', '=', 'projects.id')
							->join('media_files', 'media.id', '=', 'media_files.mediaId')
							->join('files', 'media_files.fileId', '=', 'files.id')
							->select('media.*', 'files.mimetype', 'files.extension', 'files.filepath AS rawfilepath')
							->where('media.filepath', ('/' . $path_parts['dirname']))
							->where('media.filename', $path_parts['basename'])
							->first();
		}

		if ($media)
		{
			$filepath = str_replace('/files/', '', Config::get('media.content_dir')) . $media->rawfilepath;
			if (file_exists($filepath))
			{
				$headers = array('Content-Type: ' . $media->mimetype);
				return Response::download($filepath, $media->filename, $headers);
			}
		}
	}


	/**
	 * Display a thumbnail (JPEG) of the specified resource, creating it if necessary.
	 *
	 * @param mixed $projectId
	 * @param string $path
	 * @param string $size Can be 50 (xs), 80 (sm), 120 (md), 160 (lg), 320 (xl); we might also want to allow heights to be specified (e.g. 160x120).
	 * @param int $mediaId
	 * @return Response
	 */
	public function showThumbnail($projectId, $path = null, $size = null, $mediaId = null)
	{
		// TODO: Figure out why we can't properly use $mediaId as a parameter in the routes. It keeps coming up as $path.

		// If neither $path or $mediaId was specified, we can't do anything.
//		if ($path == null && $mediaId == null)
//			return null;

		if (is_numeric($path))
			$mediaId = $path;

		// If $mediaId is specified, retrieve the media object through that. Otherwise, use the path.
		if ($mediaId != null)
		{
			if (is_numeric($projectId))
			{
				$media = DB::table('media')
								->join('media_files', 'media.id', '=', 'media_files.mediaId')
								->join('files', 'media_files.fileId', '=', 'files.id')
								->select('media.*', 'files.mimetype', 'files.extension', 'files.filepath AS rawfilepath')
								->where('media.id', $mediaId)
								->where('media.projectId', $projectId)
								->first();
			}
			else
			{
				$media = DB::table('media')
								->join('projects', 'media.projectId', '=', 'projects.id')
								->join('media_files', 'media.id', '=', 'media_files.mediaId')
								->join('files', 'media_files.fileId', '=', 'files.id')
								->select('media.*', 'files.mimetype', 'files.extension', 'files.filepath AS rawfilepath')
								->where('media.id', $mediaId)
								->first();
			}
		}
		else
		{
			// Get the media object from the path.
			$path_parts = pathinfo($path);
	
			if (is_numeric($projectId))
			{
				$media = DB::table('media')
								->join('media_files', 'media.id', '=', 'media_files.mediaId')
								->join('files', 'media_files.fileId', '=', 'files.id')
								->select('media.*', 'files.mimetype', 'files.extension', 'files.filepath AS rawfilepath')
								->where('media.projectId', $projectId)
								->where('media.filepath', ('/' . $path_parts['dirname']))
								->where('media.filename', $path_parts['basename'])
								->first();
			}
			else
			{
				$media = DB::table('media')
								->join('projects', 'media.projectId', '=', 'projects.id')
								->join('media_files', 'media.id', '=', 'media_files.mediaId')
								->join('files', 'media_files.fileId', '=', 'files.id')
								->select('media.*', 'files.mimetype', 'files.extension', 'files.filepath AS rawfilepath')
								->where('media.filepath', ('/' . $path_parts['dirname']))
								->where('media.filename', $path_parts['basename'])
								->first();
			}
		}

		if ($media)
		{
			// Determine the width and height from $size.
			if ($size == 50 || $size == 'xs')
			{
				$width = 50;
				$height = 50;
			}
			else if ($size == 80 || $size == 'sm')
			{
				$width = 80;
				$height = 60;
			}
			else if ($size == 120 || $size == 'md')
			{
				$width = 120;
				$height = 80;
			}
			else if ($size == 160 || $size == 'lg')
			{
				$width = 160;
				$height = 120;
			}
			else if ($size == 320 || $size == 'xl')
			{
				$width = 320;
				$height = 240;
			}
			else	// Default to large size.
			{
				$width = 160;
				$height = 120;
			}

			if (isset($width) && isset($height))
			{
				$thumb = DB::table('media_thumbnails')
								->join('files', 'media_thumbnails.fileId', '=', 'files.id')
								->select('media_thumbnails.*', 'files.mimetype', 'files.extension', 'files.filepath AS rawfilepath')
								->where('media_thumbnails.mediaId', $media->id)
								->where('media_thumbnails.width', $width)
								->first();
				if ($thumb)
				{
					$thumb->rawfilepath = str_replace('/files/', '', Config::get('media.content_dir')) . $thumb->rawfilepath;
				}

				// Append the full directory to the file path.
				if (!$thumb)
				{
					$media->rawfilepath = str_replace('/files/', '', Config::get('media.content_dir')) . $media->rawfilepath;
					if (file_exists($media->rawfilepath))
					{
						$thumb = $this->createThumbnail($media, $width, $height);
					}
					else
					{
						// TODO: If the file doesn't exist, we have an orphaned record. Clean it up and recreate the thumbnail.
					}
				}

				if ($thumb)
				{
					if (file_exists($thumb->rawfilepath))
					{
						$headers = array('Content-Type: ' . $thumb->mimetype);
						return Response::download($thumb->rawfilepath, $thumb->filename, $headers);
					}
				}
			}
		}
        return null;
	}



	/**
	 * Create a thumbnail (JPEG) of the specified resource.
	 *
	 * @param object $media
	 * @param int $width
	 * @param int $height
	 * @param bool $forceCreate Create the thumbnail if it doesn't already exist.
	 * @return
	 */
	public function createThumbnail($media, $width, $height, $forceCreate = false)
	{
		if ($media->type == 'image' || $media->type == 'backgroundimage')
		{
			$temp_filename = Config::get('media.temp_dir') . uniqid('temp_') . '_' . mt_rand(10000000, 99999999) . '.jpg';
			$cmd = 'convert ' . escapeshellarg($media->rawfilepath) . ' -resize ' . $width . 'x' . $height . '\! ' . escapeshellarg($temp_filename);
			exec($cmd);
			if (file_exists($temp_filename))
			{
				// Get the contenthash for the file, move it to the proper directory, and create a file record.
				$contenthash = sha1_file($temp_filename);
				if (strlen($contenthash) >= 16)
				{
                    // Make the subdirectory if we need to.
					$filedir = substr($contenthash, 0, 2);
                    if (!is_dir(Config::get('media.content_dir') . $filedir))
                           mkdir(Config::get('media.content_dir') . $filedir);

                    $filepath = $filedir . '/' . $contenthash;
	
					// TODO: Get width, height, and length.
	
					// If the file already exists and is the same size, this thumbnail has been created before, but is
					// just missing a thumbnail record for whatever reason. Create the thumbnail record now and return it.
					// before. We should only overwrite it here if $forceCreate is true.
					$fileItem = FileItem::where('contenthash', $contenthash)
											->where('filesize', filesize($temp_filename))
											->first();
					if ($fileItem)
					{
						$fileId = $fileItem->getId();
	
						// Make sure the file exists.
						if (!file_exists(Config::get('media.content_dir') . $filepath))
						{
                            return Response::json(array('success' => false, 'error' => 'Could not retrieve thumbnail.'));
						}
                        else
                        {
                            // Create the thumbnail record if we don't have one.
                            $media_thumbnail = MediaThumbnail::where('mediaId', $media->id)
                                                            ->where('fileId', $fileId)
															->where('width', $width)
															->first();
                            if (!$media_thumbnail)
                            {
                            	$filename_info = pathinfo($media->filename);
								$thumbnail_filename = $filename_info['filename'] . '-thumb-' . $width . 'x' . $height . '.jpg';
								$media_thumbnail = MediaThumbnail::create(array(
									'mediaId' => $media->id,
									'fileId' => $fileId,
									'width' => $width,
									'filename' => $thumbnail_filename,
								));
                            }

                            $media_thumbnail->mimetype = $media->mimetype;
                            $media_thumbnail->extension = 'jpg';
                            $media_thumbnail->rawfilepath = Config::get('media.content_dir') . $filepath;
                        }
					}
					else
					{
						// Move the file.
						if (!rename($temp_filename, Config::get('media.content_dir') . $filepath))
						{
							return Response::json(array('success' => false, 'error' => 'Could not save thumbnail.'));
						}
						else
						{
							// Create the new file record.
	                    	$filename_info = pathinfo($media->filename);
							$thumbnail_filename = $filename_info['filename'] . '-thumb-' . $width . 'x' . $height . '.jpg';
							$fileItem = FileItem::create(array(
								'contenthash' => $contenthash,
								'filepath' => '/files/' . $filepath,
								'title' => $media->filename . ' (thumbnail ' . $width . 'x' . $height . ')',
								'filename' => $thumbnail_filename,
								'mimetype' => $media->mimetype,
								'filesize' => filesize(Config::get('media.content_dir') . $filepath),
								'extension' => 'jpg',
							));
							$fileId = $fileItem->id;

							// Create the thumbnail record.
							$media_thumbnail = MediaThumbnail::create(array(
								'mediaId' => $media->id,
								'fileId' => $fileId,
								'width' => $width,
								'filename' => $thumbnail_filename,
							));

                            $media_thumbnail->mimetype = $media->mimetype;
                            $media_thumbnail->extension = 'jpg';
                            $media_thumbnail->rawfilepath = Config::get('media.content_dir') . $filepath;
						}
					}
				}
				unlink($temp_filename);
			}
		}

		if (isset($media_thumbnail))
			return $media_thumbnail;
		else
			return null;
	}


	/**
	 * Retrieve an array for the file manager containing all the project files and directories.
	 *
	 * @param int $projectId
	 * @return array
	 */
	public function getFileArray($projectId)
	{
/*
		$dirs = DB::table('media_folders')
					->where('projectId', '=', $projectId)
					->get();

		$media = DB::table('media')
					->where('projectId', '=', $projectId)
					->get();
*/

		$dirs = MediaFolder::with(array('childrenRecursive', 'media'))
							->where('projectId', $projectId)
							->get();

		// Create the directories first.
		$files = array();
		$folders = array(
			'/' => array(
				'name' => '/',
				'type' => 'folder',
				'path' => '/',
				'thumbnail' => '',
			)
		);

		$dir = new stdClass;
		$dir->id = 0;
		$dir->name = 'project';
		$dir->path = 'project';
		$dir->thumbnail = '';

		$dir->items = $this->addFiles($projectId, 0, 'project');
		echo '<pre>'; print_r(json_decode(json_encode($files))); echo '</pre>';
/*
		foreach ($dirs as $dirId => $dir)
		{
			print_r($dir);
		}
*/
		echo json_encode($dirs);
	}


	protected function addFiles($projectId, $dir, $current_path = '', $files = array())
	{
		// Get the items for the current directory.
		$items = array();

		$folder = new stdClass;
		$folder->type = 'folder';
		$folder->path = $current_path;
		$folder->thumbnail = '';

		if ($dir == null)
		{
			$folderName = 'project';
			$folderId = 0;
		}
		else
		{
			$folderName = $dir->name;
			$folderId = $dir->id;
		}


		// Get the folders for the current directory, and recursively load everything in them.
		$dirs = MediaFolder::with(array('childrenRecursive', 'media'))
							->where('projectId', $projectId)
							->where('parentId', $folderId)
							->get();
		foreach ($dirs as $diritems)
		{
			$folder = new stdClass;
			$folder->type = 'folder';
			$folder->path = $current_path;
			$folder->thumbnail = '';
			$folder->items = $this->addFiles($projectId, $dir->children_recursive, ($current_path .= '/' . $folderName), $files);
		}

		// Get the files for the current directory.
		echo '<pre>'; print_r(json_decode(json_encode($dir))); echo '</pre>';
		$dirfiles = DB::table('media')
					->where('projectId', $projectId)
					->where('folderId', $folderId)
					->orderBy('filename')
					->get();
		foreach ($dirfiles as $dirfile)
		{
			$item = new stdClass;
			$item->name = $dirfile->filename;
			$item->type = 'file';
			$item->path = $current_path . '/' . $dirfile->filename;
			$item->thumbnail = '';
			$items[] = $item;
		}

		$folder->items = $items;
		$files[] = $folder;
		return $files;
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$sessionId = Input::get('sessionId');
		if ($sessionId)
		{
			echo 'Session: ' . $sessionId;
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	public function download($contenthash, $filesize)
	{
		$file = DB::table('files')->where('files.contenthash', '=', $contenthash)
								->where('files.filesize', '=', $filesize)
								->first();
		if ($file)
		{
			// TODO: This is a hack to get around the fact that we've saved the files with /files/
			// in the path. We'll just want to remove that part, so the path begins at Config::get('media.content_dir').
	//		$filepath = Config::get('media.content_dir') . $file->filepath;
			$filepath = str_replace('/files/', '', Config::get('media.content_dir')) . $file->filepath;
			if (file_exists($filepath))
			{
				$headers = array('Content-Type: ' . $file->mimetype);
				return Response::download($filepath, $file->filename, $headers);
			}
		}
		return;
	}
}
