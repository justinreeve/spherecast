<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsCacheTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects_cache', function(Blueprint $table)
		{
			$table->integer('projectId');
			$table->boolean('useCache')->default(false);
			$table->longtext('json')->nullable();

			// Indexes.
			$table->primary('projectId');

			// created_at, updated_at DATETIME
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('projects_cache', function(Blueprint $table)
		{
		});
	}
}
