<?php

class SceneEventController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	protected function random($char = 8)
	{
		$random = "";
		srand((double) microtime() * 1000000);
		$data = "0123456789abcdefghijklmnopqrstuvwxyz";

		// Create a shortname, and ensure it has alphabetic characters and isn't already being used.
		do
		{
			for ($i = 0; $i < $char; $i++)
			{
				$random .= substr($data, (rand() % (strlen($data))), 1);
			}
		}
		while (is_numeric($random) || SceneEvent::where('shortname', $random)->first());
		return $random;
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$sceneId = Input::get('sceneId');
		$eventType = Input::get('eventType');
		$x = Input::get('positionX');
		$y = Input::get('positionY');
		$z = Input::get('positionZ');
		$radius = Input::get('radius');
		$duration = Input::get('duration');
		$target = Input::get('target');
		$labelMediaId = Input::get('labelMediaId');
		$countdownMediaId = Input::get('countdownMediaId');

		// Create a new event and add a hotspot with a jump action.
		if ($eventType == 'jump')
		{
			if (isset($sceneId) && isset($x) && isset($y) && isset($z) && isset($target))
			{
				if (!isset($radius))
					$radius = '40.000';
				if (!isset($duration))
					$duration = 1250;

				$event = SceneEvent::create(array(
					'shortname' => $this->random(8),
					'sceneId' => $sceneId,
					'type' => 'hotspot',
				));

				$params = new stdClass;
				$params->positionX = $x;
				$params->positionY = $y;
				$params->positionZ = $z;
				$params->spritemapTilesNum = 18;
				$params->spritemapTilesHorizontal = 18;
				$params->spritemapTilesVertical = 1;
				$params->spritemapHeight = 4320;
				$params->spritemapWidth = 237;
				$params->spritemapDuration = $duration;
				$params->spritemapRepeat = true;
				$params->spritemapShowFirstAsFixed = true;
				$params->spritemapTileSequence = array(
					1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
				);

				Hotspot::create(array(
					'eventId' => $event->id,
					'positionX' => $x,
					'positionY' => $y,
					'positionZ' => $z,
					'radius' => $radius,
					'duration' => $duration,
					'countdownMedia' => $countdownMediaId,
					'countdownMediaParams' => serialize($params),
				));

				if (isset($labelMediaId))
				{
					$params = new stdClass;
					$params->disappearOnCountdown = false;
					$params->opacity = 0.5;

					Label::create(array(
						'eventId' => $event->id,
						'mediaId' => $labelMediaId,
						'type' => 'image',
						'positionX' => $x,
						'positionY' => $y,
						'positionZ' => $z,
						'scaleX' => '0.25',
						'scaleY' => '0.25',
						'params' => serialize($params),
					));
				}

				$params = new stdClass;
				$params->targetId = $target;

				Action::create(array(
					'eventId' => $event->id,
					'type' => 'jump',
					'sequence' => 1,
					'continue' => 0,
					'params' => serialize($params),
				));

				Redirect::away('http://app.spherecast.org/hotspot.php');
			}
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
