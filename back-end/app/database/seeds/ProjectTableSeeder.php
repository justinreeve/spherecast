<?php

class ProjectTableSeeder extends Seeder
{
	public function run()
	{
		DB::table('projects')->delete();

		Project::create(array(
			'shortname' => 'language',
			'title' => 'Language Learning',
			'subtitle' => '',
			'description' => '',
			'createdBy' => 0,
			'startScene' => 0,
		));
	}

}
