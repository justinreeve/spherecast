<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMediaThumbnailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('media_thumbnails', function(Blueprint $table)
		{
			$table->dropPrimary(array('mediaId', 'fileId'));
			$table->index(array('mediaId', 'fileId'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('media_thumbnails', function(Blueprint $table)
		{
		});
	}

}
