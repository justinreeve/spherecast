<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSceneeventsHotspotsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sceneevents_hotspots', function(Blueprint $table)
		{
			$table->boolean('activateByTrigger')->default(false);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sceneevents_hotspots', function(Blueprint $table)
		{
			$table->dropColumn('activateByTrigger');
		});
	}
}
