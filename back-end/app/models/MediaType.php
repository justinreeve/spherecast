<?php
class MediaType extends Eloquent
{
	static protected $audio_mimetypes = array(
		'mp3' => 'audio/mpeg',
		'wav' => 'audio/wav',
	);

	static protected $document_mimetypes = array(
		"doc" => "application/msword",
		"docx" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
		"htm" => "text/html",
		"html" => "text/html",
		"odp" => "application/vnd.oasis.opendocument.presentation",
		"ods" => "application/vnd.oasis.opendocument.spreadsheet",
		"odt" => "application/vnd.oasis.opendocument.text",
		"pdf" => "application/pdf",
		"ppt" => "application/vnd.ms-powerpoint",
		"pptx" => "application/vnd.openxmlformats-officedocument.presentationml.presentation",
		"rtf" => "text/rtf",
		"txt" => "text/plain",
		"xls" => "application/vnd.ms-excel",
		"xlsx" => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
	);

	static protected $image_mimetypes = array(
		'gif' => 'image/gif',
		'jpg' => 'image/jpeg',
		'jpeg' => 'image/jpeg',
		'png' => 'image/png',
	);

	static protected $video_mimetypes = array(
		'asf' => 'video/x-ms-asf',
		'avi' => 'video/x-msvideo',
		'divx' => '',
		'flv' => 'video/x-flv',
		'm4v' => '',
		'moov' => '',
		'mov' => 'video/quicktime',
		'mp4' => 'video/mp4',
		'mpeg' => 'video/mpeg',
		'mpg' => 'video/mpeg',
		'rm' => '',
		'rmvb' => '',
		'vob' => '',
		'wmv' => 'video/x-ms-wmv',
		'3gp' => 'video/3gpp',
	);

	static public function getMimeTypeForExtension($ext)
	{
		$ext = strtolower($ext);
		if (isset(self::$audio_mimetypes[$ext]))
		{
			return self::$audio_mimetypes[$ext];
		}
		else if (isset(self::$document_mimetypes[$ext]))
		{
			return self::$document_mimetypes[$ext];
		}
		else if (isset(self::$image_mimetypes[$ext]))
		{
			return self::$image_mimetypes[$ext];
		}
		else if (isset(self::$video_mimetypes[$ext]))
		{
			return self::$video_mimetypes[$ext];
		}
		return false;
	}

	static public function getExtensionForMimeType($mimetype)
	{
		foreach (self::$audio_mimetypes as $ext => $mt)
		{
			if ($mt == $mimetype)
				return $ext;
		}

		foreach (self::$document_mimetypes as $ext => $mt)
		{
			if ($mt == $mimetype)
				return $ext;
		}

		foreach (self::$image_mimetypes as $ext => $mt)
		{
			if ($mt == $mimetype)
				return $ext;
		}

		foreach (self::$video_mimetypes as $ext => $mt)
		{
			if ($mt == $mimetype)
				return $ext;
		}
		return false;
	}

	static public function isAudio($q)
	{
		foreach (self::$audio_mimetypes as $ext => $mt)
		{
			if ($q == $ext || $q == $mt)
				return true;
		}
		return false;
	}

	static public function isDocument($q)
	{
		foreach (self::$document_mimetypes as $ext => $mt)
		{
			if ($q == $ext || $q == $mt)
				return true;
		}
		return false;
	}

	static public function isImage($q)
	{
		foreach (self::$image_mimetypes as $ext => $mt)
		{
			if ($q == $ext || $q == $mt)
				return true;
		}
		return false;
	}

	static public function isVideo($q)
	{
		foreach (self::$video_mimetypes as $ext => $mt)
		{
			if ($q == $ext || $q == $mt)
				return true;
		}
		return false;
	}
}
?>