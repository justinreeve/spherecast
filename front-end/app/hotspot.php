<?php
$sceneId = $_POST['sceneId'];
$type = $_POST['eventType'];
$x = $_POST['positionX'];
$y = $_POST['positionY'];
$z = $_POST['positionZ'];
$target = $_POST['target'];
$labelMediaId = $_POST['labelMediaId'];
?>
<form action="/api/event" method="post">
	<input type="hidden" name="projectId" value="5" />
	<input type="hidden" name="eventType" value="jump" />
	<input type="hidden" name="labelMediaId" value="237" />
	<input type="hidden" name="countdownMediaId" value="129" />
	<input type="hidden" name="radius" value="30.000" />
	<input type="hidden" name="duration" value="1250" />
	<input type="text" name="sceneId" placeholder="Scene ID" />
	<br /><br />
	<input type="text" name="positionX" placeholder="X" />
	<br />
	<input type="text" name="positionY" placeholder="Y" />
	<br />
	<input type="text" name="positionZ" placeholder="Z" />
	<br /><br />
	<input type="text" name="target" placeholder="Jump to" />
	<br /><br />
	<input type="submit" value="Submit" />
</form>