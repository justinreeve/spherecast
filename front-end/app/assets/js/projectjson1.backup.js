var projectjson1 = {
	id: 1,
	shortname: 'ocean',
	title: 'Ocean Exploration',
	subtitle: '',
	description: 'Join these scuba divers and explore the magnificent wonders of the ocean.',
	createdby: 1,
	timecreated: 0,
	startScene: 10,

	globalVariables: [
	{
		name: 'playedEnteringTheDepths',
		defaultValue: '0',
	},
	{
		name: 'playedTubbatahaReefs',
		defaultValue: '0',
	},
	{
		name: 'playedSubtationCuracao',
		defaultValue: '0',
	}],

	prefetchAssets:
	{
		images: [
		{
			id: 1,
			sceneid: 1,
			path: '/files/49/494679425abebbdeb7457822e9dcff2d4273de0b',
			priority: 1,										// Background media should take priority.
			type: 'image',
		},
		{
			id: 2,
			sceneid: 3,
			path: '/files/51/51d31bdbafb7223728549a270e46ef0f415abdc4',
			priority: 1,										// Background media should take priority.
			type: 'image',
		}],

		audio: [
		{
			id: 6,
			sceneid: 1,
			path: '/files/f5/f51cd3358db6e6f531d0e50dd86678147b19f6cc',
			priority: 1,
			type: 'audio',
		},
		{
			id: 10,
			sceneid: 2,
			path: '/files/23/2349bbe135b6e1a2790ccfe691eab0589094cdc6',
			priority: 1,
			type: 'audio',
		},
		{
			id: 9,
			sceneid: 1,
			path: '/files/89/8927300a265eb3f6a3a7f5806b37d70206b08bc0',
			priority: 2,
			type: 'audio',
		}],

		video: [
		{
			id: 17,
			sceneid: 10,
			path: '/files/86/86e595f59e6904cd3ae1b39b6ef1ab8f07e0fd49',
			priority: 1,
			type: 'video',
		}],

		sceneManifest: [
		{
			sceneid: 1,
			paths: [
			{
				id: 'image_1',
				src: '/files/49/494679425abebbdeb7457822e9dcff2d4273de0b',
				assetType: 'image',
			},
			{
				id: 'audio_6',
				src: '/files/f5/f51cd3358db6e6f531d0e50dd86678147b19f6cc',
				assetType: 'audio',
			},
			{
				id: 'audio_9',													// Priority 2 asset.
				src: '/files/89/8927300a265eb3f6a3a7f5806b37d70206b08bc0',
				assetType: 'audio',
			}],
		},
		{
			sceneid: 2,
			paths: [
			{
				id: 'audio_10',
				src: '/files/23/2349bbe135b6e1a2790ccfe691eab0589094cdc6',
				assetType: 'audio',
			}],
		},
		{
			sceneid: 3,
			paths: [
			{
				id: 'image_2',
				src: '/files/51/51d31bdbafb7223728549a270e46ef0f415abdc4',
				assetType: 'image',
			}],
		},
		{
			sceneid: 10,
			paths: [
			{
				id: 'video_17',
				src: '/files/86/86e595f59e6904cd3ae1b39b6ef1ab8f07e0fd49',
				assetType: 'video',
			}],
		}],

		orderManifest: [
		{
			id: 'image_1',
			src: '/files/49/494679425abebbdeb7457822e9dcff2d4273de0b',
		},
		{
			id: 'image_2',
			src: '/files/51/51d31bdbafb7223728549a270e46ef0f415abdc4',
		},
		{
			id: 'audio_10',
			src: '/files/23/2349bbe135b6e1a2790ccfe691eab0589094cdc6',
		},
		{
			id: 'audio_6',
			src: '/files/f5/f51cd3358db6e6f531d0e50dd86678147b19f6cc',
		},
		{
			id: 'video_17',
			src: '/files/86/86e595f59e6904cd3ae1b39b6ef1ab8f07e0fd49',
		},
		{
			id: 'audio_9',
			src: '/files/89/8927300a265eb3f6a3a7f5806b37d70206b08bc0',
		}],
	},

	scenes: [
	{
		id: 1,
		shortname: 'bnzswbsmnswrinsj',
		projectid: '1',
		sequence: '1',
		parent: '0',
		title: 'Entering the Depths',
		subtitle: '',
		description: 'Scuba diving is a sport that is practiced recreationally all around the world, and for some, is even a profession. Once underwater, divers meet a dazzling array of fish, marine mammals, and aquatic plant life. The undersea world is vast with beauty. A trained scuba diver can swim to depths over 100 feet, and explore this amazing world that few people get to see.',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: '1',
			filePath: '/files/49/494679425abebbdeb7457822e9dcff2d4273de0b',
			fileType: 'image',
		},
		embedid: '0',
		createdby: '1',
		lockedby: '0',
		lockedtime: '0',
		timecreated: '0',
		timemodified: '0',

		// As start events are running, hotspots can be disabled until they have all completed if "overridable" is set to "0."
		// Conditions can be placed on either entire events or single actions. In this case, we don't play
		// the event if the global variable playedEnteringTheDepths flag has been set.
		startEvents: [
		{
			id: 4,
			visible: '0',
			overridable: '1',							// The text and the audio can be interrupted by hotspots events.
			conditions: [
			{
				conditionType: 'variable',
				conditionVariableName: 'playedEnteringTheDepths',
				conditionVariableOperator: '!=',
				conditionVariableOperand: '0',
			}],

			actions: [
			{
				actionType: 'text',						// Fade in the text and fade out after 3 seconds.
				actionText: 'Welcome to the Spherecast',
				actionDuration: 1500,					// Milliseconds
				actionSequence: 1,
				actionContinue: true,					// If the "continue" flag is set to false, do not process the next action until this one finishes.
			},
			{
				actionType: 'audio',					// Play an audio file.
				actionItemId: 9,
				actionFile:
				{
					fileId: 10,
//					filePath: '/files/f5/f51cd3358db6e6f531d0e50dd86678147b19f6cc',
					fileType: 'audio',
				},
				actionDuration: 0,						// TODO: Number of milliseconds before stopping audio file.
				actionRepeat: false,					// TODO: Repeat the audio file indefinitely (useful for ambient sounds). Should not (can not?) be used if actionContinue is set to "false."
				actionSequence: 2,
				actionContinue: false,					// Don't continue until the audio file is finished playing, or another audio action has interrupted it.
			},
			{
				actionType: 'variable',
				actionVariableName: 'playedEnteringTheDepths',
				actionVariableOperator: 'set',
				actionVariableOperand: '1',
				actionSequence: 3,
				actionContinue: false,					// Should always be "false" when setting a variable.
			}],
		}],

		// End events only occur immediately before a scene jump. The jump takes place when they have all finished.
		endEvents: [
		{
		}],

		// If a hotspot is not "overridable" that means no other hotspots can be activated while the event is playing its actions.
		// If it is "overridable" as soon as as hotspot event is triggered (after the arc completes its rotation), the overridable
		// event will stop playing.

		// If there's an unmet condition on a hotspot event, don't even process the hotspot countdown. This is how hotspots can
		// be deactivated, by using global variables.

		// The toggleOtherHotspots field deactivates all other hotspots in the scene until the same hotspot is activated again.
		// Although this is intended to be used as a nav panel trigger to turn hotspots on/off, it could also be used in conjunction
		// with global variables to restrict access to different parts of the scene until conditions are met.
		hotspots: [
		{
			id: 3,
			positionX: '0',
			positionY: '1',
			positionZ: '0',
			radius: '0.2',
			duration: '1500',							// Milliseconds
			visible: '1',
			overridable: '0',
			toggleOtherHotspots: true,					// If "true" then this hotspot is exclusive when activated. No other hotsopts in the scene can be activated until this hotspot is activated again.
			conditions: [
			{
				conditionType: 'variable',
				conditionVariableName: 'playedEnteringTheDepths',
				conditionVariableOperator: '!=',
				conditionVariableOperand: '0',
			}],
			actions: [
			{
				actionType: 'jump',
				actionItemId: 2,
				actionSequence: 1
			}],
		},
		{
			id: 2,
			positionX: '0',
			positionY: '-1',
			positionZ: '0',
			radius: '0.2',
			duration: '1000',							// Milliseconds
			visible: '1',
			overridable: '0',
			actions: [
			{
				actionType: 'jump',
				actionItemId: 2,
				actionSequence: 2,
				audioContinue: false,
			}],
		},
		{
			id: 1,
			positionX: '-0.067',
			positionY: '-0.209',
			positionZ: '0.976',
			radius: '0.2',
			label: 'Diver',
			duration: '1000',							// Milliseconds
			visible: '1',
			overridable: '0',
			conditions: [
			{
				conditionType: 'variable',
				conditionVariableName: 'playedEnteringTheDepths',
				conditionVariableOperator: '!=',
				conditionVariableOperand: '0',
			}],

			actions: [
			{
				actionType: 'text',						// Fade in the text and fade out after 3 seconds.
				actionText: 'This is a diver',
				actionDuration: 1000,					// Milliseconds
				actionSequence: 1,
				actionContinue: false,					// If the "continue" flag is set to false, do not process the next action until this one finishes.
			},
			{
				actionType: 'image',					// Display an image.
				actionPlacement: 'fixed',				// This places an image directly at the specified coordinates (which are directly over the hotspot).
				actionDuration: 1000,
				actionPositionX: '-0.067',
				actionPositionY: '-0.209',
				actionPositionZ: '-0.976',
				actionItemId: 3,
				actionFile:
				{
					fileId: 3,
					filePath: '/files/78/78fe5b67a58b211159883d6cd28dd81814b8d685',
					fileType: 'image',
				},
				actionSequence: 4,
				actionContinue: true,
			},
			{
				actionType: 'variable',					// Set a global variable.
				actionVariableName: 'playedEnteringTheDepths',
				actionVariableOperator: 'set',
				actionVariableOperand: '1',
				actionSequence: 5,
				actionContinue: false,
			},
			{
				actionType: 'audio',					// Play an audio file.
				actionItemId: 9,
				actionFile:
				{
					fileId: 9,
					filePath: '/files/89/8927300a265eb3f6a3a7f5806b37d70206b08bc0',
					fileType: 'audio',
				},
				actionDuration: 0,						// TODO: Number of milliseconds before stopping audio file.
				actionRepeat: false,					// TODO: Repeat the audio file indefinitely (useful for ambient sounds). Cannot be used if actionContinue is set to "false."
				actionSequence: 6,
				actionContinue: true,					// Don't continue until the audio file is finished playing, or another audio action has interrupted it.
			},
			{
				actionType: 'audio',					// Play an audio file.
				actionItemId: 10,
				actionFile:
				{
					fileId: 10,
					filePath: '/files/f5/f51cd3358db6e6f531d0e50dd86678147b19f6cc',
					fileType: 'audio',
				},
				actionDuration: 0,						// TODO: Number of milliseconds before stopping audio file.
				actionRepeat: true,						// TODO: Repeat the audio file indefinitely (useful for ambient sounds). Cannot be used if actionContinue is set to "false."
				actionSequence: 7,
				actionContinue: true,					// Don't continue until the audio file is finished playing, or another audio action has interrupted it.
			},
			{
				actionType: 'text',						// Fade in the text and fade out after 3 seconds.
				actionText: 'Feel free to browse around the scene',
				actionDuration: 5000,					// Milliseconds
				actionSequence: 8,
				actionContinue: true,					// If the "continue" flag is set to false, do not process the next action until this one finishes.
			}],
		}],
	},
	{
		id: 2,
		shortname: 'zpqgbfamcfjbllsv',
		projectid: '1',
		sequence: '2',
		parent: '0',
		title: 'Tubbataha Reefs',
		subtitle: '',
		description: 'The Tubbataha Reefs, located in a protected area of the Philippines, in the middle of the Sulu Sea, has one of the highest density rates of marine life anywhere in the known ocean. There are no less than 600 fish species, 360 coral species, 11 shark species, and 13 dolphin and whale species that live in the reefs. It is made up of two coral atolls, which form a spectacular 330-foot perpendicular coral wall, and was formed thousands of years ago by underwater volcanoes which are now inactive.',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: '4',
			filePath: '/files/49/490f3190819d9ecc83180b161e83f5776b6ca9bf',
			fileType: 'image',
		},
		embedid: '0',
		createdby: '1',
		lockedby: '0',
		lockedtime: '0',
		timecreated: '0',
		timemodified: '0',

		// As start events are running, hotspots are disabled until they have all completed.
		// Conditions can be placed on either entire events or single actions. In this case, we don't play
		// the event if the global variable playedEnteringTheDepths flag has been set.
		startEvents: [
		{
			id: 1,
			visible: '0',
			overridable: '1',							// The text and the audio can be interrupted by hotspots events.
			actions: [
			{
				actionType: 'text',						// Fade in the text and fade out after 3 seconds.
				actionText: 'Tubbataha Reefs',
				actionDuration: 1500,					// Milliseconds
				actionSequence: 1,
				actionContinue: false,
			},
			{
				actionType: 'audio',					// Play an audio file.
				actionItemId: 10,
				actionFile:
				{
					fileId: 10,
					filePath: '/files/23/2349bbe135b6e1a2790ccfe691eab0589094cdc6',
					fileType: 'audio',
				},
				actionDuration: 0,						// TODO: Number of milliseconds before stopping audio file.
				actionRepeat: false,					// TODO: Repeat the audio file indefinitely (useful for ambient sounds). Cannot be used if actionContinue is set to "false."
				actionSequence: 1,
				actionContinue: false,					// Don't continue until the audio file is finished playing, or another audio action has interrupted it.
			}],
		}],

		// End events only occur immediately before a scene jump. The jump takes place when they have all finished.
		endEvents: [
		{
		}],

		// If a hotspot is not "overridable" that means no other hotspots can be activated while the event is playing its actions.
		// If it is "overridable" as soon as as hotspot event is triggered (after the arc completes its rotation), the overridable
		// event will stop playing.
		hotspots: [
		{
			id: 5,
			positionX: '0',
			positionY: '-1',
			positionZ: '0',
			radius: '0.2',
			duration: '2000',				// Milliseconds
			visible: '1',
			overridable: '0',
			actions: [
			{
				actionType: 'jump',
				actionItemId: 3,
				actionSequence: 1,
				actionContinue: false,
			}],
		}],
	},
	{
		id: 3,
		shortname: 'oczvjggkxphdeopy',
		projectid: '1',
		sequence: '3',
		parent: '0',
		title: 'Substation Curacao',
		subtitle: '',
		description: 'The divers pass by the Substation Curacao. This is a mini-submarine for tourists, capable of carrying two passengers at a time. Unlike scuba diving, submarine diving has no effects of pressure change on the body. As a result, it is able to safely travel to depths of 1000 feet, deeper than the divers can reach. The bottom of the ocean is a vast and greatly unknown frontier, and these submarines can be used for scientific research as well. Studying the ocean floor can help contribute to the understanding of bio-chemical structures and substances found within marine biology.',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: '1',
			filePath: '/files/51/51d31bdbafb7223728549a270e46ef0f415abdc4',
			fileType: 'image',
		},
		embedid: '0',
		createdby: '1',
		lockedby: '0',
		lockedtime: '0',
		timecreated: '0',
		timemodified: '0',

		// As start events are running, hotspots are disabled until they have all completed.
		// Conditions can be placed on either entire events or single actions. In this case, we don't play
		// the event if the global variable playedEnteringTheDepths flag has been set.
		startEvents: [
		{
			id: 10,
			visible: '0',
			overridable: '1',							// The text and the audio can be interrupted by hotspots events.
			actions: [
			{
				actionType: 'audio',					// Play an audio file.
				actionItemId: 11,
				actionFile:
				{
					fileId: 11,
					filePath: '/files/62/62d088ce5b50f00fd905e052a1287e12aa2d3d47',
					fileType: 'audio',
				},
				actionDuration: 0,						// TODO: Number of milliseconds before stopping audio file.
				actionRepeat: false,					// TODO: Repeat the audio file indefinitely (useful for ambient sounds). Cannot be used if actionContinue is set to "false."
				actionSequence: 1,
				actionContinue: false,					// Don't continue until the audio file is finished playing, or another audio action has interrupted it.
			}],
		}],

		// End events only occur immediately before a scene jump. The jump takes place when they have all finished.
		endEvents: [
		{
		}],

		// If a hotspot is not "overridable" that means no other hotspots can be activated while the event is playing its actions.
		// If it is "overridable" as soon as as hotspot event is triggered (after the arc completes its rotation), the overridable
		// event will stop playing.
		hotspots: [
		{
			id: 4,
			positionX: '-0.512',
			positionY: '-0.110',
			positionZ: '-0.86',
			radius: '0.5',
			duration: '1',					// Milliseconds
			visible: '1',
			overridable: '0',
			actions: [
			{
				actionType: 'text',
				actionText: 'Substation Curacao',
				actionDuration: 1000,
				actionSequence: 1,
				actionContinue: true,
			}],
		},
		{
			id: 5,
			positionX: '0.870',
			positionY: '0.426',
			positionZ: '0.251',
			radius: '0.2',
			duration: '1',					// Milliseconds
			visible: '1',
			overridable: '0',
			actions: [
			{
				actionType: 'text',
				actionText: 'Diver',
				actionDuration: 1000,
				actionSequence: 1,
				actionContinue: true,
			}],
		},
		{
			id: 6,
			positionX: '0',
			positionY: '-1',
			positionZ: '0',
			radius: '0.2',
			duration: '2000',					// Milliseconds
			visible: '1',
			overridable: '0',
			actions: [
			{
				actionType: 'jump',
				actionItemId: 4,
				actionSequence: 1,
				actionContinue: false,
			}],
		}],
	},
	{
		id: 4,
		shortname: 'bctnxcawpgbegbuz',
		projectid: '1',
		sequence: '4',
		parent: '0',
		title: 'Bait Ball',
		subtitle: '',
		description: 'Fish swimming alone are more likely to be eaten by a predator than large groups. So these salema fish are congregating in what’s known as a “bait ball.” This is where the school of fish swarm in a tightly-packed spherical formation. It’s a defensive mechanism smaller fish use when they’re threatened by predators.',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: '20',
			filePath: '/files/a8/a86d88e1aa2a1705fdec1de5c19087b7316b3160',
			fileType: 'image',
		},
		embedid: '0',
		createdby: '1',
		lockedby: '0',
		lockedtime: '0',
		timecreated: '0',
		timemodified: '0',

		// As start events are running, hotspots are disabled until they have all completed.
		// Conditions can be placed on either entire events or single actions. In this case, we don't play
		// the event if the global variable playedEnteringTheDepths flag has been set.
		startEvents: [
		{
			id: 11,
			visible: '0',
			overridable: '1',							// The text and the audio can be interrupted by hotspots events.
			actions: [
			{
				actionType: 'audio',					// Play an audio file.
				actionItemId: 12,
				actionFile:
				{
					fileId: 12,
//					filePath: '/files/58/581afbaa612b0ed936449290cb345b02c31cc282',
					fileType: 'audio',
				},
				actionDuration: 0,						// TODO: Number of milliseconds before stopping audio file.
				actionRepeat: false,					// TODO: Repeat the audio file indefinitely (useful for ambient sounds). Cannot be used if actionContinue is set to "false."
				actionSequence: 1,
				actionContinue: false,					// Don't continue until the audio file is finished playing, or another audio action has interrupted it.
			}],
		}],

		// End events only occur immediately before a scene jump. The jump takes place when they have all finished.
		endEvents: [
		{
		}],

		// If a hotspot is not "overridable" that means no other hotspots can be activated while the event is playing its actions.
		// If it is "overridable" as soon as as hotspot event is triggered (after the arc completes its rotation), the overridable
		// event will stop playing.
		hotspots: [
		{
			id: 6,
			positionX: '0',
			positionY: '-1',
			positionZ: '0',
			radius: '0.2',
			duration: '2000',
			visible: '1',
			overridable: '0',
			actions: [
			{
				actionType: 'jump',
				actionItemId: 10,
				actionSequence: 1,
				actionContinue: false,
			}],
		}],
	},

	// This scene contains a video as the background. As soon as the video is finished playing, it jumps to another scene.
	// This could be indicated by either a startEvent with a delay exactly the length of the video, or a special stopEvent
	// that is specific to the scene itself. If we were playing a popup video as a normal event, we'd simply set actionContinue
	// to false to prevent any further actions from playing until the video was finished.
	{
		id: 10,
		shortname: 'bctnxcawpgbegbuz',
		projectid: '1',
		sequence: '4',
		parent: '0',
		title: 'Scuba Diving',
		subtitle: '',
		description: 'Experience a video of scuba diving.',
		viewType: 'spherecast',
		backgroundFile:
		{
			fileId: '17',
			filePath: '/files/86/86e595f59e6904cd3ae1b39b6ef1ab8f07e0fd49',
			fileType: 'video',
			fileMimeType: 'video/webm',
		},
		embedid: '0',
		createdby: '1',
		lockedby: '0',
		lockedtime: '0',
		timecreated: '0',
		timemodified: '0',

		// Background events are specific to events occuring with the background media (currently, just videos).
		backgroundEvents: [
		{
			id: 30,
			visible: 0,
			overridable: 0,
			actions: [
			{
				actionType: 'stop',
			}],
		}],

		// As start events are running, hotspots are disabled until they have all completed.
		// Conditions can be placed on either entire events or single actions. In this case, we don't play
		// the event if the global variable playedEnteringTheDepths flag has been set.
		startEvents: [
		{
			id: 20,
			visible: 0,
			overridable: 0,								// The event cannot be interrupted by hotspots events.
			actions: [
			{
				actionType: 'audio',					// Play an audio file.
				actionItemId: 9,
				actionFile:
				{
					fileId: 10,
					filePath: '/files/f5/f51cd3358db6e6f531d0e50dd86678147b19f6cc',
					fileType: 'audio',
				},
				actionDuration: 0,						// TODO: Number of milliseconds before stopping audio file.
				actionRepeat: false,					// TODO: Repeat the audio file indefinitely (useful for ambient sounds). Should not (can not?) be used if actionContinue is set to "false."
				actionSequence: 1,
				actionContinue: false,					// Don't continue until the audio file is finished playing, or another audio action has interrupted it.
			}],
		}],

		speechEvents: [
		{
			id: 100,
			words: ['stop', 'pause'],
			language: 'en',
			minConfidence: 0.75,
			overridable: 0,
			actions: [
			{
				actionType: 'mediaStop',
				actionItemId: 10,						// The file id of the audio or video to stop playing (the audio or video player should contain this in its id).
				actionSequence: 1,
				actionContinue: false,
			}],
		},
		{
			id: 101,
			words: ['play'],
			language: 'en',
			minConfidence: 0.75,
			overridable: 0,
			actions: [
			{
				actionType: 'mediaPlay',
				actionItemId: 10,						// The file id of the audio or video to start playing (the audio or video player should contain this in its id).
				actionSequence: 1,
				actionContinue: false,
			}],
		},
		{
			id: 102,
			words: ['back'],
			language: 'en',
			minConfidence: 0.75,
			overridable: 0,
			actions: [
			{
				actionType: 'mediaBack',
				actionItemId: 10,						// The file id of the audio or video to skip back.
				actionSkipDuration: '10%',				// This could be a number of seconds, or a percentage of the entire media.
				actionSequence: 1,
				actionContinue: false,
			}],
		},
		{
			id: 103,
			words: ['forward'],
			language: 'en',
			minConfidence: 0.75,
			overridable: 0,
			actions: [
			{
				actionType: 'mediaPlay',
				actionItemId: 10,						// The file id of the audio or video to skip forward.
				actionSkipDuration: '10%',				// This could be a number of seconds, or a percentage of the entire media.
				actionSequence: 1,
				actionContinue: false,
			}],
		}],

		// End events only occur immediately before a scene jump. The jump takes place when they have all finished.
		endEvents: [
		{
		}],

		// If a hotspot is not "overridable" that means no other hotspots can be activated while the event is playing its actions.
		// If it is "overridable" as soon as as hotspot event is triggered (after the arc completes its rotation), the overridable
		// event will stop playing.
		hotspots: [
		{
		}],
	},
	{
		id: 20,
		shortname: 'abcdefghijklmop',
		projectid: '1',
		sequence: '5',
		parent: '0',
		title: 'Google Street View',
		subtitle: '',
		description: '',
		viewType: 'googlestreetview',
		backgroundUrl: '',
		embedid: '0',									// Create a separate table and link to it with the embedid?
		createdby: '1',
		lockedby: '0',
		lockedtime: '0',
		timecreated: '0',
		timemodified: '0',

		startEvents: [
		{
		}],

		endEvents: [
		{
		}],

		hotspots: [
		{
		}],
	},
	{
		id: 6,
		shortname: 'abcdefghijklmop',
		projectid: '1',
		sequence: '6',
		parent: '0',
		title: 'Sketchfab',
		subtitle: '',
		description: '',
		viewType: 'sketchfab',
		backgroundUrl: '',
		embedid: '0',									// Create a separate table and link to it with the embedid?
		createdby: '1',
		lockedby: '0',
		lockedtime: '0',
		timecreated: '0',
		timemodified: '0',

		startEvents: [
		{
		}],

		endEvents: [
		{
		}],

		hotspots: [
		{
		}],
	}],
};
