@include('docs.docsheader')
<h1>Conditions</h1>

<div class="attributes">
	<dl>
		<dt class="integer">id</dt>
		<dd>condition id</dd>

		<dt class="integer"><a href="/api/docs/hotspots">eventId</a></dt>
		<dd>event id the condition is associated with</dd>

		<dt class="string" data-default="variable">type</dt>
		<dd>the type of condition; currently, only "variable" is supported, and this refers to a <a href="/api/docs/globalvariables">global variable</a> that has been defined at the <a href="/api/docs/project">project</a> level.</dd>

		<dt class="string">variableName</dt>
		<dd>
			either a global variable that has been defined at the project level, or any of the following reserved variables that are prefixed with "SC":
			<dl class="options">
				<dt class="string">SCViewerMode</dt>
				<dd>indicates which mode (standard or VR) the user currently has enabled, so interactions can be different depending on the mode; for example, an overlay appearing in the corner in standard mode, but popping up in the center in VR mode</dd>

				<dt class="integer">SCTimeElapsed</dt>
				<dd>the number of seconds the user has currently spent in the project</dd>

				<dt class="boolean">SCShowCrosshairs</dt>
				<dd>whether crosshairs should be displayed in the viewer or not</dd>
			</dl>
		</dd>

		<dt class="string">variableOperator</dt>
		<dd>
			one of four operators to perform on the variable, to test if the condition is true or false:
			<dl class="options">
				<dt>=</dt>
				<dd>test if variableName equals variableOperand; if variableOperand is a string, tests are case-insensitive</dd>

				<dt>!=</dt>
				<dd>test if variableName does not equal variableOperand; if variableOperand is a string, tests are case-insensitive</dd>

				<dt>&gt;</dt>
				<dd>test if variableName is greater than variableOperand; if variableOperand is not numeric, the test will always return false</dd>

				<dt>&lt;</dt>
				<dd>test if variableName is less than variableOperand; if variableOperand is not numeric, the test will always return false</dd>
			</dl>
		</dd>

		<dt class="string">variableOperand</dt>
		<dd>a number (integer or decimal), or a string for which the variableName is being tested with variableOperand</dd>
	</dl>
</div>

<div class="example">
	<pre>
{
	"id": "2",
	"eventId": "12",
	"type": "variable",
	"variableName": "completedMountainScene",
	"variableOperator": "=",
	"variableOperand": "1",
	"created_at": "-0001-11-30 00:00:00",
	"updated_at": "-0001-11-30 00:00:00"
}
	</pre>
</div>
@include('docs.docsfooter')