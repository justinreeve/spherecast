var spherecastApp = angular.module('spherecastApp', ['ngRoute']);

spherecastApp.run(function($rootScope, $templateCache) {
    $rootScope.$on('$routeChangeStart', function(event, next, current) {
        if (typeof(current) !== 'undefined'){
            $templateCache.remove(current.templateUrl);
        }
    });
});

spherecastApp.constant('Modernizr', Modernizr);
spherecastApp.constant('Detectizr', Detectizr);

spherecastApp.controller('AppCtrl', ['$scope', function($scope)
{
    $scope.canvasWidth = 400;
    $scope.canvasHeight = 400;
    $scope.dofillcontainer = true;
    $scope.scale = 1;

	$scope.toggleMode = function() {
		if ($scope.mode == 'vr')
			$scope.mode = 'standard';
		else
			$scope.mode = 'vr';
	};
}]);

spherecastApp.controller('EditorController', ['$scope', '$http', '$route', '$routeParams', '$location', 'Modernizr', 'Project', 'Scene', function($scope, $http, $route, $routeParams, $location, Modernizr, Project, Scene)
{
	// Make sure we have routeParams for a sceneId. Otherwise, get the startScene for the project.
	$scope.Detectizr = Detectizr;

	$scope.projectLoaded = false;
	$scope.embedded = true;
    $scope.vrMode = false;

	$scope.showShareBlock = true;
	$scope.toggleShareBlock = function()
	{
		$scope.showShareBlock = $scope.showShareBlock === false ? true : false;
	};

	$scope.selectShareText = function($event)
	{
		$event.target.select();
	};

	// TODO: Use a hidden button as the trigger for fullscreen, and process it in a $scope.watch().
	$scope.toggleFullscreen = function()
	{
	}

//	if (Modernizr.hasEvent('deviceOrientation'))
//	if (window.DeviceOrientationEvent || window.OrientationEvent || typeof window.onorientationchange != 'undefined')
	{
		$scope.vrMode = false;
	}

	// Retrieve the scene.
	if (typeof $routeParams.sceneId !== 'undefined')
	{
		Scene.get($routeParams.sceneId).then(function(response)
		{
			$scope.activeScene = response.data;
			$scope.projectLoaded = true;
		});
	}
	else
	{
		Project.get($routeParams.projectId, true).then(function(response)
		{
			$scope.activeScene = response.data;
			$scope.projectLoaded = true;
		});
	}
}]);

// See http://stackoverflow.com/questions/21667613/in-angular-how-to-pass-json-object-array-into-directive
spherecastApp.controller('ViewEmbedController', ['$scope', '$http', '$route', '$routeParams', 'Modernizr', 'Detectizr', 'Project', function($scope, $http, $route, $routeParams, Modernizr, Detectizr, Project)
{
	$scope.Detectizr = Detectizr;

	$scope.projectLoaded = false;
	$scope.showLoadingScreen = false;
	$scope.embedded = true;
	$scope.canvasWidth = '100%';
	$scope.canvasHeight = '100%';
    $scope.dofillcontainer = true;
    $scope.scale = 1;
    $scope.vrMode = true;
    $scope.requireTapToContinue = false;
    $scope.windowWidth = window.innerWidth;
    $scope.windowHeight = window.innerHeight;

	$scope.showShareBlock = true;
	$scope.toggleShareBlock = function()
	{
		$scope.showShareBlock = $scope.showShareBlock === false ? true : false;
	};

	$scope.selectShareText = function($event)
	{
		$event.target.select();
	};

	// TODO: Use a hidden button as the trigger for fullscreen, and process it in a $scope.watch().
	$scope.toggleFullscreen = function()
	{
		$scope.goFullscreen = $scope.goFullscreen === false ? true : false;
	}

//	if (Modernizr.hasEvent('deviceOrientation'))
//	if (window.DeviceOrientationEvent || window.OrientationEvent || typeof window.onorientationchange != 'undefined')
	// TODO: We may need to distinguish between phones and tablets, since tablets may still require
	// a click before playing media, but we don't necessarily want to load it in stereoscopic mode.
	if (isMobileBrowser() == true)
	{
		// TODO: Check if a screen tap has already occurred at some point. This would typically only
		// be true if we're using the editor.
		$scope.isMobile = true;
		$scope.requireTapToContinue = true;
		$scope.vrMode = true;
	}
	else
	{
		$scope.isMobile = false;
		$scope.vrMode = false;
	}

//	$scope.activeProject = $route.current.locals.activeProject;
	Project.get($routeParams.projectId).then(function(response) {
		$scope.activeProject = response.data;

		// If there's a scene id, change the startScene.
		if (typeof $routeParams.sceneId !== 'undefined')
		{
			for (var s = 0; s < $scope.activeProject.scenes.length; s++)
			{
				if ($scope.activeProject.scenes[s].shortname == $routeParams.sceneId)
				{
					$scope.activeProject.startScene = $scope.activeProject.scenes[s].id;
					break;
				}
			}
		}
		$scope.projectLoaded = true;
	});
}]);


spherecastApp.config(function($routeProvider, $locationProvider)
{
	$locationProvider.html5Mode(true);

	$routeProvider
		.when('/login',
		{
			templateUrl: 'partials/login.html',
			controller: 'LoginController'
		})
		.when('/project',
		{
			templateUrl: 'partials/projects.html',
			controller: 'ProjectController'
		})
		.when('/project/:projectId',
		{
			templateUrl: 'partials/scenes.html',
			controller: 'SceneController'
		})
		.when('/project/:projectId/:sceneId',
		{
			templateUrl: 'partials/scenes.html',
			controller: 'SceneController'
		})
		.when('/:projectId/edit',
		{
			templateUrl: 'partials/editor-desktop.html',
			controller: 'EditorController'
		})
		.when('/:projectId/:sceneId',
		{
			templateUrl: function(param) {
				if (Modernizr.webgl !== true)
				{
					return 'partials/upgradebrowser.html';
				}
				else
					return 'partials/viewer.html'
			},
			controller: 'ViewEmbedController',
		})
		.when('/:projectId/:sceneId/edit',
		{
			templateUrl: 'partials/editor-desktop.html',
			controller: 'EditorController'
		})
		.when('/:projectId',
		{
			templateUrl: function(param) {
				if (Modernizr.webgl !== true)
				{
					return 'partials/upgradebrowser.html';
				}
				else
					return 'partials/viewer.html'
			},
			controller: 'ViewEmbedController',
/*
			resolve: {
				activeProject: function(Project, $route) {
					return Project.get($route.current.params.projectId).then(function(response) {
						return response.data;
					});
				}
			}
*/
		})
		.when('/:projectId/:sceneId/edit',
		{
			templateUrl: 'partials/editor-desktop.html',
			controller: 'EditorController'
		})
		.otherwise({
			controller: function() {
				window.location.replace('http://spherecast.org');
			},
			template: '<div></div>'
	});

});


// The "stacktrace" library that we included in the Scripts
// is now in the Global scope; but, we don't want to reference
// global objects inside the AngularJS components - that's
// not how AngularJS rolls; as such, we want to wrap the
// stacktrace feature in a proper AngularJS service that
// formally exposes the print method.
/*
spherecastApp.factory(
	'stacktraceService',
	function() {
		return ({
			print: printStackTrace
		});
	}
);


// By default, AngularJS will catch errors and log them to
// the Console. We want to keep that behavior; however, we
// want to intercept it so that we can also log the errors
// to the server for later analysis.
app.provider(
    "$exceptionHandler",
    {
        $get: function( errorLogService ) {

            return( errorLogService );

        }
    }
);


       
spherecastApp.factory(
	'errorLogService',
	function($log, $window, stackTraceService) {
	}
);
*/

