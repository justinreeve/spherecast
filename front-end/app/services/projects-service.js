spherecastApp.factory('Project', function($http)
{
	return 	{
		// Get a specific project or all the projects if no parameter is included. If a
		// specific project is retrieved, this will also retrieve the scenes for the project.
		get: function(id, startScene) {
			if (typeof id !== 'undefined')
			{
				if (startScene === true)
					return $http.get('/api/project/' + id + '/startScene');
				else
					return $http.get('/api/project/' + id);
			}
			else
				return $http.get('/api/project');
		},

		// Save a project.
		save: function(projectData) {
			return $http({
				method: 'POST',
				url: '/api/project',
				headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
				data: $.param(projectData)
			});
		},

		// Destroy a project.
		destroy: function(id) {
			return $http.delete('/api/project/' + id);
		}
	}
});
