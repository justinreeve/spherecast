<?php
class ActionParamsSpritemap extends ActionParamsImage
{
	protected $repeat;			// If true, repeat animation until the scene ends.

	public function file()
	{
		return $this->hasOne('file', 'fileid');
	}
}
?>