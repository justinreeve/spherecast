<?php
class Hotspot extends Eloquent
{
	protected $eventId;
	protected $positionX;
	protected $positionY;
	protected $positionZ;
	protected $radius;
	protected $duration;
	protected $activateByTrigger;
	protected $blockOtherHotspots;
	protected $countdownMedia;
	protected $countdownMediaParams;

	protected $fillable = array('eventId', 'positionX', 'positionY', 'positionZ', 'radius', 'duration', 'blockOtherHotspots', 'countdownMedia', 'countdownMediaParams');
	protected $table = 'sceneevents_hotspots';

	public function event()
	{
		return $this->belongsTo('SceneEvent', 'eventId', 'id');
	}

	public function labels()
	{
		return $this->hasMany('Label', 'eventId', 'id');
	}

	public function actions()
	{
		return $this->hasMany('Action', 'eventId', 'id');
	}
}
?>