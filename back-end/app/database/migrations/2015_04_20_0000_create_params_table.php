<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParamsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('params', function(Blueprint $table)
		{
			$table->integer('itemId');
			$table->char('itemType', 16);			// action, label, media, hotspot
			$table->string('value');

			// Indexes.
			$table->primary(array('itemId', 'itemType'));
			$table->index('itemType');

			// created_at, updated_at DATETIME
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('params', function(Blueprint $table)
		{
		});
	}
}
